
public class BruttoNettoKategorie
{
	public static void main(String[] args)
	{
		double nettoPreis;			//Deklaration
		int steuerKategorie;			
//		nettoPreis = 98;			// Initialisierung
		double brutto = 150;
		steuerKategorie = 2;

		nettoPreis=nettoRechnung(110, 2);
		
//		brutto = bruttoPreis(100, 2);  //Nutzt die Methode mit den Parametern
//		brutto = bruttoPreis(200, 1);
		
//		switch (steuerKategorie) 
//		{		
//		case 1:
//			brutto = nettoPreis * 1.2;
//			break;
//		case 2:
//			brutto = nettoPreis * 1.12;
//			break;
//		case 3:
//			brutto = nettoPreis * 1.1;
//			break;
//		default:
//			brutto=nettoPreis;
//			break;
//		}
//		

		
		System.out.println("Nettopreis: " + nettoPreis);
		System.out.println("Steuerkategorie: " + steuerKategorie);
		System.out.println("Bruttopreis: " + brutto);
		

//		if (steuerKategorie == 1)
//		{
//			brutto = nettoPreis * 1.2; // 20% Steuer
//		}
//
//		else if (steuerKategorie == 2)
//		{
//			brutto = nettoPreis * 1.12; // 12% Steuer
//		} 
//		else if (steuerKategorie == 3)
//		{
//			brutto = nettoPreis * 1.1;  // 10 % Steuer
//		}
//		else {
//			System.out.printf("Bruttopreis entspricht dem Nettopreis von %.2f", nettopreis);
//
//		}
//
//		System.out.printf("Der Bruttopreis ist %.2f", brutto); // .2 = auf 2 Kommastellen

	}

	public static double bruttoPreis(double nettoPreis, int steuerKategorie) // (double x,..) von au�en werden parameter mitgegeben
	{
		double brutto;   //f�hrt das brutto ein zum rechnen
		
		switch (steuerKategorie) 
		{		
		case 1:
			brutto = nettoPreis * 1.2;
			break;
		case 2:
			brutto = nettoPreis * 1.12;
			break;
		case 3:
			brutto = nettoPreis * 1.1;
			break;
		default:
			brutto=nettoPreis;
			break;
		}
		
		
		return brutto; // Liefert einen Wert zur�ck
	}
	
	public static double nettoRechnung(double brutto, int kategorie)
	{
		double netto;
		
		switch(kategorie)
		{
		case 1:
			netto=brutto / 1.2;
			break;
		case 2:
			netto=brutto / 1.12;
			break;
		case 3:
			netto=brutto / 1.1;
			break;
		default:
			netto=brutto+2;
			break;
		}
		
		return netto;
	}
	
}
