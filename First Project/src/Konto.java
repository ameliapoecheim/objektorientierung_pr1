
public class Konto
{

	public static void main(String[] args)
	{
		int jahre=10;			// Anzahl der Jahre
		double konto=2000;			// 2000 �
		double zinsen=1.5/100;		// 2,5% Zinsen
		
		
		System.out.println("\n\nNach " + jahre + " Jahren betr�gt der Kontostand: " + kontoJahr(konto, jahre, zinsen));
			// Gibt Endkontostand nach x Jahren aus
	}
	
	public static double kontoJahr(double konto, int max, double zinsen) //Holt sich die Variablen
	{
		double ergebnis =konto;		// Deklariert und Initialisiert Kontostand
		System.out.println("");		// Macht eine Leerzeile (Nicht funktionaler Befehl)
		
		for(double index=1; index<=max; index++) 	//Schleife die von Jahr 1 bis Jahr x l�uft.
		{
			ergebnis=ergebnis*(1.0+zinsen);			//Rechnet pro jahr Zinsen dazu
			
			System.out.println("Jahr " + index + "  Kontostand: " + ergebnis);		//Gibt die Zinsen f�r jedes Jahr aus
		}
		

		
		return ergebnis;
	}
	
}
