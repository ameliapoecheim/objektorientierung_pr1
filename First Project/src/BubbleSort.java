
public class BubbleSort
{
	
	public static void main(String[] args)
	{
		int[] a = new int[] {1,5,12,4,7,9,8,6,11,3,2};   // Feld deklariert und initialisiert
		
//		int max= findMax(a);
//		System.out.println(max);
		
//		System.out.println(findMax(a));		// Methode wird abgerufen und ausgegeben
		
		printArray(a);
		bubbleSort(a);
//		printArray(a);
		
//		System.out.println(a.length);	
	}
	
	public static int findMax(int[] a)		
	{
		int result = a[0];					// Speicherung f�r "aktuelles" max
		
		for(int index=0; index < a.length; index++)		// Schleife lauft solang das Ende des Feldes noch nicht erreicht ist.
		{
			if(result < a[index])					// Bedingung zur Suche des Maximums
			{
				result = a[index];
			}
			
			
		}
		
		return result;
			
	}
	
	
	public static void printArray(int[] a)
	{
		for(int index=0; index<a.length;index++)	// Schleife zur Ausgabe jedes Inhalts aus allen Feldern
		{
			System.out.print(a[index]+" ");
		}
		System.out.println();
	}
	
	public static void bubbleSort(int[] a)
	{		
//		int counter=0;	
		boolean change=true;										//Boolean wie counter
		
		int temp=a[0];		
		int length=a.length;
		
			while(length!=1)			//a[0]>a[1] && a[1]>a[2] && a[2]>a[3] && a[3]>a[4]
			{				// Change = true/false
					
				for(int index=0; index+1<length;index++)
				{
					
			
					if(a[index]>a[index+1])
					{
						change=true;	
						
						temp=a[index];
						a[index]=a[index+1];
						a[index+1]= temp;
					}
					change=false;
					
					
				}
				printArray(a);
				length--;
//					System.out.println("Z�hler = "+counter);
			}
	
	}
	

}
