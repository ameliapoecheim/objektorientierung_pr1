
public class ExampleArray
{

	public static void main(String[] args)
	{
					   // Stelle 0 1 2 3 4
		int[] feld1 = new int[] {5,2,4,1,3};		// Deklariert und initialisiert ein Feld und seinen Inhalt
		
//		System.out.println(feld1[4]);				// Ausgabe des Inhalts an der Stelle 4 (=3)
//		System.out.println(feld1.length);			// Gibt aus wie lang/gro� das Feld ist
		
		
		printArray(feld1);
//		printArray(new int[]{1,2,3,4,8,5,6,9,7,4,5});  // Ruft Methode mit dem Feld den Inhalten die man festlegt ab
		System.out.println();
		
		printArray(feld1);
		System.out.println();
		swap(feld1, 2,3);
		System.out.println();
		printArray(feld1);		
		
	}
	
	
	public static void printArray(int[] feld1)
	{
		for(int index=0; index<feld1.length;index++)	// Schleife zur Ausgabe jedes Inhalts aus allen Feldern
		{
			System.out.println(feld1[index]);
		}
	}
	
	
	public static void swap(int[] feld, int first, int second)
	{
		int swap = feld[first];
		
		feld[first]=feld[second];
		
		feld[second]=swap;
		
		
	}
	
	
	
	

}
