
public class WhileExample
{

	public static void main(String[] args)
	{
		double einlage =2000;
		double zinsen=1.025;
		double zielbetrag=3000;
		double kontostand=einlage;
		int jahr=0;
		
		while(kontostand<zielbetrag)
		{
			kontostand=kontostand*zinsen;
			jahr++;
			System.out.println("Jahr "+ jahr + " " +kontostand);
		}
		
		
	}

}
