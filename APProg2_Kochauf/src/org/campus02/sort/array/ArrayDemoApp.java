package org.campus02.sort.array;

import java.util.Arrays;

public class ArrayDemoApp
{

	public static void main(String[] args)
	{
		int[] demoArray = new int[8];
		demoArray[0] = 3;
		demoArray[1] = 1;
		demoArray[2] = 8;
		demoArray[3] = 4;
		demoArray[4] = 9;
		demoArray[5] = 2;
		demoArray[6] = 7;
		demoArray[7] = 6;

		// printArray(demoArray);
		// System.out.println();
		//
		// Arrays.sort(demoArray);
		// printArray(demoArray);

		Person[] leute = new Person[5];
//		leute[]=; 
		

//		printArray(leute);

	}

//	public static void printArray(Person[] arr)
//	{
//		for (int i : arr)
//		{
//			System.out.print(i + " ");
//		}
//		System.out.println();
//	}

	public static void printArray(int[] arr)
	{
		for (int i : arr)
		{
			System.out.print(i + " ");
		}
		System.out.println();
	}
}
