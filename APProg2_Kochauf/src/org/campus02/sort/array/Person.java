package org.campus02.sort.array;

public class Person implements Comparable<Person>
{
	private String vorname;
	private String nachname;
	private int number;
	private int age;
	public Person(String vorname, String nachname, int number, int alter)
	{
		super();
		this.vorname = vorname;
		this.nachname = nachname;
		this.number = number;
		this.age = alter;
	}
	@Override
	public String toString()
	{
		return "Person [vorname=" + vorname + ", nachname=" + nachname + ", number=" + number + ", alter=" + age
				+ "]";
	}
	public String getVorname()
	{
		return vorname;
	}
	public String getNachname()
	{
		return nachname;
	}
	public int getNumber()
	{
		return number;
	}
	public int getAlter()
	{
		return age;
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + ((nachname == null) ? 0 : nachname.hashCode());
		result = prime * result + number;
		result = prime * result + ((vorname == null) ? 0 : vorname.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (age != other.age)
			return false;
		if (nachname == null)
		{
			if (other.nachname != null)
				return false;
		} else if (!nachname.equals(other.nachname))
			return false;
		if (number != other.number)
			return false;
		if (vorname == null)
		{
			if (other.vorname != null)
				return false;
		} else if (!vorname.equals(other.vorname))
			return false;
		return true;
	}
	@Override
	public int compareTo(Person other)
	{
		if(age<other.age)
			return -1;
		if(age>other.age)
			return 1;
		
		return 0;
	}
	
}
