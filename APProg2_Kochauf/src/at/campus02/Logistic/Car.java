package at.campus02.Logistic;

public class Car implements Moveable
{
	private String type;
	private String color;
	private double weight;
	
	public Car(String type, String color, double weight)
	{
		this.type = type;
		this.color = color;
		this.weight = weight;
	}

	public String getType()
	{
		return type;
	}

	public String getColor()
	{
		return color;
	}

	public double getWeight()
	{
		return weight;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		long temp;
		temp = Double.doubleToLongBits(weight);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (color == null)
		{
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (type == null)
		{
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (Double.doubleToLongBits(weight) != Double.doubleToLongBits(other.weight))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "Car [type= " + type + ", color= " + color + ", weight= " + weight + "]";
	}

	@Override
	public void moveable(String destination)
	{
		System.out.println(color+" "+type+" will be moved to "+destination);
		
	}

	
	
	
}
