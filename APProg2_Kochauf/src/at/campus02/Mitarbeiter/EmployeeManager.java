package at.campus02.Mitarbeiter;
import java.util.*;

public class EmployeeManager
{
	private ArrayList<Employee> emplo;
	
	public EmployeeManager()
	{
		emplo = new ArrayList<>();
	}
	
	public void addEmployee(Employee e)
	{
		emplo.add(e);
	}
	
	public double calcTotalSalary()
	{
		double result=0;
		
		for (Employee emp : emplo)
		{
			result+=emp.getFullSalary();
		}
		return result;
	}
	
	public HashMap<String, Double> getSalaryByDepartment()
	{
		HashMap<String, Double> getBy = new HashMap<>();
		
		for (Employee emp : emplo)
		{
			if(!getBy.containsKey(emp.getDepartment()))
			{
				getBy.put(emp.getDepartment(), emp.getFullSalary());	//Department noch nicht in HashMap
			}
			else
			{
				double value = getBy.get(emp.getDepartment());		// Auslesen
				value += emp.getFullSalary();						// Bearbeiten
				getBy.put(emp.getDepartment(), value);				// Ablegen
			}
		}
		
		
		return getBy;
	}
}
