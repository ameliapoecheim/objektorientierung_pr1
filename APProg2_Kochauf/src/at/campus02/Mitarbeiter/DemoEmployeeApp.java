package at.campus02.Mitarbeiter;

public class DemoEmployeeApp
{

	public static void main(String[] args)
	{
		
		FixCommissionEmployee e1 = new FixCommissionEmployee("E1N", "E1N", "Dep1", 2000, 350);
		PercentCommissionEmployee e2 = new PercentCommissionEmployee("E2N", "E2N", "Dep1", 2000, 10);
		FixCommissionEmployee e3 = new FixCommissionEmployee("E3N", "E3N", "Dep3", 2150, 350);
		PercentCommissionEmployee e4 = new PercentCommissionEmployee("E4N", "E4N", "Dep4", 2500, 5);
		
		EmployeeManager empManager= new EmployeeManager();
		empManager.addEmployee(e1);
		empManager.addEmployee(e2);
		empManager.addEmployee(e3);
		empManager.addEmployee(e4);
		
		System.out.println("Total Salary: "+empManager.calcTotalSalary());
		
		System.out.println(empManager.getSalaryByDepartment());		

	}

}
