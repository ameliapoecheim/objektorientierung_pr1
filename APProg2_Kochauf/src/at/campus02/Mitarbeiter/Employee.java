package at.campus02.Mitarbeiter;

public class Employee
{
	protected String lastname;
	protected String firstname;
	protected String department;
	protected double baseSalary;
	
	public Employee(String lastname, String firstname, String department, double baseSalary)
	{
		this.lastname = lastname;
		this.firstname = firstname;
		this.department = department;
		this.baseSalary = baseSalary;
	}
	
	public double getFullSalary()
	{
		return baseSalary;
	}
	

	public String getLastname()
	{
		return lastname;
	}

	public String getFirstname()
	{
		return firstname;
	}

	public String getDepartment()
	{
		return department;
	}

	public double getBaseSalary()
	{
		return baseSalary;
	}
	
	
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(baseSalary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((department == null) ? 0 : department.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (Double.doubleToLongBits(baseSalary) != Double.doubleToLongBits(other.baseSalary))
			return false;
		if (department == null)
		{
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (firstname == null)
		{
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (lastname == null)
		{
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "Employee [lastname=" + lastname + ", firstname=" + firstname + ", department=" + department
				+ ", baseSalary=" + baseSalary + "]";
	}
}
