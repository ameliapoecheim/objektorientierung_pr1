package at.campus02.Mitarbeiter;

public class PercentCommissionEmployee extends Employee
{
	protected double percentCommission;

	public PercentCommissionEmployee(String lastname, String firstname, String department, double baseSalary,
			double percentCommission)
	{
		super(lastname, firstname, department, baseSalary);
		this.percentCommission = percentCommission;
	}
	
	
	@Override
	public double getFullSalary()
	{
		return baseSalary+baseSalary/100*percentCommission;
	}


	public double getPercentCommission()
	{
		return percentCommission;
	}


	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(percentCommission);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}


	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PercentCommissionEmployee other = (PercentCommissionEmployee) obj;
		if (Double.doubleToLongBits(percentCommission) != Double.doubleToLongBits(other.percentCommission))
			return false;
		return true;
	}


	@Override
	public String toString()
	{
		return "PercentCommissionEmployee [percentCommission=" + percentCommission + "]";
	}
	
	
	

}
