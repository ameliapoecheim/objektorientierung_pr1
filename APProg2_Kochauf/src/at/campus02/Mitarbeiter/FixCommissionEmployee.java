package at.campus02.Mitarbeiter;

public class FixCommissionEmployee extends Employee
{
	protected double additionalCommission;

	public FixCommissionEmployee(String lastname, String firstname, String department, double baseSalary,
			double additionalCommission)
	{
		super(lastname, firstname, department, baseSalary);

		this.additionalCommission = additionalCommission;

	}
	@Override
	public double getFullSalary()
	{
		return baseSalary+additionalCommission;
	}
	
	public double getAdditionalCommission()
	{
		return additionalCommission;
	}
	@Override
	public String toString()
	{
		return "FixCommissionEmployee [additionalCommission=" + additionalCommission + "]";
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(additionalCommission);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		FixCommissionEmployee other = (FixCommissionEmployee) obj;
		if (Double.doubleToLongBits(additionalCommission) != Double.doubleToLongBits(other.additionalCommission))
			return false;
		return true;
	}
	
	

}
