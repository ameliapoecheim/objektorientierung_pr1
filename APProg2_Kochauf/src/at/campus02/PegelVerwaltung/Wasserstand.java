package at.campus02.PegelVerwaltung;

public class Wasserstand
{
	private int id;
	private String Gewaessername;
	private double messWert;
	private double messWertFuerAlamierung;
	private int Zeitpunkt;
	private String ort;
		
	public Wasserstand(int id, String gewaessername, double messWert, double messWertFuerAlamierung, int zeitpunkt, String ort)
	{
		this.id = id;
		this.Gewaessername = gewaessername;
		this.messWert = messWert;
		this.messWertFuerAlamierung = messWertFuerAlamierung;
		this.Zeitpunkt = zeitpunkt;
		this.ort=ort;
	}
	
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getGewaessername()
	{
		return Gewaessername;
	}
	public void setGewaessername(String gewaessername)
	{
		Gewaessername = gewaessername;
	}
	public double getMessWert()
	{
		return messWert;
	}
	public void setMessWert(double messWert)
	{
		this.messWert = messWert;
	}
	public double getMessWertFuerAlamierung()
	{
		return messWertFuerAlamierung;
	}
	public void setMessWertFuerAlamierung(double messWertFuerAlamierung)
	{
		this.messWertFuerAlamierung = messWertFuerAlamierung;
	}
	public int getZeitpunkt()
	{
		return Zeitpunkt;
	}
	public void setZeitpunkt(int zeitpunkt)
	{
		Zeitpunkt = zeitpunkt;
	}
	public String getOrt()
	{
		return ort;
	}
	
	
	@Override
	public String toString()
	{
		return "Wasserstand [id=" + id + ", Gewaessername=" + Gewaessername + ", messWert=" + messWert
				+ ", messWertFuerAlamierung=" + messWertFuerAlamierung + ", Zeitpunkt=" + Zeitpunkt + "]";
	}
}
