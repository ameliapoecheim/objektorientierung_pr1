package at.campus02.PegelVerwaltung;

import java.util.ArrayList;

public class WasserstandManager
{
	private ArrayList<Wasserstand> sammlung;
	
	public WasserstandManager()
	{
		sammlung = new ArrayList<>();
	}
	
	public void addWasserstand(Wasserstand w)
	{
		sammlung.add(w);
	}
	
	public Wasserstand findById(int id)
	{
		for (Wasserstand ws : sammlung)
		{
			if(ws.getId()==id)
			{
				return ws;
			}
		}
		return null;
	}
	
	public ArrayList<Wasserstand> findAllByGewaesser(String gewaesserName)
	{
		ArrayList<Wasserstand> wanted = new ArrayList<>();
		for (Wasserstand ws : sammlung)
		{
			if(ws.getGewaessername()==gewaesserName)
			{
				wanted.add(ws);
			}
		}
		return wanted;
	}
	
	public Wasserstand findLastWasserstand(String gewaesserName)
	{
		Wasserstand last = sammlung.get(0);
		for (Wasserstand ws : sammlung)
		{
			if(ws.getGewaessername()==gewaesserName&& last.getZeitpunkt()<ws.getZeitpunkt())
			{
				last=ws;
			}
		}
		return last;
	}
	
	public ArrayList<Wasserstand> findForAlarmierung()
	{
		ArrayList<Wasserstand> alarm = new ArrayList<>();
		for (Wasserstand ws : sammlung)
		{
			if(ws.getMessWert()>=ws.getMessWertFuerAlamierung())
			{
				alarm.add(ws);
			}
		}
		return alarm;
	}
	
	public double calcMittlererWasserstand(String gewaesserName, String ort)
	{
		double average=0;
		int ctr=0;
		
		for (Wasserstand ws : sammlung)
		{
			if(ws.getGewaessername()==gewaesserName&&ws.getOrt()==ort);
			{
				average+=ws.getMessWert();
				ctr++;
			}
		}
		
		return average/ctr;
	}
	
	public ArrayList<Wasserstand> findByZeitpunkt(int von, int bis, String gewaesserName, String ort)
	{
		
		for (Wasserstand ws : sammlung)
		{
			if(ws.getGewaessername()==gewaesserName&&ws.getOrt()==ort&&von<ws.getZeitpunkt()&&ws.getZeitpunkt()<bis)
			{
				
			}
		}
		
		
		
		
		
		return null;
	}
	
	
	
	
	
}
