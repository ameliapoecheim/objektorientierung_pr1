package at.campus02.personalverwaltung.comperator;

import java.util.Arrays;

public class DemoEmployeeCompare
{

	public static void main(String[] args)
	{
		Employee[] emps = new Employee[3];
		
		emps[0]=new Employee(2, "Emp1", 1500.0, "Dep1");
		emps[1]=new Employee(3,"Emp2",1750.0,"Dep1");
		emps[2]=new Employee(1,"Emp2",2100.0,"Dep2");
		
		printArray(emps);
		
		Arrays.sort(emps, new NameDepComp());
		
		printArray(emps);
		
		
		
	}
	
	public static void printArray(Employee[] e)
	{
		for (Employee emp : e)
		{
			System.out.print(emp + " ");
		}
		System.out.println();
	}
	
	

}
