package at.campus02.personalverwaltung.comperator;

import java.util.Comparator;

public class NameDepComp implements Comparator<Employee>
{
	
	
	@Override
	public int compare(Employee o1, Employee o2)
	{
	/*
	 * int result = o1.getName().compareTo(o2.getName());
	 * 
	 * if(result==0) 
	 * {
	 * 		result=o1.getDepartment().compareTo(o2.getDepartment());
	 * }
	 * 
	 * return result;
	 */	
		
		
		
		if(o1.getName().compareTo(o2.getName())>0)
			return 1;
		if(o1.getName().compareTo(o2.getName())<0)
			return -1;
		
		if(o1.getDepartment().compareTo(o2.getDepartment())>0)
			return 1;
		if(o1.getDepartment().compareTo(o2.getDepartment())<0)
			return-1;

		return 0;
	}
	
}
