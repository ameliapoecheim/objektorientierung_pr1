package at.campus02.EventKalender;

public class DemoEventApp
{

	public static void main(String[] args)
	{
		EventKalender evkal = new EventKalender();
		
		Event e1 = new Event("Event 1", "Graz", 20.50);
		Event e2 = new Event("Event 2", "Wien", 30.25);
		Event e3 = new Event("Event 3", "Graz", 25.50);
		Event e4 = new Event("Event 4", "Klagenfurt", 35.53);
		Event e5 = new Event("Event 5", "Graz", 45);
		Event e6 = new Event("Event 3", "Wien", 40.75);
		
		evkal.add(e1);	evkal.add(e2);
		evkal.add(e3);	evkal.add(e4);
		evkal.add(e5);	evkal.add(e6);
		
		System.out.println(evkal.getByTitle("Event 3"));	
		System.out.println(); System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		
		System.out.println(evkal.getByOrt("Graz"));
		System.out.println(); System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		
		System.out.println(evkal.getbyPreis(25.50, 35.53));
		System.out.println(); System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		
		System.out.println(evkal.getbyPreis(25.50, 35.53));
		System.out.println(); System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
				
		System.out.println(evkal.getMostExpensiveByOrt("Graz"));
		System.out.println(); System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		
		System.out.println(evkal.getAvgPreisByOrt("Klagenfurt"));
		System.out.println(); System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		
		
		
		
		
	}

}
