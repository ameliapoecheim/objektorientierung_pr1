package at.campus02.EventKalender;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class EventKalender
{
	
	ArrayList<Event> events;
	
	public EventKalender()
	{
		events = new ArrayList<>();
	}
	
	public void add(Event eve)
	{
		events.add(eve);		
	}
	
	public Event getByTitle(String title)
	{
		Event byTitle;
		
		for (Event eve : events)
		{
			if(eve.getTitle().equals(title))
			{
				byTitle=eve;
				return byTitle;
			}
		}
		
		return null;
	}
	
	public ArrayList<Event> getByOrt(String ort)
	{
		ArrayList<Event> byOrt= new ArrayList<>();
		
		for (Event eve : events)
		{
			if(eve.getOrt().equals(ort))
			{
				byOrt.add(eve);
			}
		}
		
		return byOrt;
	}
	
	public ArrayList<Event> getbyPreis(double min, double max)
	{
		ArrayList<Event> preisgrenze = new ArrayList<>();
		
		for (Event eve : events)
		{
			if(eve.getPreis()>=min && eve.getPreis()<=max)
			{
				preisgrenze.add(eve);
			}
		}
		
		return preisgrenze;
	}
	
	public ArrayList<Event> getMostExpensiveByOrt (String ort)
	{
		ArrayList<Event> byort=getByOrt(ort);
		ArrayList<Event> result = new ArrayList<>();
		Event mostEx=byort.get(0);
		
		for (Event eve : byort)
		{
			if(eve.getPreis()>mostEx.getPreis())
			{
				mostEx=eve;
			}
		}
		result.add(mostEx);
		
		return result;
	}
	
	public double getAvgPreisByOrt(String ort)
	{
		int ctr=0;
		double result=0;
		
		for (Event eve : events)
		{
			if(eve.getOrt().equals(ort))
			{
				result+=eve.getPreis();
				ctr++;
			}
		}
		
		
		return result/ctr;
	}
	
	public HashMap<String, Integer> getCountEventsByOrt()
	{
		return null;
	}
	
	public HashMap<String, Integer> getSumPriceEventsByOrt()
	{
		return null;
	}


	

}
