package at.campus02.exeptions;

public class Note
{
	private String date;
	private String title;
	private String description;
	
	public Note(String date, String title, String description)
	{
		super();
		this.date = date;
		this.title = title;
		this.description = description;
	}
	
	public String getDate()
	{
		return date;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	@Override
	public String toString()
	{
		return "Note [date=" + date + ", title=" + title + ", description=" + description + "]";
	}
	
	
	
	
	
	
}
