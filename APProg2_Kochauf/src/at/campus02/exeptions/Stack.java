package at.campus02.exeptions;

public class Stack
{	
	private int pointer;
	private Note[] notes;
	
	public Stack(int size)
	{
		notes=new Note[size];
		pointer=0;
	}
	
	public void push(Note note)	throws StackFullException
	{
		if(pointer<notes.length)
		{
			notes[pointer++]=note;
//			pointer++;					// Mit 1. push wird der Pointer 1;
		}
		else
		{
//			System.out.println("Kann nicht, Stack ist voll.");
//							Stack ist voll, kann keine weiteren Elemente aufnehmen
			throw new StackFullException("Stack ist voll, maximal Platz fuer "+notes.length + " Notes.");
		}
	
	}
	
	public Note pop() throws StackEmptyException
	{
//		pointer--;					// Pointer wird verringert;
//		Note note = notes[pointer];
//		return note;
		
		if(pointer==0)
		{
			throw new StackEmptyException("Stack ist leer. Es ist unm�glich etwas von einem leeren Stapel herunterzunehmen");
		}
		
		return notes[--pointer];
	}
	
	
}
