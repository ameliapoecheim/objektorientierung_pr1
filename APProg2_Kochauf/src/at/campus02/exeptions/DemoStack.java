package at.campus02.exeptions;

public class DemoStack
{

	public static void main(String[] args)
	{
		try
		{
			Stack stack = new Stack(5);
			
			stack.push(new Note("23.4.2018","Note 1", "1 - Ein bisschen Text"));
			stack.push(new Note("24.4.2018","Note 2", "2 - Ein bisschen Text"));
			stack.push(new Note("25.4.2018","Note 3", "3 - Ein bisschen Text"));
			
			System.out.println(stack.pop());	// 3
			System.out.println(stack.pop());	// 2
			
			stack.push(new Note("26.4.2018","Note 4", "4 - Ein bisschen Text"));
			stack.push(new Note("27.4.2018","Note 5", "4 - Ein bisschen Text"));
			stack.push(new Note("28.4.2018","Note 6", "6 - Ein bisschen Text"));
			
			System.out.println(stack.pop());	// 6
			
			stack.push(new Note("29.4.2018","Note 7", "7 - Ein bisschen Text"));
			stack.push(new Note("30.4.2018","Note 8", "8 - Ein bisschen Text"));
			
//			System.out.println("Vor dem Fehler...");
//			stack.push(new Note("26.4.2018","Note 4", "4 - Ein bisschen Text"));		//StackFullException wird ausgel�st
//			System.out.println("...nach dem Fehler");
			
			System.out.println(stack.pop());
			System.out.println(stack.pop());
			System.out.println(stack.pop());
			System.out.println(stack.pop());
			System.out.println(stack.pop());
			System.out.println(stack.pop());			//StackEmptyException wird aufgerufen
//			System.out.println(stack.pop());			
			
			
		} 
		
		catch (StackFullException f)
		{
			f.printStackTrace();
		}

		catch (StackEmptyException e) 
		{
			e.printStackTrace();
		}
		
		finally
		{
			System.out.println("Wir sind im finally-Block");
		}
		
		
	}

}
