package at.campus02.Figuren;

public interface Figure
{
	double getPerimeter();
	double getArea();
}

/*
 * 
 * public abstract class Figure
 * {
 * 		public abstract double getPerimeter();
 * 		public abstract double getArea(); 		
 * }
 * 
 * 
 * 
 */
