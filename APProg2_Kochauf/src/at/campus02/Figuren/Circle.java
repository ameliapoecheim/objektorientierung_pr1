package at.campus02.Figuren;

public class Circle implements Figure
{
	private double radius;
	
	public Circle(double radius)
	{
		super();
		this.radius=radius;
	}
	
	

	@Override
	public double getPerimeter()
	{
		return 2*radius*Math.PI;
	}

	@Override
	public double getArea()
	{
		// TODO Auto-generated method stub
		return Math.pow(radius, 2)*Math.PI;
	}

}
