package at.campus02.Figuren;

import java.util.ArrayList;
import java.util.HashMap;

public class FigureManager
{
	protected ArrayList<Figure> figuren = new ArrayList<>();
	
	public void add(Figure f)
	{
		figuren.add(f);
	}
	
	public double getMaxPerimeter()
	{
		double max=0;
		for (Figure fig : figuren)
		{
			if(fig.getPerimeter()>max)
			{
				max=fig.getPerimeter();
			}
		}
		return max;
	}
	
	public HashMap<String, Double> getAreaSizeByVategories()
	{
		HashMap<String, Double> erg= new HashMap<>();
		
		erg.put("Klein", 0.0);
		erg.put("Mittel", 0.0);
		erg.put("Gro�",0.0);	
		
		for (Figure fig : figuren)
		{
			if(fig.getArea()<1000)
			{
				double value =erg.get("Klein");
				value+=fig.getArea();
				erg.put("Klein", value);
			}
			else if(fig.getArea()<5000)
			{
				double value =erg.get("Mittel");
				value+=fig.getArea();
				erg.put("Mittel", value);
			}
			else
			{
				double value =erg.get("Gro�");
				value+=fig.getArea();
				erg.put("Gro�", value);
			}
		}
		return erg;
	}
	
	
}
