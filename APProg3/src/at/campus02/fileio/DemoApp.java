package at.campus02.fileio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DemoApp
{

	public static void main(String[] args) throws LustigeException
	{
		File f = new File("test.txt");		//		 \ / kann man mit .FileSeperator, Seperator umgangen werden
		System.out.println(f.getAbsolutePath());

// 		File dir = new File("bla\\bar\\bas");
		
//		System.out.println(dir.getAbsolutePath());
//		if (!dir.exists())
//		{
//			System.out.println(dir.mkdirs());
//		}
		try
		{
			if (!f.exists())
			{
				f.createNewFile();
			}
		} catch (IOException e)
		{
			System.out.println("Fehler beim erstellen der Datei: " + e.getMessage());
			return;
		}
		
		try
		{
			FileInputStream inp = new FileInputStream("test.txt");  // Auch new FileInputStream(f); m�glich
			
//			inp.skip(2);  // �berspringt die 1. x Bytes
			
			int b;
			
			while(-1 != (b = inp.read()))
			{
				System.out.println(b);
				System.out.println(Character.toChars(b));
			}
			
			
			
			inp.close();
		} 
		catch (FileNotFoundException e)
		{
			System.out.println("Datei wurde nicht gefunden: " + e.getMessage());
			return;
		} 
		catch (IOException e)
		{
			System.out.println("Datei konnte nicht gelesen werden: " + e.getMessage());
			return;
		}
		
		
		
		

// 		if (dir.isDirectory())
// 		{
// 			for (String l : f.list(new txtFilenameFilter()))
// 			{
// 				System.out.println(l);
// 			}
// 		}

	}

}
