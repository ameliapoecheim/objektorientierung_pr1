package at.campus02.fileio;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

public class AdapterStreamDemo
{

	public static void main(String[] args) throws FileNotFoundException
	{
		
		InputStream ins = new FileInputStream("noten.csv");		// ByteStream �ffnen
		Reader isr = new InputStreamReader(ins);				// Stream auf Reader �bersetzen
		BufferedReader br = new BufferedReader(isr);			// Reader f�r zeilenweises Lesen		�bersetzt Zeilenweise Byte-Streams ein und liefert Zeichen.
		
//		br.readLine();
		
		
		OutputStream out = new FileOutputStream("noten.csv");	// Schreibt den in Bytes �bersetzten String
		Writer wr = new OutputStreamWriter(out);				// �bersetzt den gew�nschten String in Bytes
		
//		wr.write("Teile 1\r\n");
		
	}

}
