package at.campus02.fileio;

import java.io.File;
import java.io.FilenameFilter;

public class txtFilenameFilter implements FilenameFilter
{

	@Override
	public boolean accept(File dir, String name)
	{
		return name.endsWith(".txt");
	}

}
