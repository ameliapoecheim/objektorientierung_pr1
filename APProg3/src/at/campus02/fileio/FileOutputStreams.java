package at.campus02.fileio;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class FileOutputStreams
{

	public static void main(String[] args)
	{
		String text = "Dies ist ein Text, welcher nicht sonderlich verwunderlich ist.";
		
		try
		{
			OutputStream out = new FileOutputStream("output.txt");
			
			for(char x : text.toCharArray())
			{
				out.write(x);
			}
			
			out.flush(); 		
			// .flush wartet so lange bis es eine Best�tigung der erfolgreichen Speicherung erh�lt. 			
			out.close();
		} 
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		
	}

}
