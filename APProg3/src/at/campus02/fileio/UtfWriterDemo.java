package at.campus02.fileio;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

public class UtfWriterDemo
{

	public static void main(String[] args)
	{
		try(
				OutputStream os = new FileOutputStream("umlaute.txt");
				Writer wr = new OutputStreamWriter(os, StandardCharsets.UTF_8)		// Speichert den Text mit der UTF 8 Codierung
				)
		{
			wr.write("K�che machen M�sli mit �pfeln.");
			
			
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		
		
	}

}
