package at.campus02.fileio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.channels.FileChannel;

public class InputStreamDemo
{

	public static void main(String[] args)
	{
		
		try
		{
			
			File f = new File("output.txt");
			long length = f.length();
			System.out.println(length);
			
			InputStream inp = new FileInputStream(f);
			FileOutputStream out = new FileOutputStream("output.txt", true);
			
			int b;
			
			while((b=inp.read()) != -1)
			{
				System.out.println(b);
			}
			
//			out.write(46); Appends "."
			
			
			out.close();
			inp.close();
			
		} 
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		

	}

}
