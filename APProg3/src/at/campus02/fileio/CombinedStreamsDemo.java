package at.campus02.fileio;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.crypto.EncryptedPrivateKeyInfo;

public class CombinedStreamsDemo
{

	public static void main(String[] args)
	{
		try
		{
			FileOutputStream fos = new FileOutputStream("test.txt");
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			EncryptenOutputStreams eos = new EncryptenOutputStreams(bos, 3);
			
			eos.write('a');
			eos.write('b');
			eos.write('c');
			
			eos.flush();
			eos.close();
			
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		

	}

}
