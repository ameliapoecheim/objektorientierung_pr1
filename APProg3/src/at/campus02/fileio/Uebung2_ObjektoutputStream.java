package at.campus02.fileio;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Uebung2_ObjektoutputStream
{

	public static void main(String[] args)
	{
		
		String text = "Das ist ein Text, aber auch ein Java-Object.";
		
		try
		{
			FileOutputStream fos = new FileOutputStream("object.dat");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(text);
			oos.flush();
			oos.close();
			
			FileInputStream fis = new FileInputStream("object.dat");
			ObjectInputStream ois = new ObjectInputStream(fis);
			
			String text_read = (String) ois.readObject();
			
			ois.close();
			
			System.out.println(text.equals(text_read));
			
		} 
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		} 
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		
		
		
		
	}

}
