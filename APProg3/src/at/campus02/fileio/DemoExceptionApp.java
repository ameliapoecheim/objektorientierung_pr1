package at.campus02.fileio;

import java.io.File;
import java.io.IOException;

public class DemoExceptionApp
{

	public static void main(String[] args) throws LustigeException
	{

		try
		{
			File f = new File("F:\\text.txt");
			System.out.println(f.getAbsolutePath());
			if (!f.exists())
			{
				f.createNewFile();
				throw new LustigeException();
			}
			
			
			
			
		} 
		catch (IOException e)
		{
			System.out.println("Es ist ein Fehler passiert");
			LustigeException le = new LustigeException();
			le.addSuppressed(e);
			throw le;
		}
		catch (LustigeException e)
		{
			System.out.println("Es ist ein lustigen Fehler passiert");
			throw e;
		}
		

	}

}
