package at.campus02.fileio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;


public class Uebung4_FileWriter
{

	public static void main(String[] args)
	{
		try
		{
			File notes = new File("noten.csv");
			if(!notes.exists())
			{
				notes.createNewFile();
			}
			
			
			Reader in = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(in);
			Writer fw = new FileWriter("noten.csv");
			
			String line;
			
			
			while((line = br.readLine()) != null)
			{
				if(line.equals("STOP"))
				{
					fw.close();
					br.close();
					in.close();
					return;
				}
				else
				{
					writeLine(line, fw);
				}
			}
			
			
			fw.close();
			br.close();
		} 
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}
	
	private static void writeLine(String line, Writer fw) throws IOException
	{
		for(String i : line.split(" "))
		{
			if(i.length() == 0)
			{
				continue;
				
				// M�gliche Abbruchbedingungen:
				// break; continue; return;
				
			}
			if(i.charAt(i.length()-1) == ':')
			{
				fw.write(i.substring(0, i.length() - 1));
//				System.out.print(i.substring(0, i.length() - 1));
			}
			else
			{
				fw.write(i);
//				System.out.print(i);
			}
			fw.write(';');
//			System.out.print(";");
		}
		fw.write("\r\n");
//		System.out.println();		
		
		
		
		
	}
	
	
	
	
	
	

}
