package at.campus02.fileio;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class TryWithResourcesDemo
{

	public static void main(String[] args)
	{
		try (
				Reader re = new FileReader("noten.csv"); 
				BufferedReader br = new BufferedReader(re);
			)
		{
			
			String line;

			while ((line = br.readLine()) != null)
			{
				System.out.println(line);
			}
			
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		
	}

}
