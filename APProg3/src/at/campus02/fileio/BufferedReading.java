package at.campus02.fileio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class BufferedReading
{

	public static void main(String[] args)
	{
		try
		{
			
			Reader in = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(in);
			
			String line;
			
			while((line = br.readLine()) != null)
			{
				switch (line)
				{
				case "HELP":
					System.out.println("STOP: Stoppt die Anwendung");
					break;
				case "STOP":
					return;
				}
			}
			
			
			
			br.close();
		} 
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}

}
