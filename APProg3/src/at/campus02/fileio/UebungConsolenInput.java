package at.campus02.fileio;

import java.io.IOException;

public class UebungConsolenInput
{

	public static void main(String[] args)
	{
		int b;
		
		try
		{
			
			while ((b = System.in.read()) != -1)
			{
				char zero = Character.toChars(b)[0];
//				char[] inp = Character.toChars(b);			// Andere Variante
				
				if(zero == 'x' || zero == 'X')		// Enter f�gt der Zeichenkette \n \r hinzu; das erzeugt viele Zeilenumbr�che
				{
					System.out.println(zero);
					return;
				}
				System.out.print(zero);
				
				
				// Variante 2
				
//				if( b == 88 || b == 120)
//				{
//					System.out.println(Character.toChars(b));
//					return;
//				}
				
				
			}
			
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}

	}

}
