
public class Student
{
	private String vorname;
	private String nachname;
	private double credits;
	private boolean inskribiert;
	private String lieblingsfarbe;
		
	public String getVorname()
	{
		return vorname;
	}
	public void setVorname(String vorname)
	{
		this.vorname = vorname;
	}

	public String getNachname()
	{
		return nachname;
	}

	public void setNachname(String nachname)
	{
		this.nachname = nachname;
	}
	public double getCredits()
	{
		return credits;
	}

	public void setCredits(double credits)
	{
		this.credits = credits;
	}
	public boolean isInskribiert()
	{
		return inskribiert;
	}
	public void setInskribiert(boolean inskribiert)
	{
		this.inskribiert = inskribiert;
	}
	public String getLieblingsfarbe()
	{
		return lieblingsfarbe;
	}


	public void setLieblingsfarbe(String lieblingsfarbe)
	{
		this.lieblingsfarbe = lieblingsfarbe;
	}

	@Override
	public String toString()
	{
		return "Student [vorname=" + vorname + ", nachname=" + nachname + ", credits=" + credits + ", inskribiert="
				+ inskribiert + ", lieblingsfarbe=" + lieblingsfarbe + "]";
	}
	
}
