import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyStarter
{

	public static void main(String[] args)
	{
		MyDBHelper helper = new MyDBHelper();

		helper.init();
//		helper.printAlleUrlaube();
//		System.out.println();
//		helper.printAlleStudenten();
//		System.out.println();
//		helper.printAllUrlaubeWithPreisGreaterThanOrderByOrt(245);
//		System.out.println();
//		helper.printDetailsforStudentWithId(6);
//		System.out.println();
//		helper.printDetailsforStudentWithIdPrepared(6);
//		System.out.println();
//		helper.printAllUrlaubeForStudentWithIdAndPriceGreaterThan(1, 200);
//		System.out.println();
//		helper.printUrlaubsuebersichtForAllStudents();
//		System.out.println();
//		helper.printZusammenfassungProStudent();
//		System.out.println();
//		helper.printUrlaubsOrtStatistik();
//		System.out.println();
		
		
		for(int i=1; i<5;i++)
		{
		System.out.println(helper.getStudent(i));
		}
		
//		helper.insertUrlaub("Mexico", 1200);
//
//		helper.deleteUrlaub(2);
		
		
		
		
		

		helper.close();
	}

	public static void demoEins() throws Exception // Zwang - der Aufrufer MUSS ein Try Catch verwenden
	{
		System.out.println("Hello World aus Demo eins");

		int i = 10, j;

		j = 0;

		int erg = 0;

		try
		{
			erg = i / j; // Kein Catch deshalb weiter hochwerfen throw
		} catch (Exception e)
		{
			System.out.println("Stack Trace demo eins");
			e.printStackTrace();
			throw e;
		}

		System.out.println(erg);
	}
}