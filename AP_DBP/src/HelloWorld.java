import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

public class HelloWorld
{
	private Connection con = null;
	
	public static void main(String[] args)
	{
		System.out.println("Hello Campus02.");
		
		
//		import java.sql.DriverManager;
//		import java.sql.Connection;
//		import java.sql.SQLException;
		
		
		try		//Treiber laden um eine Connection herzustellen
		{
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		}
		catch(ClassNotFoundException ex)
		{
			ex.printStackTrace();
		}
		
		try		//Nutzen des des geladenes Treibers um eine Connetion herzustellen
		{
			Connection con = DriverManager.getConnection("jdbc:ucanaccess://"+
							"C:/Users/amelia.poecheim/Desktop/DBP/"+
							"Urlaubsverwaltung.accdb");
			System.out.println("Super - hat funktioniert ^^");
			
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void close()
	{
		if(con!=null)
		{
			try
			{
				con.close();
			}
			
			catch(SQLException e)
			{
				e.printStackTrace();
				System.out.println("Fehler beim schlie�en der Verbindung.");
			}
			
			
		}
	}
	
	

}
