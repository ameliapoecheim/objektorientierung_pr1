import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class MyDBHelper
{

	private Connection con = null;

	public void init()
	{

		try
		{
			// 1a. Jar-Files referenzieren - Java Build Path
			// 1b. Load Access Driver
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		} catch (ClassNotFoundException e)
		{

			e.printStackTrace();
		}

		try
		{

			// 2. get Connection to database
			con = DriverManager.getConnection(
					"jdbc:ucanaccess://" + "C:/Users/amelia.poecheim/Desktop/DBP/" + "Urlaubsverwaltung.accdb");

			System.out.println("Your are connected :-D\n");

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public void close()
	{
		if (con != null)
			try
			{
				con.close();
				System.out.printf("\nYou are diconnected. *wave*");
			} catch (SQLException e)
			{
				e.printStackTrace();
				System.out.println("Fehler beim Schliessen der Verbindung");
			}
	}

	public void insertUrlaub(String destination, double preis)
	{
		// insert into MeineUrlaube(Ziel, price) .....
	}

	public void insertUrlaub(Urlaub urlaub)
	{
		// insert into MeineUrlaube(Ziel, price) .....
	}

	public void deleteUrlaub(int urlaubsNr)
	{

	}
	/*
	 * public ArrayList<String> GetAlleUrlaubPreisGroesserAls(double preis) {
	 * 
	 * return null; }
	 */

	public ArrayList<Urlaub> GetAlleUrlaubPreisGroesserAls(double preis)
	{

		return null;
	}

	public void listKunden()
	{

		try
		{

			Statement stmt = con.createStatement();
			String sql = "SELECT * FROM Kunde ";

			ResultSet rs = stmt.executeQuery(sql);

			System.out.println("Meine Kunden");
			while (rs.next())
			{
				System.out.print("Kdnr: " + rs.getInt(1));
				System.out.println(" Vorname " + rs.getString(2));

			}
			rs.close();
			stmt.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public void printAlleUrlaube()
	{

		try
		{
			Statement stmt = con.createStatement();
			String query = "SELECT Preis, Ort, Preis*1.1 FROM Urlaubswunsch";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next())
			{
				String ort = rs.getString(2);
				double preis = rs.getDouble(1);
				double preisPlus10prz = rs.getDouble(3);

				System.out.printf("Ort %s, Preis %.2f\n", ort, preis);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printAlleStudenten()
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "SELECT Vorname, Nachname, Credits, Inskribiert FROM Student";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next())
			{
				String vorname = rs.getString(1);
				String nachname = rs.getString(2);
				int credits = rs.getInt(3);
				boolean inskribiert = rs.getBoolean(4);
				String inskri;

				if (inskribiert == true)
				{
					inskri = "inskribiert";
				} else
				{
					inskri = "nicht inskribiert";
				}

				System.out.printf("Vorname: %s, Nachname: %s, Credits: %d, Inskribiert: %s\n", vorname, nachname,
						credits, inskri);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public void printAllUrlaubeWithPreisGreaterThanOrderByOrt(double Preis)
	{

		try
		{
			Statement stmt = con.createStatement();
			String query = "SELECT DISTINCT Ort, Preis FROM Urlaubswunsch WHERE Preis >";
			query += Preis;
			query += " Order By Ort";

			ResultSet rs = stmt.executeQuery(query);
			while (rs.next())
			{
				String ort = rs.getString(1);
				double preis = rs.getDouble(2);

				System.out.printf("Ort %s, Preis %.2f\n", ort, preis);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printDetailsforStudentWithId(int id)
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "SELECT Vorname, Nachname, Credits, Inskribiert, Lieblingsfarbe FROM Student WHERE Studentid ="
					+ id;
			ResultSet rs = stmt.executeQuery(query);

			if (rs.next() == true)
			{
				String vorn = rs.getString(1);
				boolean inskribiert = rs.getBoolean(4);

				System.out.printf("Vorname: %s, Insribiert: %s", vorn, inskribiert);
			} else
			{
				System.out.println("Kein solcher Student gefunden.");
			}

			// while (rs.next())
			// {
			// String vorname = rs.getString(1);
			// String nachname = rs.getString(2);
			// int credits = rs.getInt(3);
			// boolean inskribiert = rs.getBoolean(4);
			// String farbe=rs.getString(5);
			//
			// String inskri;
			//
			// if (inskribiert == true)
			// {
			// inskri = "inskribiert";
			// } else
			// {
			// inskri = "nicht inskribiert";
			// }
			//
			// System.out.printf("Vorname: %s, Nachname: %s, Credits: %d, Inskribiert: %s,
			// Lieblingsfarbe: %s\n", vorname, nachname, credits, inskri, farbe);
			// }
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printDetailsforStudentWithIdPrepared(int id)
	{
		String query = "SELECT Vorname, Nachname, Credits, Inskribiert, Lieblingsfarbe FROM Student WHERE Studentid = ?";

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			if (rs.next() == true)
			{
				String vorn = rs.getString(1);
				boolean inskribiert = rs.getBoolean(4);

				System.out.printf("Vorname: %s, Insribiert: %s", vorn, inskribiert);
			} else
			{
				System.out.println("Kein solcher Student gefunden.");
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printAllUrlaubeForStudentWithIdAndPriceGreaterThan(int id, double price)
	{
		String query = "SELECT Vorname FROM Student WHERE studentId=? ";

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			if (rs.next())
			{
				String vorname = rs.getString(1);
				System.out.printf("Student %s\n", vorname);

				String queryUrlaube = "SELECT Preis, Ort FROM Urlaubswunsch WHERE preis > ? AND Studentid = ?";
				PreparedStatement stmtUrlaube = con.prepareStatement(queryUrlaube);
				stmtUrlaube.setDouble(1, price);
				stmtUrlaube.setInt(2, id);
				ResultSet rsUrlaube = stmtUrlaube.executeQuery();
				int ctr = 0;

				while (rsUrlaube.next())
				{
					ctr++;
					double preis = rsUrlaube.getDouble(1);
					String ort = rsUrlaube.getString(2);

					System.out.printf("%.2f, %s\n", preis, ort);
				}
				System.out.println();
				if (ctr == 0)
				{
					System.out.println("Dieser Student ist urlaubslos glücklich.");
				}
			} else
			{
				System.out.printf("Student %s wurde nicht gefunden.", id);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printUrlaubsuebersichtForAllStudents()
	{
		String query = "SELECT studentid FROM Student ";
		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				int studentId = rs.getInt(1);
				printAllUrlaubeForStudentWithIdAndPriceGreaterThan(studentId, 0);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printZusammenfassungProStudent()
	{
		// SELECT Student.Vorname, COUNT(Urlaubswunsch.StudentID),
		// AVG(Urlaubswunsch.Preis)
		// FROM Student, Urlaubswunsch
		// WHERE Student.StudentID=Urlaubswunsch.StudentID
		// GOURP BY Vorname

		// String query = "SELECT Student.Vorname, COUNT(Urlaubswunsch.StudentID),
		// AVG(Urlaubswunsch.Preis) FROM Student, Urlaubswunsch WHERE
		// Student.StudentID=Urlaubswunsch.StudentID GROUP BY Student.Vorname";

		String query = "SELECT Student.Vorname, COUNT(Urlaubswunsch.StudentID), AVG(Urlaubswunsch.Preis)"
				+ " FROM Student left join Urlaubswunsch" // left join / inner join / right join / full outer join
				+ " ON Student.StudentID=Urlaubswunsch.StudentID" + " GROUP BY Student.Vorname";

		// 1. Variante gibt nur alle Studenten mit mind. einem Urlaubswunsch aus.
		// 2. Variante gibt alle STudenten, auch ohne Urlaubswünsche aus.

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				String vorname = rs.getString(1);
				int anzahl = rs.getInt(2);
				double durchschnitt = rs.getDouble(3);

				System.out.printf("%s, %d, %.2f\n", vorname, anzahl, durchschnitt);

			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printUrlaubsOrtStatistik()
	{
		String query = "SELECT Ort, COUNT(*) as Anzahl FROM Urlaubswunsch GROUP BY Ort ORDER BY COUNT(*) desc"; // Auch
																												// Count(Ort)
																												// möglich,
																												// aber
																												// wenn
																												// ein
																												// Ort
																												// null
																												// ist
																												// hat
																												// man
																												// ein
																												// Problem

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				String ort = rs.getString(1);
				int anzahl = rs.getInt(2);
				System.out.printf("%s, %d\n", ort, anzahl);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public Student getStudent(int studentNr)
	{
		Student s = new Student();

		try
		{
			String query = "SELECT Vorname, Nachname, Credits, Inskribiert, Lieblingsfarbe From Student WHERE Studentid = ?";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, studentNr);
			ResultSet rs = stmt.executeQuery();

			if (rs.next() == true)
			{
//				String vor = rs.getString(1);
//				s.setVorname(vor); 
				
				s.setVorname(rs.getString(1));
				s.setNachname(rs.getString(2));
				s.setCredits(rs.getDouble(3));
				s.setInskribiert(rs.getBoolean(4));
				s.setLieblingsfarbe(rs.getString(5));
							
			}
			else
			{
				return null;
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return s;
	}

}