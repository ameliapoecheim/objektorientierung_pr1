package org.campus.uno.prog1;

import java.util.ArrayList;
import java.util.Collections;


public class Unospiel
{
	private ArrayList<Card> ablageStapel = new ArrayList<Card>();
	private ArrayList<Card> kartenStapel = new ArrayList<Card>();
	
	private ArrayList<Spieler> mitspieler = new ArrayList<Spieler>();
	

	public Unospiel()
	{

		for(int index=0; index<2;index++)		//2x  10x Karten alles 4 Farben 
		{
			for(int zahlenwert=0; zahlenwert<10;zahlenwert++)
			{
				kartenStapel.add(new Card("gelb", zahlenwert));
				kartenStapel.add(new Card("rot", zahlenwert));
				kartenStapel.add(new Card("blau", zahlenwert));
				kartenStapel.add(new Card("gruen", zahlenwert));
			
			}
		}	
		
		Collections.shuffle(kartenStapel);		// Mischt die Karten
			
	}
	
	
	public Card abheben()
	{
		// Sind noch gen�gend Karten am Stapel?
		
		if(kartenStapel.size()==0)
		{
			Card obersteKarte = ablageStapel.remove(0);   //Oberste Karte "Sichern"
						
			Collections.shuffle(ablageStapel);   	//Restlichen karten neu mischen.
			
			kartenStapel.addAll(ablageStapel);   	//Gemischte Ablage Karten zum Karten Stapel hinzuf�gen
			ablageStapel.clear();				// Kartenstapel leeren und
			ablageStapel.add(obersteKarte);  	// "gesicherte" Karte wieder auf den Ablage Stapel legen, 
												//Damit man wieder eine offene Karte zum weiterspielen hat.
			
		}
		
		return kartenStapel.remove(0);
	}
	
	public void ablegen(Card abzulegende)
	{
		ablageStapel.add(0, abzulegende);   // Karte an der Stelle 0 einf�gen
	}
	
	public void mitSpielen(Spieler neuerSpieler)
	{
		mitspieler.add(neuerSpieler);
	}
	
	
	
	public boolean spielZug()
	{
		// Wer ist dran?
		Spieler aktuellerSpieler = mitspieler.remove(0);
		System.out.println("Am Zug ist: "+aktuellerSpieler);
		
		//Spieler vergleicht die oberste karte am Stapel mit der Hand, ohne noch etwas damit zu tun.
		Card gefundeneKarte = aktuellerSpieler.passendeKarte(ablageStapel.get(0));		
		
		if(gefundeneKarte!=null)
		{
			System.out.println("Karte ablegen:"+gefundeneKarte);
			ablegen(gefundeneKarte);
		}
		else
		{
			System.out.println(" zieht eine Karte.");
			Card abgehobeneKarte = abheben();
			if (abgehobeneKarte.match(ablageStapel.get(0)))
			{
				System.out.println(" ablegen:"+abgehobeneKarte);
				ablegen(abgehobeneKarte);
				
			}
			else
			{
				System.out.println(" aufnehmen.");
				aktuellerSpieler.aufnehmen(abgehobeneKarte);
			}
		}
		
		if(aktuellerSpieler.anzahlHandkarten()==0)
		{
			System.out.println("UNO UNO!! Gewinner ist: " + aktuellerSpieler);
			return false;
		}
		
		// Spieler am ende wieder zum Ende der Liste hinzuf�gen.
		mitspieler.add(aktuellerSpieler);
		System.out.println();
		return true;
	}

	public void austeilen()
	{
		for(int index=0;index<7;index++)
		{
			for (Spieler sp : mitspieler)
			{
				Card karte = abheben();  // Remove von Stapel
				sp.aufnehmen(karte);		 // Add karte -> Hand
				
			}
		}
		Card temp=abheben();
		ablageStapel.add(temp);
		
	}
	
	
	
	

}
