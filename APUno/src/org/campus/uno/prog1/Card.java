package org.campus.uno.prog1;

public class Card
{
	private String farbe;
	private int zahl;
	
	public Card (String kartenfarbe, int kartenzahl)		//Konstruktor
	{
		farbe=kartenfarbe;
		zahl=kartenzahl;
	}
	
	
	boolean match (Card karteAufHand)
	{
		
		if(farbe  == karteAufHand.farbe || zahl==karteAufHand.zahl)
		{
			return true;
		}
		else
		{
			
			return false;
		}
	}
	
	
	public boolean compare (Card zweiteKarte)
	{
		if(farbe==zweiteKarte.farbe )
		{
			boolean result = zahl<zweiteKarte.zahl;
			return result;
			
//			return zahl<zweiteKarte.zahl;
		}
		else
		{
			
			return farbe.compareTo(zweiteKarte.farbe) >0 ;
		}
		
		
	}
	
	
	
	public String toString()
	{
		return farbe + zahl;
	}
	
	
	
	
}
