package org.campus.uno.prog1;

import java.util.ArrayList;

public class Spieler
{
	private String name;
	
	private ArrayList<Card> handkarten = new ArrayList<Card>();
	
	public Spieler(String name)
	{
		this.name=name;
	}
	
	public void aufnehmen(Card karte)
	{
		handkarten.add(karte);
	}
	
	public String toString()
	{
		return name;
	}
	
	public Card passendeKarte(Card vergleich)
	{
		for (Card karte : handkarten)
		{
			if(karte.match(vergleich))
			{
				handkarten.remove(karte);
				if(handkarten.size()==1)
				{
					System.out.println("UNO!");
				}
					return karte;
			}
			
		}		
		
		
		return null;
	}
	
	
	public int anzahlHandkarten()
	{
		return handkarten.size();
	}
	

	

}
