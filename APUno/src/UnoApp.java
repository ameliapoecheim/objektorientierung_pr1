import org.campus.uno.prog1.*;

public class UnoApp
{

	public static void main(String[] args)
	{
		Unospiel meinSpiel = new Unospiel();

		Spieler sp1 = new Spieler("Remy LeBeau");
		Spieler sp2 = new Spieler("Maverick");
		Spieler sp3 = new Spieler("Adrian Monk");

		meinSpiel.mitSpielen(sp1);
		meinSpiel.mitSpielen(sp2);
		meinSpiel.mitSpielen(sp3);
		meinSpiel.mitSpielen(new Spieler("Twisted Fate")); // Gleichwertig wie Spieler sp4 = new Spieler("Curry");

		meinSpiel.austeilen();

		int counter = 0;
		while (meinSpiel.spielZug())
		{
			counter++;
		}
		System.out.println("Das Spiel endete nach "+counter+" Z�egen");
		// System.out.println();
		// System.out.println(sp1.passendeKarte(new Unokarte("rot",1))); //Funktioniert
		// noch nit?!?!

		// KartenStapel abheben

		// Unokarte abgehobeneKarte = meinSpiel.abheben();
		// System.out.println(abgehobeneKarte);
		// System.out.println(meinSpiel.abheben());

	}

}
