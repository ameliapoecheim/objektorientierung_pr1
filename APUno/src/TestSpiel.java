

import org.campus.uno.prog1.*;

public class TestSpiel
{

	public static void main(String[] args)
	{
		Card karte1bl = new Card ("blau", 1);
		Card karte2bl = new Card ("blau", 2);
		Card karte3bl = new Card ("blau", 3);
		Card karte4bl = new Card ("blau", 4);
		Card karte5bl = new Card ("blau", 5);
		Card karte6bl = new Card ("blau", 6);
		Card karte7bl = new Card ("blau", 7);
		Card karte8bl = new Card ("blau", 8);
		Card karte9bl = new Card ("blau", 9);
		
		Card karte1ro = new Card ("rot", 1);
		Card karte2ro = new Card ("rot", 2);
		Card karte3ro = new Card ("rot", 3);
		Card karte4ro = new Card ("rot", 4);
		Card karte5ro = new Card ("rot", 5);
		Card karte6ro = new Card ("rot", 6);
		Card karte7ro = new Card ("rot", 7);
		Card karte8ro = new Card ("rot", 8);
		Card karte9ro = new Card ("rot", 9);
		
		Card karte1ge = new Card ("gelb", 1);
		Card karte2ge = new Card ("gelb", 2);
		Card karte3ge = new Card ("gelb", 3);
		Card karte4ge = new Card ("gelb", 4);
		Card karte5ge = new Card ("gelb", 5);
		Card karte6ge = new Card ("gelb", 6);
		Card karte7ge = new Card ("gelb", 7);
		Card karte8ge = new Card ("gelb", 8);
		Card karte9ge = new Card ("gelb", 9);
		
		Card karte1gr = new Card ("gruen", 1);
		Card karte2gr = new Card ("gruen", 2);
		Card karte3gr = new Card ("gruen", 3);
		Card karte4gr = new Card ("gruen", 4);
		Card karte5gr = new Card ("gruen", 5);
		Card karte6gr = new Card ("gruen", 6);
		Card karte7gr = new Card ("gruen", 7);
		Card karte8gr = new Card ("gruen", 8);
		Card karte9gr = new Card ("gruen", 9);
		
		
//		System.out.println(karte1ro);
		
		System.out.println(karte8gr.compare(karte2gr));
		
//		System.out.println(ausgabe(karte1ro.match(karte1bl)));
//		System.out.println(ausgabe(karte1ro.match(karte8gr)));
//		System.out.println(ausgabe(karte1ge.match(karte8ge)));
		
	}
	
	public static String ausgabe(boolean passt)
	{
		if (passt)
		{
			return "Karte passt.";
		}
		
		else
		{
			return "Karten passen nicht.";
		}
	}

}
