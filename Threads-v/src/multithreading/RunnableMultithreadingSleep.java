package multithreading;

public interface RunnableMultithreadingSleep
{

	public static void main(String[] args)
	{
		Thread t1 = new Thread(new TimerRunnable());
		Thread t2 = new Thread(new TimerRunnable());
		Thread t3 = new Thread(new TimerRunnable());
		
		t1.start();
		t2.start();
		t3.start();
		
	}
}