package multithreading;

public class MultithreadSleep
{

	public static void main(String[] args)
	{
	Thread t1 = new TimerThread();
	Thread t2 = new TimerThread();
	Thread t3 = new TimerThread();
	Thread t4 = new TimerThread();
	Thread t5 = new TimerThread();
	
	t1.start();
	t2.start();
	t3.start();
	t4.start();
	t5.start();
	
	try
	{
		//Hauptsthread schlafen legen nach 10 sekunden, dann gehts wieder weiter
		Thread.sleep(10000);
	} catch (InterruptedException e)
	{
		e.printStackTrace();
	}

	t1.stop(); //streicht er durch, weil "Holzhammermethode" einen Thread zu beenden - sollte eigentlich nicht passieren
	t2.stop(); // beendet die anderen Threads
	t3.stop();
	t4.stop();
	t5.stop();
	System.out.println("Fertig");
	}

}
