import java.util.Scanner;

public class Console
{

	public static void main(String[] args)
	{
		System.out.println("Eingabe");
		Scanner vonConsole = new Scanner(System.in);
		
		int vonConsoleGelesen = vonConsole.nextInt();
		System.out.println(vonConsoleGelesen);
		
		System.out.print("Geben sie ihren Namen ein:");
		
		String name=vonConsole.next();
		System.out.printf("Hallo %s, du hast vorher %d eingegeben. :-D", name, vonConsoleGelesen);
		
		vonConsole.close();
		
		
	}

}
