package org.campus02.oop;

public class Person
{

	private String firstname;
	private String lastname;
	private char gender;
	private int age;
	private String country;
	private int salary;
	private String eyeColor;
	private int weight;
	private int size;

	public Person (String firstname, String lastname, char gender, int age, String country, int salary, String eyeColor, int weight, int size)
	{
		this.firstname=firstname;
		this.lastname=lastname;
		this.gender=gender;
		this.age=age;
		this.country=country;
		this.salary=salary;
		
		switch(eyeColor)
		{
		case "blau": this.eyeColor=eyeColor;
		break;
		case "braun": this.eyeColor=eyeColor;
		break;
		case "gr�n": this.eyeColor=eyeColor;
		break;
		default: this.eyeColor="undefiniert";
		break;
		}
		
		this.weight=weight;
		this.size=size;
	}

	public String getFirstname()
	{
		return firstname;
	}

	public void setFirstname(String firstname)
	{
		this.firstname = firstname;
	}

	public String getLastname()
	{
		return lastname;
	}

	public void setLastname(String lastname)
	{
		this.lastname = lastname;
	}

	public char getGender()
	{
		return gender;
	}

	public void setGender(char gender)
	{
		this.gender = gender;
	}

	public int getAge()
	{
		return age;
	}

	public void setAge(int age)
	{
		this.age = age;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public int getSalary()
	{
		return salary;
	}

	public void setSalary(int salary)
	{
		this.salary = salary;
	}

	public String getEyeColor()
	{
		return eyeColor;
	}

	public void setEyeColor(String eyeColor)
	{
		this.eyeColor = eyeColor;
	}

	public int getWeight()
	{
		return weight;
	}

	public void setWeight(int weight)
	{
		this.weight = weight;
	}

	public int getSize()
	{
		return size;
	}

	public void setSize(int size)
	{
		this.size = size;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((eyeColor == null) ? 0 : eyeColor.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + gender;
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + salary;
		result = prime * result + size;
		result = prime * result + weight;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (age != other.age)
			return false;
		if (country == null)
		{
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (eyeColor == null)
		{
			if (other.eyeColor != null)
				return false;
		} else if (!eyeColor.equals(other.eyeColor))
			return false;
		if (firstname == null)
		{
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (gender != other.gender)
			return false;
		if (lastname == null)
		{
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (salary != other.salary)
			return false;
		if (size != other.size)
			return false;
		if (weight != other.weight)
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "Person [firstname=" + firstname + ", lastname=" + lastname + ", gender=" + gender + ", age=" + age
				+ ", country=" + country + ", salary=" + salary + ", eyeColor=" + eyeColor + ", weight=" + weight
				+ ", size=" + size + "]";
	}

}
