package at.campus02.ASCII_Art;

import at.campus02.ASCII_Art.*;
import java.util.*;

public class DemoApp
{

	public static void main(String[] args)
	{	Scanner groe=null;
		Scanner shape=null;
		
		try
		{
		while (true)
		{
			System.out.println("Bitte Gr��e des Bildes (Nur 3 oder ein vielfaches von 3 m�glich) festlegen");
			groe = new Scanner(System.in);
			int groesse = groe.nextInt();
			
			System.out.println("Bitte ausw�hlen, welches Shape (1-6) sie sehen m�chten?");
			shape = new Scanner(System.in);
			int auswahl = shape.nextInt();
			// .netLine();

			if (auswahl == 1)  // if(aus ==a||aus==A)
			{
				ShapeA A = new ShapeA(groesse);
				System.out.println(A.toString());
			} 
			else if (auswahl == 2)
			{
				ShapeB B = new ShapeB(groesse);
				System.out.println(B.toString());
			} else if (auswahl == 3)
			{
				ShapeC C = new ShapeC(groesse);
				System.out.println(C.toString());
			} else if (auswahl == 4)
			{
				ShapeD D = new ShapeD(groesse);
				System.out.println(D.toString());
			} else if (auswahl == 5)
			{
				ShapeE E = new ShapeE(groesse);
				System.out.println(E.toString());
			} else if (auswahl == 6)
			{
				ShapeF F = new ShapeF(groesse);
				System.out.println(F.toString());
			} else
			{
				System.out.println("FEHLER!!!!! Es gibt kein entsprechendes Shape. Bitte um Eingabe zwischen 1-6.");
				System.out.println();
			}
		}
		}
		finally {
			if(groe!=null)
			{
				groe.close();
			}
			if(shape!=null)
			{
				shape.close();
			}
		}
	}

}
