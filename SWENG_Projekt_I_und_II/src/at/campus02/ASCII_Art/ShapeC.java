package at.campus02.ASCII_Art;

public class ShapeC extends Shape
{

	public ShapeC(int groe)
	{
		super(groe);

		if (groe == 3)
		{
			raster[0][0] = "x";
			raster[0][1] = "x";
			raster[0][2] = "x";

			raster[1][0] = "x";
			raster[1][1] = " ";
			raster[1][2] = "x";

			raster[2][0] = "x";
			raster[2][1] = "x";
			raster[2][2] = "x";
		} 
		else if (groe > 3)
		{
			for (int z = 0; z < mult; z++)
			{
				for (int ctr = 0; ctr < mult; ctr++)
				{
					raster[z][ctr] = x;
				}
				for (int ctr = mult; ctr < mult*2; ctr++)
				{
					raster[z][ctr] = x;
				}
				for (int ctr = mult*2; ctr < mult*3; ctr++)
				{
					raster[z][ctr] = x;
				}
			}
			
			for (int z = mult; z < mult*2; z++)
			{
				for (int ctr = 0; ctr < mult; ctr++)
				{
					raster[z][ctr] = x;
				}
				for (int ctr = mult; ctr < mult*2; ctr++)
				{
					raster[z][ctr] = l;
				}
				for (int ctr = mult*2; ctr < mult*3; ctr++)
				{
					raster[z][ctr] = x;
				}
			}
			
			for (int z = mult*2; z < mult*3; z++)
			{
				for (int ctr = 0; ctr < mult; ctr++)
				{
					raster[z][ctr] = x;
				}
				for (int ctr = mult; ctr < mult*2; ctr++)
				{
					raster[z][ctr] = x;
				}
				for (int ctr = mult*2; ctr < mult*3; ctr++)
				{
					raster[z][ctr] = x;
				}
			}
		}
		else
		{
			System.out.println("Angegebene Gr��e der Matrix ist zu klein.");
		}
	}

}
