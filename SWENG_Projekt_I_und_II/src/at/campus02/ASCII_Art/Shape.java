package at.campus02.ASCII_Art;

public class Shape
{	
	protected String [][] raster;		// Initialisiert die 2D Matrix
	protected int gr;					// Variable f�r die Gr��e des Arrays
	protected String x="x";				// String f�r das "X"
	protected String l=" ";				// String f�r der Leerzeichen
	protected int mult;					// Multiplikator um den Inhalt zu vervielfachen
	
	
	public Shape(int groe)			// Groe�e wird mitgegeben um eine Matrix mit dieser Groe�e zu erzeugen 
	{
		raster = new String[groe][groe];
		gr=groe;					
		mult=groe/3;				// Faktor wird berechnet um das 3x3 Grundmuster vervielfachen
		
	}


	public String toString()   // Gibt das beschriebene Array aus.   !!!! 2.Ansatz Grundfigur und Skalierung in der toString()
	{
		String shape="";
		for(int x = 0; x<gr;x++)
		{
			for(int y=0; y<gr;y++)
			{
//				System.out.print(raster[x][y]);
				shape+=raster[x][y];				// In der Schleife wird Der String immer um einen Wert in einer Zeile erweitert
			}
			shape+="\n";						// F�gt dem String einen Zeilenumbruch hinzu um in der n�chsten Zeile weiterzuschreiben
//			System.out.println();
		}
		
//		for (int z = 0; z < mult; z++)
//		{
//			for (int ctr = 0; ctr < mult; ctr++)
//			{
//					shape+=raster[z][0];
//			}
//		}
//		shape+="\n";
//		for (int z = 0; z < mult; z++)
//		{
//			for (int ctr = mult; ctr < mult * 2; ctr++)
//			{
//				shape+=raster[z][1];
//			}
//		}
//		shape+="\n";
//		for (int z = 0; z < mult; z++)
//		{
//			for (int ctr = mult * 2; ctr < mult * 3; ctr++)
//			{
//				shape+=raster[z][2];
//			}
//			
//		}
		shape+="\n";
		return shape;		// Gibt zusammengest�pselten String aus
	}
}
