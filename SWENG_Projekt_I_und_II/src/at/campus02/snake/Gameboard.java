package at.campus02.snake;

public class Gameboard
{
	String[][] board;
	String snake="S ";
	String border = "X ";
	String leer="  ";
	
	int xb;
	
	int xs=1;
	int ys=1;
	

	public Gameboard(int xb)
	{
		this.xb=xb;
		xs=xb/2;				// Legt x Startkoordinate fest
		ys=xb/2;				// Legt y Startkoordinatefest
		
		
		board = new String[xb][xb];				// board mit x y Achse mit der �bergebenen Gr��e xb
		
		for (int x = 0; x < board.length; x++)
		{
			for (int y = 0; y < board[0].length; y++)
			{
				if (x == 0 || x == board.length-1)
				{
					board[x][y] = border;
				} 
				else if (y == 0 || y == board[0].length-1)
				{
					board[x][y] = border;
				}
				else
				{
					board[x][y]=leer;
				}
			}
		}
		board[xs][ys] = snake;
	}

	public void moveLeft()
	{
		if(!board[xs][ys-1].contains(border))
		{
			board[xs][ys-1]=snake;	// "S "
			board[xs][ys]=leer;	// "  "
			ys=ys-1;
		}
		
	}
	public void moveRight()
	{
		if(!board[xs][ys+1].contains(border))
		{
			board[xs][ys+1]=snake;
			board[xs][ys]=leer;
			ys=ys+1;
		}
	}
	public void moveUp()
	{
		if(!board[xs-1][ys].contains(border))
		{
			board[xs-1][ys]=snake;
			board[xs][ys]=leer;
			xs=xs-1;
		}
	}
	public void moveDown()
	{
		if(!board[xs+1][ys].contains(border))
		{
			board[xs+1][ys]=snake;
			board[xs][ys]=leer;
			xs=xs+1;
		}
	}
	
	private void clear()
	{
		System.out.println(new String(new char[70]).replace("\0", "\r\n"));
	}

	public String toString()
	{
		clear();
		for(int x=0; x<board.length;x++)
		{
			for(int y = 0; y<board[0].length;y++)
			{
				System.out.print(board[x][y]);
			}
			System.out.println();
		}
		
		return "";
	}
}
