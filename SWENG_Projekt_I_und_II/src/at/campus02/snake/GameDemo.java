package at.campus02.snake;

import java.util.Scanner;

public class GameDemo
{
	public static void main(String[] args)
	{
		boolean exit=false;
		
		System.out.println("Bitte um Feldgroesse > 5");
		Scanner gr = new Scanner(System.in);
		int groesse= gr.nextInt();
		Gameboard g = new Gameboard(groesse);
			System.out.println();
		g.toString();
		
		Scanner eingabe= new Scanner(System.in);
		try
		{
		
		while(exit!=true)
		{
			
			String richtung=eingabe.nextLine();
			
			if(richtung.equals("a")||richtung.equals("A"))
			{
				g.moveLeft();
			}
			else if(richtung.equals("s")||richtung.equals("S"))
			{
				g.moveDown();
			}
			else if(richtung.equals("d")||richtung.equals("D"))
			{
				g.moveRight();
			}
			else if(richtung.equals("w")||richtung.equals("W"))
			{
				g.moveUp();
			}
			else if(richtung.equals("exit"))
			{
				exit=true;
			}
			else
			{
				System.out.println("FALSCH");
			}
		
			g.toString();
		
		}
		}
		finally
		{
			if(gr!=null)
			{
				gr.close();
			}
			if(eingabe!=null)
			{
				eingabe.close();
			}
			
		}
	}

}
