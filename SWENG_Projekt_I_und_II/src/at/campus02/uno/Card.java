package at.campus02.uno;

import java.util.Arrays;

public class Card
{
	protected char[][] card;
	protected char cardNumber;
	protected Integer number;
	protected Character cardColor;
	protected String action;

	// Konstroktor f�r eine Farben - Zahlen Karte (zb.: R 1)

	public Card(char color, Integer number)
	{
		this.cardColor = color;
		this.number = number;
		this.cardNumber = number.toString().charAt(0);

		card = new char[5][9];

		generateBorder();
	}

	// Konstruktor f�r eine Farbige Aktionskarte ( zb: R +2)

	public Card(char color, String action)
	{
		this.cardColor = color;
		this.action = action;

		card = new char[5][9];

		generateBorder();
	}

	// Konstroktor f�r eine Aktionskarte ohne Farbe ( +4 / Farbwechsel)

	public Card(String action)
	{

		this.action = action;

		card = new char[5][9];

		generateBorder();
	}

	// Konstruktor erzeugt folgende Karte
	/*
	 * xxxxxxxxx x x x x x x xxxxxxxxx
	 * 
	 */

	public void generateBorder()
	{
		for (int y = 0; y < 5; y++) // Bef�lt den Rand der Karte mit X
		{
			for (int x = 0; x < 9; x++)
			{
				if (x == 0 || x == 8) // Array ist 9 breit aber das Array z�hlt von 0-8
				{
					card[y][x] = 'X';
				} else if (y == 0 || y == 4)
				{
					card[y][x] = 'X';
				} else
				{
					card[y][x] = ' ';
				}
			}
		}
	}

	boolean match(Card karteAufHand)
	{
		if(karteAufHand.action == "WldDr" && action != "Wld" || karteAufHand.action == "Wld" && action != "WldDr")
		{
			return true;
		}
		else if (cardColor == karteAufHand.cardColor || number == karteAufHand.number || action == karteAufHand.action && action!= null)
		{
			return true;
		} 
		
		else
		{
			return false;
		}
	}

	// getter

	public int getRowLength()
	{
		return 9;
	}

	public int getColLength()
	{
		return 5;
	}

	public char[][] getCard()
	{
		return card;
	}

	public boolean isActionCard()
	{
		if(this.action.isEmpty())
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public String getContent()
	{
		if (null == action)
		{
			return String.format(" %c %d ", cardColor, number);
		} else if (action.equals("WldDr")) // +4 & Farbwechsel String action farbe=null,
		{
			return String.format("WldDr");
		} else if (action.equals("Wld"))
		{
			return String.format(" Wld ");
		} else if (number == null) // Farbigen Aktionskaren zb Blau +2
		{
			return String.format("%c %s", cardColor, action);
		} else
		{
			return String.format("Fehler bei der Karte");
		}
	}

	// Gibt die Karte als String aus, (sys out - karte)
	// kann nicht benutzt werden um mehrere Karten nebeneinander abzubilden
	// Mehrere werden dann untereinander in der Console wiedergegeben

	public String toString()
	{ // cardColor, number, action char, int, string
		if (null == action)
		{
			return String.format("%c %d", cardColor, number);
		} else if (number == null && cardColor == null) // +4 & Farbwechsel String action farbe=null,
		{
			return String.format("%s", action);
		} else if (number == null) // Farbigen Aktionskaren zb Blau +2
		{
			return String.format("%c %s", cardColor, action);
		} else
		{
			return String.format("Fehler bei der Karte");
		}

		// String karteVertikal = "";
		//
		// for (int y = 0; y < card.length; y++)
		// {
		// karteVertikal += "\n";
		//
		// for (int x = 0; x < card[0].length; x++)
		// {
		// karteVertikal += card[y][x];
		// }
		//
		// }
		//
		// return karteVertikal;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + Arrays.deepHashCode(card);
		result = prime * result + ((cardColor == null) ? 0 : cardColor.hashCode());
		result = prime * result + cardNumber;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (action == null)
		{
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (!Arrays.deepEquals(card, other.card))
			return false;
		if (cardColor == null)
		{
			if (other.cardColor != null)
				return false;
		} else if (!cardColor.equals(other.cardColor))
			return false;
		if (cardNumber != other.cardNumber)
			return false;
		if (number == null)
		{
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		return true;
	}
}
