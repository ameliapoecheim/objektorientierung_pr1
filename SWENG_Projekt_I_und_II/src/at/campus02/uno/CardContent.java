package at.campus02.uno;

public class CardContent extends Card
{
	private char color;
	private int number;
	private String action;
	
	
	
		// Generiert Karte mit Farbe und Zahlenwert
	public CardContent(char color, int number)
	{
		super(color, number);
		
		this.color = color;
		this.number = number;
		
		card[2][3] = cardColor;		//Bef�llt die Karte mit der �bergebenen Farbe und 0
		card[2][5] = cardNumber;
		
	}
		// Generiert Aktionskarte Mit Farbe und der Aktion
	public CardContent(char color, String action)
	{
		super(color, action);

		this.color = color;
		this.action = action;

		card[2][2] = color;					// Farbe
		card[2][4] = action.charAt(0);
		card[2][5] = action.charAt(1);		// Bef�llt das Feld mit einem Char bezogen auf den String
		card[2][6] = action.charAt(2);      // String darf maximal 3 Zeichen lang sein
		
	}
	
		// Zum erzeugen einer 4+ Karte oder Farbwechsel-karte
	public CardContent(String action)
	{
		super(action);
		
		this.action = action;
		
		if(action.length() == 5)		// 4+  -  WildDraw -> WldDr
		{
			card[2][2] = action.charAt(0);
			card[2][3] = action.charAt(1);
			card[2][4] = action.charAt(2);
			card[2][5] = action.charAt(3);
			card[2][6] = action.charAt(4);
		}
		else if ( action.length()==3)	// Farbwechsel  -  Wild -> Wld
		{
			card[2][3] = action.charAt(0);
			card[2][4] = action.charAt(1);
			card[2][5] = action.charAt(2);
		}
		else		// Error f�r faslche Eingabe ( G�ltig: Wld / WldDr)
		{
			card[2][3] = 'E';
			card[2][4] = 'R';
			card[2][5] = 'R';
			card[1][4] = 'O';
			card[3][4] = 'O';
		}
		
	}
	
	
	public char getColor()
	{
		return this.color;
	}
	
	public int getNumber()
	{
		return this.number;
	}
	
	public String getAction()
	{
		return this.action;
	}

}
