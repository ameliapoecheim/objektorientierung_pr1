package at.campus02.uno;

import java.util.ArrayList;
import java.util.Collections;

public class UnoGameManagement
{
	// Verwaltet Spieler, KartenStapel und AblageStapel

	private ArrayList<Gambler> gamblers = new ArrayList<>();

	private ArrayList<Card> discardDeck = new ArrayList<>();
	public ArrayList<Card> deck = new ArrayList<>();

	// Methoden:
	// Konstruktor - (bef�llt den kartenStapel mit allen Karten und mischt diese)

	public UnoGameManagement()
	{
		ArrayList<String> actionsCards = new ArrayList<>();
		actionsCards.add("Rev");
		actionsCards.add("Skp");
		actionsCards.add(" +2");
		actionsCards.add("Wld");
		actionsCards.add("WldDr"); // ArrayList von Aktionskarten, damit man mit einer Schleife alle generieren
									// kann.

		for (int i = 0; i < 10; i++) // Schleife f�r Farbige Zahlen 0-9, wobei 0er gibt es nur 1x pro Farbe
		{
			deck.add(new CardContent('R', i));
			deck.add(new CardContent('G', i));
			deck.add(new CardContent('B', i));
			deck.add(new CardContent('Y', i));

			if (i > 0)
			{
				deck.add(new CardContent('R', i));
				deck.add(new CardContent('G', i));
				deck.add(new CardContent('B', i));
				deck.add(new CardContent('Y', i));
			}
		}

		for (int ctr = 0; ctr < 2; ctr++)
		{
			for (int i = 0; i < 3; i++)
			{
				deck.add(new CardContent('R', actionsCards.get(i))); // Alle farbigen AktionsKarten
				deck.add(new CardContent('G', actionsCards.get(i)));
				deck.add(new CardContent('B', actionsCards.get(i)));
				deck.add(new CardContent('Y', actionsCards.get(i)));
			}
		}

		for (int ctr = 0; ctr < 4; ctr++)
		{
			for (int i = 3; i < 5; i++)
			{
				deck.add(new CardContent(actionsCards.get(i))); // +4 und Farbwechsel
			}
		}

		Collections.shuffle(deck); // Mischt das Deck
	}

	// mitspielen - f�gt Spieler in die ArrayList hinzu

	public void addGambler(Gambler g)
	{
		if (4 >= gamblers.size())
		{
			gamblers.add(g);
		} else
		{
			System.out.println("Maximale Spieleranzahl erreicht.");
		}
		
		Collections.shuffle(gamblers);		// Mischt die Spieler
	}

	// austeilen - gibt jedem Spieler 7 Karten
	// aufgedeckte Karte auf den
	// AblageStapel, wenn es keine Aktionskarte ist

	public void dealOutCards()
	{
		for (int i = 0; i < 7; i++)
		{
			for (Gambler gmbl : gamblers)
			{
				Card card = drawCard();
				gmbl.cardPickup(card);
			}
		}

		Card temp = drawCard();	
		
		while(temp.isActionCard() == true)		// Sucht solange nach einer Karte die keine Aktionskarte ist
		{		
			deck.add(temp);
			temp = drawCard();
		}
		
		discardDeck.add(temp);		// Dann wird Karte als oberste offene Karte auf den ABlagestapel gelegt
	}

	// abheben == drawCard

	public Card drawCard()
	{
		if (0 == deck.size())
		{
			Card showedCard = discardDeck.remove(0);

			Collections.shuffle(discardDeck);

			deck.addAll(discardDeck);
			discardDeck.clear();
			discardDeck.add(showedCard);
		}

		return deck.remove(0);
	}

	// ablegen == putCard- f�gt die Karte dem AblageStapel an der Stelle 0 hinzu

	public void putCard(Card cardToPut)
	{
		discardDeck.add(0, cardToPut);
	}

	// Spielzug - Logik muhaha ^^

	public boolean gameTurn()
	{
		Gambler currentGambler = gamblers.remove(0);
		Card foundCard = currentGambler.matchingCard(discardDeck.get(0));
		
//		___________________________________________________________
		
		
		
		
//		___________________________________________________________
		
		
		return true;
	}

}
