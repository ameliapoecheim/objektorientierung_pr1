package at.campus02.uno;

import java.util.ArrayList;

public class BotA extends Gambler implements BotInterface
{
	protected String name;
	protected ArrayList<Card> handCards = new ArrayList<>();	
	
	//Methodenerklärum im BotInterface zu finden
	
	public BotA(String name)
	{
		super(name);
		
	}
	
	public void cardPickup(Card unoCard)
	{
		handCards.add(unoCard);
	}
	
	public Card matchingCard(Card cardToCompare)
	{
		for (Card card : handCards)
		{
			if (card.match(cardToCompare))
			{
				handCards.remove(card);
				if (handCards.size() == 1)
				{
					System.out.println("UNO!");
				}
				return card;
			}
		}
		return null;
	}
	
	@Override
	public String getDecision()
	{
		if(handCards.size() == 2)
		{
			return "uno";
		}
		
		
		return null;
	}

}
