package at.campus02.uno;

public interface BotInterface
{
	// getDecision liefert als String gew�nschte Karte zum Ablegen zur�ck. zb R 1 und matching gibt dann die karte an das spiel zur�ck
	
	// 3 Methoden:
	//  1. damit Bots Karten in die Hand nehmen k�nnen
	//  2. sucht anhand des implementierten Algorithmus eine Karte und wandelt getDecision in eine Stelle im Array der Handkarten um.
	//     z.B.: R 1 -> handCards[3];  return handCards[3];
	//  3. damit sie eine Ablegeentscheidung treffen und darauf folgend eine Karte aus ihrer Hand w�hlen k�nnen
	
	void cardPickup(Card unoCard);
	
	public Card matchingCard(Card cardToCompare);
	
	String getDecision();
	
}
