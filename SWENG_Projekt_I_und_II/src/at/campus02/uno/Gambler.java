package at.campus02.uno;

import java.util.ArrayList;
import java.util.Scanner;

public class Gambler
{
	protected String name;
	private ArrayList<Card> handCards = new ArrayList<>();

	// TODO: Datenbankanbindungen m�ssen noch implementiert werden. mit Highscores
	// Bots

	public Gambler(String name)
	{
		this.name = name;
	}

	public void cardPickup(Card unoCard)
	{
		handCards.add(unoCard);
	}

	// TODO implementieren

	public String getDecision()
	{
		System.out.println("Bitte um Aktion. (play, draw, uno oder help sind m�glich)");
		Scanner decis = new Scanner(System.in);
		String decision = decis.nextLine();
		decis.close();
		return decision;
		
	}
	
	
	
	public Card matchingCard(Card cardToCompare)
	{
		System.out.println("Bitte um gew�nschte Karte. Format 1-x. Z.B.: 4  == 4.Karte der Hand");
		Scanner decis = new Scanner(System.in);
		String decision = decis.nextLine();
		//  = R 5
		//  = Wld
		//  = WldDr
		//  = Y Rev
		Card temp = convertStringToCard(decision);
		
		
			for (Card card : handCards)
			{
				
				if(cardToCompare.match(temp) && card.match(temp))
				{
					Card result = card;
					handCards.remove(card);
					return result;
				}
				else
				{
					
				}
			}
		decis.close();
		return null;
		

	}
	
	private Card convertStringToCard(String input)
	{
		if(input.equals("WldDr"))
		{
			CardContent temp = new CardContent("WldDr");
			return temp;
		}
		
		else if(input.equals("Wld"))
		{
			CardContent temp = new CardContent("WldDr");
			return temp;
		}
			
		else if(input.length()==5)
		{
			char color = input.charAt(0);
			String action = "" + input.charAt(2) + input.charAt(3) + input.charAt(4);
			
			CardContent temp = new CardContent(color, action);
			return temp;
		}
		
		else if(input.length()==3)
		{
			
			char color = input.charAt(0);
			char digit = input.charAt(2);		
			int number = Character.getNumericValue(digit);			// Konvertiert Char -> Int
			
			CardContent temp = new CardContent(color, number);
			return temp;
		}
		
		else
		{
			System.out.println("Error - convertStrigToCard");
			return null;
		}
			
//		char col = input.charAt(0);
//		String action = "" + input.charAt(2) + input.charAt(3) + input.charAt(4);
//		
//		CardContent temp = new CardContent(col, action);
//		
//		return temp;
	}
	
	public int getHandSize()
	{
		return handCards.size();
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((handCards == null) ? 0 : handCards.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gambler other = (Gambler) obj;
		if (handCards == null)
		{
			if (other.handCards != null)
				return false;
		} else if (!handCards.equals(other.handCards))
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public String toString()
	{
		return name;
	}

}
