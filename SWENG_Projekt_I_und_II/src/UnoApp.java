import java.util.ArrayList;
import java.util.Scanner;

import at.campus02.uno.*;

public class UnoApp {

	// �berlegungen:
	// java.lang.object  // StringUtils
	
	
	public static void main(String[] args) {
		
		UnoGameManagement mngm = new UnoGameManagement();
		
		
//		System.out.println("Bitte deinen Spielernamen eingeben.");
//		Scanner entry = new Scanner(System.in);
//		String name = entry.nextLine();
//		mngm.addGambler(new Gambler(name));
//		mngm.addGambler(new BotA("BotA"));
//		
//		mngm.dealOutCards();
		
		

		
//		try
//		{
//			while (mngm.gameTurn())
//			{
//
//			} 
//		} 
//		
//		
//		finally
//		{
//			entry.close();
//		}
		
		
		CardContent b1 = new CardContent('B', 1);
		CardContent g4 = new CardContent('G', 4);
		CardContent r7 = new CardContent('R', 7);
		CardContent b0 = new CardContent('B', 0);
		CardContent wild = new CardContent("Wld");
		
		Gambler a = new Gambler("Albert");
		a.cardPickup(wild);
		a.cardPickup(g4);
		a.cardPickup(r7);
		a.cardPickup(b0);
		
		System.out.println(a.matchingCard(b1));
				
		
		
		
//		printHandCards(mngm.deck);
//		
//		ArrayList<Card> handArray = new ArrayList<>();
//
//		CardContent y_Rev = new CardContent('Y', "Rev");
//		CardContent wild = new CardContent("Wld");
//		CardContent wildDraw = new CardContent("WldDr");
//		CardContent y_Skp = new CardContent('Y', "Rev");
//
//		CardContent b1 = new CardContent('B', 1);
//		CardContent g4 = new CardContent('G', 4);
//		CardContent r7 = new CardContent('R', 7);
//		CardContent b0 = new CardContent('B', 0);
//
//		handArray.add(y_Rev);
//		handArray.add(wildDraw);
//		handArray.add(wild);
//		handArray.add(b1);
//		handArray.add(g4);
//		handArray.add(r7);
//		handArray.add(b0);
		
		
//		handArray.add(b0);
//		handArray.add(b0);
//		handArray.add(b0);
//		handArray.add(b0);
//		handArray.add(b0);
//		handArray.add(b0);
//		handArray.add(b0);
//		handArray.add(b0);
//		handArray.add(b0);
//		handArray.add(b0);
//		handArray.add(b0);
//		handArray.add(b0);
//		handArray.add(b0);

//		printHandCards(handArray);

		// char[][] hand = new char[5][10*handArray.size()-1]; // Alte Darstellung f�r
		// Karten
		// handArrayBefuellen(hand, handArray);
		// printCharArray(hand);

	}

	// St�pselt die Karteninfos zu einer sch�nen Ausgabe zusammen

	public static void printHandCards(ArrayList<Card> handArray)
	{
		for(int i = 0; i < handArray.size() * 5; i++)
		{
			if(i == 0 || i == 4)
			{
				
				System.out.println();
				
				for(int ctr = 0; ctr < handArray.size(); ctr++)
				{
					System.out.print("xxxxxxxxx ");
				}
			}
			else if (i == 1 || i == 3)
			{
				System.out.println();
				for(int ctr = 0; ctr < handArray.size(); ctr++)
				{
					System.out.print("x       x ");
				}
			}
			else if (i == 2)
			{
				System.out.println();		// System.out.print("\n");
				for(int ctr = 0; ctr < handArray.size(); ctr++)
				{
					System.out.printf("x %s x ", handArray.get(ctr).getContent());
				}
			}
		}
		
		
	}

	// !!!!! Alte Methode, gerade nicht in Ben�tzung !!!!!!!

	// Wir habe eine ArrayList welche die Handkarten verwaltet.
	// Damit die Handkarten in der Konsole nebeneinander angezeigt werden k�nnen,
	// wird ein 2D Array erstellt das
	// Reihe f�r Reihe mit den Karten bef�llt wird

	public static char[][] handArrayBefuellen(char[][] hand, ArrayList<Card> handArray) {

		for (int y = 0; y < hand.length; y++) {
			for (int x = 0; x < hand[0].length; x++) {
				if (0 <= x && x < handArray.get(0).getRowLength()) {
					hand[y][x] = handArray.get(0).getCard()[y][x];
				}

				else if (1 + handArray.get(0).getRowLength() <= x && x <= handArray.get(0).getRowLength() * 2) {
					hand[y][x] = handArray.get(1).getCard()[y][x - 10];
				}

				else if (2 + handArray.get(0).getRowLength() * 2 <= x && x <= handArray.get(0).getRowLength() * 3 + 1) {
					hand[y][x] = handArray.get(2).getCard()[y][x - 20];
				} else if (3 + handArray.get(0).getRowLength() * 3 <= x
						&& x <= handArray.get(0).getRowLength() * 4 + 2) {
					hand[y][x] = handArray.get(3).getCard()[y][x - 30];
				} else if (4 + handArray.get(0).getRowLength() * 4 <= x
						&& x <= handArray.get(0).getRowLength() * 5 + 3) {
					hand[y][x] = handArray.get(4).getCard()[y][x - 40];
				} else if (5 + handArray.get(0).getRowLength() * 5 <= x
						&& x <= handArray.get(0).getRowLength() * 6 + 4) {
					hand[y][x] = handArray.get(5).getCard()[y][x - 50];
				} else if (6 + handArray.get(0).getRowLength() * 6 <= x
						&& x <= handArray.get(0).getRowLength() * 7 + 5) {
					hand[y][x] = handArray.get(6).getCard()[y][x - 60];
				}

				// ^ Sind f�r 7 Karten auf der Hand

				else if (7 + handArray.get(0).getRowLength() * 7 <= x && x <= handArray.get(0).getRowLength() * 8 + 6) {
					hand[y][x] = handArray.get(7).getCard()[y][x - 70];
				} else if (8 + handArray.get(0).getRowLength() * 8 <= x
						&& x <= handArray.get(0).getRowLength() * 9 + 7) {
					hand[y][x] = handArray.get(8).getCard()[y][x - 80];
				} else if (9 + handArray.get(0).getRowLength() * 9 <= x
						&& x <= handArray.get(0).getRowLength() * 10 + 8) {
					hand[y][x] = handArray.get(9).getCard()[y][x - 90];
				} else if (10 + handArray.get(0).getRowLength() * 10 <= x
						&& x <= handArray.get(0).getRowLength() * 11 + 9) {
					hand[y][x] = handArray.get(10).getCard()[y][x - 100];
				} else if (11 + handArray.get(0).getRowLength() * 11 <= x
						&& x <= handArray.get(0).getRowLength() * 12 + 10) {
					hand[y][x] = handArray.get(11).getCard()[y][x - 110];
				} else if (12 + handArray.get(0).getRowLength() * 12 <= x
						&& x <= handArray.get(0).getRowLength() * 13 + 11) {
					hand[y][x] = handArray.get(12).getCard()[y][x - 120];
				} else if (13 + handArray.get(0).getRowLength() * 13 <= x
						&& x <= handArray.get(0).getRowLength() * 14 + 12) {
					hand[y][x] = handArray.get(13).getCard()[y][x - 130];
				}

				// ^ 7 weitere karten (insgesamt 14 karten abbildbar)

				else if (14 + handArray.get(0).getRowLength() * 14 <= x
						&& x <= handArray.get(0).getRowLength() * 15 + 13) {
					hand[y][x] = handArray.get(14).getCard()[y][x - 140];
				} else if (15 + handArray.get(0).getRowLength() * 15 <= x
						&& x <= handArray.get(0).getRowLength() * 16 + 14) {
					hand[y][x] = handArray.get(15).getCard()[y][x - 150];
				} else if (16 + handArray.get(0).getRowLength() * 16 <= x
						&& x <= handArray.get(0).getRowLength() * 17 + 15) {
					hand[y][x] = handArray.get(16).getCard()[y][x - 160];
				} else if (17 + handArray.get(0).getRowLength() * 17 <= x
						&& x <= handArray.get(0).getRowLength() * 18 + 16) {
					hand[y][x] = handArray.get(17).getCard()[y][x - 170];
				} else if (18 + handArray.get(0).getRowLength() * 18 <= x
						&& x <= handArray.get(0).getRowLength() * 19 + 17) {
					hand[y][x] = handArray.get(18).getCard()[y][x - 180];
				} else if (19 + handArray.get(0).getRowLength() * 19 <= x
						&& x <= handArray.get(0).getRowLength() * 20 + 18) {
					hand[y][x] = handArray.get(19).getCard()[y][x - 190];
				} else if (20 + handArray.get(0).getRowLength() * 20 <= x
						&& x <= handArray.get(0).getRowLength() * 21 + 19) {
					hand[y][x] = handArray.get(20).getCard()[y][x - 200];
				}

				// ^ 7 weitere Karten ( 21 maximal m�glich darzustellen )

			}
		}

		return hand;
	}

	// Methode um ein beliebiges 2D Array in der Console auszugeben

	public static void printCharArray(char[][] array) {
		for (int y = 0; y < array.length; y++) {
			for (int x = 0; x < array[0].length; x++) {
				System.out.print(array[y][x]);
			}
			System.out.print("\n");
		}
	}

}
