package org.campus.person;
import java.util.ArrayList;




public class Person
{
	private String vorname;
	private String nachname;
	private Person mutter;
	private Person vater;		// Um darauf zuzugreifen braucht man einen "Setter" oder "Getter"
//	private Person kinder;
	
	private ArrayList<Person> kind; // = new ArrayList<Person>();
	
	
	private static int anzahlPersonen; //Statische variablen gibt es 1x pro klasse; sonst gelten sie im ganzen Projekt
	
	public Person(String vor, String nachname) 
	{
		vorname=vor;
		this.nachname=nachname;			// Zuweisung bei gleichen Namen. Sonst unklar was wem zugewiesen wird.
										// Am besten anders bennenen.

		
				
		anzahlPersonen++;
		
	}
	
	public void setVater(Person vater)
	{
		this.vater=vater;
	}
	
	public void setMutter(Person mutter)
	{
		this.mutter = mutter;
	}
	
	
	
	public void setKind(ArrayList<Person> kind)
	{
		this.kind=kind;

			
	}


		
	public Person getVater()
	{
		return vater;
	}
	
	
	public String toString()
	{
		return vorname  + "," + nachname + "," + kind;
				
		
		
//		if(mutter==null && vater==null)
//		{
//			return String.format("(%s %s(M? V? ))", vorname, nachname);
//		}
//		else if (mutter==null)
//		{
//			return String.format("(%s %s(M? %s))", vorname, nachname, vater);
//		}
//		else if (vater==null)
//		{
//			return String.format("(%s %s(%s V?))", vorname, nachname, mutter);
//		}
//		
//		return String.format("(%s %s(%s %s)) %d Personen", vorname, nachname, mutter, vater, anzahlPersonen);
	}
	
	

}
