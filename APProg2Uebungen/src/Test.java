import java.util.Iterator;
import java.util.LinkedList;

import at.campus02.studentenverwaltung.*;

public class Test
{

	public static void main(String[] args)
	{
		LinkedList<Student> studs = new LinkedList<Student>();
		
		Student a=new Student("Max", 19);
		Student b=new Student("Phil", 39);
		Student c=new Student("Cornelia", 25);
		Student d=new Student("Maria", 22);
		Student e=new Student("Thomas", 18);
		Student f=new Student("Julia", 20);
		
		studs.addFirst(a);
		studs.addLast(b);
		studs.addFirst(c);
		studs.addLast(d);
		studs.addLast(e);
		studs.addLast(f);
				
		Iterator<Student> pointer = studs.iterator();   // studs.iterator		// Definieren eines Iterators
		
		while(pointer.hasNext())				//Schleife zur Ausgabe der Liste Mittels Iterators
		{

//			Student s = pointer.next();
//			System.out.println("Zugriff mittels Object-Person "+s.getName());		// Spring Variable mit Ausgabe
			
			System.out.println("Direkter Zugriff "+pointer.next().getName());		// Ausgabe der Liste mit 1 Befehlszeile
			
//			System.out.println(pointer.next());		// Ausgabe aller Objekte und all ihrer Eigenschaften
		}
		
		System.out.println();
		
//		#########################################################################
		
//		pointer = studs.listIterator();   // studs.iterator		// Neu initiieren f�r Wiederverwendung der Liste
//		
//		while(pointer.hasNext())				//Schleife zur Ausgabe der Liste Mittels Iterators
//		{

//			Student s = pointer.next();
//			System.out.println("Zugriff mittels Object-Person "+s.getName());		// Spring Variable mit Ausgabe
			
//			System.out.println("Direkter Zugriff "+pointer.next().getName());		// Ausgabe der Liste mit 1 Befehlszeile
			
//			System.out.println(pointer.next());		// Ausgabe aller Objekte und all ihrer Eigenschaften
//		}
		
//		#########################################################################
		
		
		System.out.println();
		
		
		for(int index=0; index<studs.size();index++)
		{
			System.out.println(studs.get(index));
		}
		
		
		
//		LinkedList<Object> mixed = new LinkedList<Object>();
		
//		Auto brum = new Auto("brumm");
//		Student a=new Student("Max", 19);
		
//		mixed.add(xx);
//		mixed.add(brum);
//		
//		for (Object object : mixed)
//		{
//			System.out.println(object);
//		}
		
		
		
	}

}
