

import at.campus02.uepersonen.Person;

public class PersonenApp
{

	public static void main(String[] args)
	{
		Person p1 = new Person("Junior", "Stiesel", 32);
		Person p2 = new Person("Senior", "Stiesel", 72);
		Person p3 = new Person("Junior", "Stiesel", 32);
		
		
		System.out.println(p1);  // Achtung bei String toString auf Vererbung aufpassen!!!
		p2.print();  System.out.println();
		
		if(p1==p3)
		{
			System.out.println("True - Sind gleich");
		}
		else
		{
			System.out.println("False - sind nicht gleich");
		}
		
		
		
		
		
		
		
	}

}
