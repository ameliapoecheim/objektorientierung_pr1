import at.campus02.konto.*;

public class KontoApp
{

	public static void main(String[] args)
	{
		konto Edna=new konto("Edna", 2000);
		
		Edna.setInhaber("Edna");
		
		Edna.aufbuchen(2500);
		Edna.kontoAbfragen();
		
		Edna.aufbuchen(25.3);
		Edna.kontoAbfragen();
		
		Edna.abbuchen(3000);
		Edna.kontoAbfragen();
		
		Edna.abbuchen(2525.3);
		Edna.kontoAbfragen();
		
	}

}
