//import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

import at.campus02.studentenverwaltung.*;

public class DemoStudentenKlasse
{

	public static void main(String[] args)
	{
		LinkedList<Student> studs = new LinkedList<Student>();
		
		Student a=new Student("Albert", 19);
		Student b=new Student("Phil", 39);
		Student c=new Student("Cornelia", 25);
		Student d=new Student("Maria", 22);
		Student e=new Student("Thomas", 18);
		Student f=new Student("Julia", 22);
		
		studs.addFirst(a);
		studs.addLast(b);
		
		studs.addFirst(c);
		
		studs.addLast(d);
		studs.addLast(e);
		studs.addLast(f);
		
//		for (Object name : studs)
//		{
//			System.out.println(name);
//		}
//		System.out.println();
		boolean equ= d.getAlter()==f.getAlter();
		System.out.println(equ);

		System.out.println(d.match(f));
		
		System.out.println();
		
		System.out.println(studs);
		System.out.println();

		ListIterator<Student> pointer = studs.listIterator();
		
		pointer.add(d);
		
		System.out.println(studs);
		System.out.println();
		
		System.out.println(pointer.next());
		System.out.println(pointer.next());
		pointer.remove();
		
		System.out.println();
		System.out.println(studs);

		pointer.add(c);

		System.out.println();
		System.out.println(studs);		
		System.out.println();
		
		pointer = studs.listIterator();
		while(pointer.hasNext())
		{
			System.out.println(pointer.next());
		}
		

		
//		ListIterator<Student> pointer = studs.listIterator();
//		while(pointer.hasPrevious())
//		{
//			
//		}
		
		
		
	}

}
