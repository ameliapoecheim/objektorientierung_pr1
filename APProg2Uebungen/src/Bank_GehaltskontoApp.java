import at.campus02.gehaltskonto.*;


public class Bank_GehaltskontoApp
{

	public static void main(String[] args)
	{
		
		Gehaltskonto k1=new Gehaltskonto("", "", "", 0);
		
		k1.addkonto("Edna", "iban123", "bi123c", 200);
		k1.addkonto("Harvey", "iban456", "bi456c", 50);
		k1.addkonto("Droggelbecher", "iban789", "bic789", 3000);
		
		k1.kontoAusgeben();
		k1.getKontostand();

	}

}
