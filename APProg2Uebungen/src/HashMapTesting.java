import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class HashMapTesting
{

	public static void main(String[] args)
	{
		HashMap<String, Integer> myMap1 = new HashMap<>();
		
		myMap1.put("Graz", 273838);
		myMap1.put("Deutschlandsberg", 8200);
		myMap1.put("Wolfsberg", 25038);
		myMap1.put("Klagenfurt", 100316);
		
		Set<String> mapkey = myMap1.keySet();
		
		for (String stadt : mapkey)	// Ausgabe der Stadt
		{
			System.out.println(stadt + " " + myMap1.get(stadt));
		}
		System.out.println();
		
		Collection<Integer> einwohner = myMap1.values();
		for (Integer anzahl : einwohner)					//Ausgabe der Einwohneranzahl
		{
			System.out.println("Einwohnerzahl " + anzahl);
		}
		System.out.println();
		
		Set<Entry<String, Integer>> keyValues = myMap1.entrySet();		// Ausgabe der Stadt + Einwohneranzahl
		for (Entry<String, Integer> eintrag : keyValues)
		{
			System.out.println("Stadt: "+eintrag.getKey()+", Einwohner: "+eintrag.getValue());
		}
		System.out.println();
		
		// Einwohner von Deutschlandsberg
		Integer d = myMap1.get("Deutschlandsberg");
		System.out.println("Einwohneranzahl von Deutschlandsberg: "+d);
		
		
		myMap1.remove("Wolfsberg");
		
		for (Entry<String, Integer> eintrag : keyValues)		// Liste nach Remove
		{
			System.out.println("Stadt: "+eintrag.getKey()+", Einwohner: "+eintrag.getValue());
		}
		System.out.println();
		
//		myMap1.clear()		leert die komplette Liste
		
		
	}

}
