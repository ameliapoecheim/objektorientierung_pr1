import java.util.HashSet;
import java.util.Iterator;

public class HashTests
{

	public static void main(String[] args)
	{
		HashSet<String> hs = new HashSet<>();
		hs.add("[String 1 vom HashSet]");
		hs.add("[String 2 vom HashSet]");
		hs.add("[String 3 vom HashSet]");
		
		Iterator<String> iter = hs.iterator();
		
		String newString = iter.next();
		System.out.println("next() ohne Schleife "+newString);
		String newString2 = iter.next();
		System.out.println("next() ohne Schleife "+newString2);
		
		System.out.println();
		System.out.println("--------Mit Schleife--------"); System.out.println();
		
		iter = hs.iterator();
		
		while(iter.hasNext())
		{
			String str = iter.next();
			System.out.println(str);
		}
		
		System.out.println();
		System.out.println("--------String 1 Removed--------"); System.out.println();
		
		hs.remove("[String 1 vom HashSet]");
		
		iter = hs.iterator();
		
		while(iter.hasNext())
		{
			String str = iter.next();
			System.out.println(str);
		}
		
		System.out.println();
		System.out.println("--------Contains & Remove--------"); System.out.println();
		
		if(hs.contains("[String 3 vom HashSet]"))
		{
		   hs.remove("[String 3 vom HashSet]");
		}
				
		iter = hs.iterator();
		
		while(iter.hasNext())
		{
			String str = iter.next();
			System.out.println(str);
		}	
			
	}
}
