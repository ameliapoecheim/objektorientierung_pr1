package UebungsBeispiele;

public class Ue3GrenzwertReihe
{

	public static void main(String[] args)
	{
		double wert=100;
		double grenz = 0.0001;
		
		System.out.printf("Der Grenzwert f�r eine �nderung '%.0f < %.4f' ist: %.2f", wert, grenz, findeGrenzwert(wert, grenz));
		
		
	}
	
	public static double findeGrenzwert(double zahl, double grenze)
	{
//		double grenze=0.0001;
		double result=0.0;
		int counter=0;
		
		for(double index=1.000;index<=zahl;index++)
		{
			result=result+ 1/index;
			
			if((1/(index)) - (1/(index+1)) <= grenze )
			{
				break;
			}
				
			counter++;
		}
		
		System.out.println(counter);
		return result;
	}
	
	
	

}
