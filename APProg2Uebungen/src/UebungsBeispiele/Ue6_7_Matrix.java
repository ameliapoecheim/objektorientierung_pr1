package UebungsBeispiele;

public class Ue6_7_Matrix
{

	public static void main(String[] args)
	{
		int matrixgroesse = 5;
//		int matrixstelle1=4;
//		int matrixstelle2=3;
//		int matrixbreite=6;
//		int matrixhoehe=4;
		
		
		generateIdentityMatrix(matrixgroesse);
		
//		printMatrix(matrixgroesse);
//		
//		System.out.println();
//		
//		searchMatrix(generateIdentityMatrix(matrixgroesse), matrixstelle1, matrixstelle2);
		
		
//		printMatrix2(matrixhoehe, matrixbreite);
		
		
	}
	
	
	public static double[][] generateIdentityMatrix(int size)  //Generiert eine Quadratische Matrix
	{
		double[][] matrix =new double[size][size];
		
		for(int matrix1=0;matrix1<size;matrix1++ )
		{
			for(int matrix0=0;matrix0<size;matrix0++)
			{
				if(matrix1==matrix0)
				{
					matrix[matrix1] [matrix0]= 1.0;
				}
				else
				{
					matrix[matrix1] [matrix0]= 0.0;
				}
								
				System.out.print(matrix[matrix1][matrix0]+"  ");
			}
			System.out.println();
		}
		
		return matrix;
	}
	
	public static void printMatrix(int size)		//Generiert eine Quadratische Matrix und gibt alle Felder aus
	{
		
		double[][] matrix =new double[size][size];
		
		for(int y=0;y<size;y++ )
		{
			for(int x=0;x<size;x++)
			{
				if(y==x)
				{
					matrix[y] [x]= 1.0;
				}
				else
				{
					matrix[y] [x]= 0.0;
				}
				System.out.print(matrix[y][x]+"  ");
			}
			System.out.println();
		}
	}
	
	public static void searchMatrix(double[][] matrix, int stelle1, int stelle2)
	{
		System.out.println("Der Inhalt an der Matrixstelle: ["+stelle1+"]"+"["+stelle2+"] ist: "+matrix[stelle1][stelle2]);
	}
	
	public static void printMatrix2(int size1, int size2)
	{
		
		double[][] matrix =new double[size1][size2];
		
		for(int matrix1=0;matrix1<size1;matrix1++ )
		{
			for(int matrix0=0;matrix0<size2;matrix0++)
			{
				if(matrix1==matrix0)
				{
					matrix[matrix1] [matrix0]= 1.0;
				}
				else
				{
					matrix[matrix1] [matrix0]= 0.0;
				}
				System.out.print(matrix[matrix1][matrix0]+"  ");
			}
			System.out.println();
		}
	}
	

}
