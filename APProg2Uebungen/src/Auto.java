
public class Auto
{
	private String name;

	public Auto (String name)
	{
		this.name=name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public String toString()
	{
		return name;
	}
}
