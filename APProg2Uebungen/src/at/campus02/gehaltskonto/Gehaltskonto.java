package at.campus02.gehaltskonto;

import java.util.ArrayList;;


public class Gehaltskonto
{
	private String inhaber;
	private String IBAN;
	private String BIC;
	private double konto;
	
	private ArrayList<Gehaltskonto> gesamt = new ArrayList<Gehaltskonto>();
	
	
	
	
	public Gehaltskonto(String besitzer, String iban, String bic, double kontostand)	//
	{
		inhaber=besitzer;
		IBAN=iban;
		BIC=bic;
		konto=0.0;
		
	}
	
	public Gehaltskonto addkonto(String besitzer, String iban, String bic, double kontostand)
	{
		Gehaltskonto einzel = new Gehaltskonto(besitzer, iban, bic, kontostand);
		gesamt.add(einzel);
		
		return einzel;
		
	}
	
	public void kontoAusgeben()
	{
		String aus="";
		for (Gehaltskonto geha : gesamt)
		{
			aus=aus+geha.toString()+"\n";
		}
		System.out.println(aus);
		
	}
		
	public void aufbuchen(double wert)
	{
		konto=konto+wert;
	}
	
	public void abbuchen(double wert)
	{
		 if(konto-wert<0)
		 {
			 System.out.print("Achtung mit der Abbuchung w�rde das Konto kleiner als 0� werden.\nEs is m�glich bis zu "+konto+"� abzuheben.\n");
		 }
		 else
		 {
			 konto=konto-wert;
		 }
			 
	}
	
	public void getKontostand()
	{
		System.out.println( inhaber + " "+ IBAN+" "+BIC+" "+konto);
	}
	
	public String toString()
	{
		return inhaber + " "+ IBAN+" "+BIC+" "+konto;
	}
	
}