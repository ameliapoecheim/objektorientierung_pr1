package at.campus02.studentenverwaltung;

public class Student
{
	private String name;
	private int alter;
	
	public Student( String name, int alter)
	{
		this.name=name;
		this.alter=alter;
	}
	
	public void setName(String name)
	{
		this.name=name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getAlter()
	{
		return alter;
	}
	
	public boolean match(Student st)
	{
		if(this.getName()==st.getName()||this.getAlter()==this.getAlter()) 
		{
			return true;
		}
		return false;
	}
	
//	Rechtsklick -> Source -> Generate Hashcode & equals
	
//	@Override
//	public int hashCode()
//	{
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + alter;
//		result = prime * result + ((name == null) ? 0 : name.hashCode());
//		return result;
//	}

//	@Override
//	public boolean equals(Object obj)
//	{
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		Student other = (Student) obj;
//		if (alter != other.alter)
//			return false;
//		if (name == null)
//		{
//			if (other.name != null)
//				return false;
//		} else if (!name.equals(other.name))
//			return false;
//		return true;
//	}

	public String toString()
	{
		return name + " "+alter;
	}
}
