package at.campus02.generics;

public class SozialesMayBe<T>
{
	private T data;
	private int status;
	
	public SozialesMayBe(T data)
	{
		this.data=data;
		this.status=1;
	}
	
	public void setStatus(int status)
	{
		if(status>=0 && status<=1)
		{
			this.status=status;
		}
	}
	
	public void print()
	{
		switch(status)
		{
		case 0:
			System.out.println("Nicht angezeigt.");
			break;
		case 1:
			System.out.println("Daten: "+data);
			break;
		case 3:
			System.out.println("Daten nicht gesetzt.");
			break;
		default:
			System.out.println("Daten unbekannt.");
			break;
		
		
		}
	}
	
	
	
}
