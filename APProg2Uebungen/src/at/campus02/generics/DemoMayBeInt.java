package at.campus02.generics;

public class DemoMayBeInt
{

	public static void main(String[] args)
	{
		MayBeInt<String> bla1 = new MayBeInt<String>("10",0);
		System.out.println(bla1.print());
		
		MayBeInt<Integer> bla2 = new MayBeInt<Integer>(100,1);
		System.out.println(bla2.print());
		
		MayBeInt<Double> bla3 = new MayBeInt<Double>(50.0,2);
		System.out.println(bla3.print());
		
		
//		MayBeIntbla1 = new MayBeInt(10,0);
//		System.out.println(bla1.print());
//		MayBeInt bla2 = new MayBeInt(100,1);
//		System.out.println(bla2.print());
//		MayBeInt bla3 = new MayBeInt(50,2);
//		System.out.println(bla3.print());
		
	}

}
