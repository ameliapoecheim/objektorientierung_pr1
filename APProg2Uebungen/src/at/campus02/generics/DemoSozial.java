package at.campus02.generics;
//import java.util.GregorianCalendar;

public class DemoSozial
{

	public static void main(String[] args)
	{
		SozialesProfil p1 = new SozialesProfil("Hansi", "Weg 1", 22, "hansi@bla.at", 'm', "1970, 01, 01", 1855.0);
		SozialesProfil p2 = new SozialesProfil("Cornelia", "Weg 2", 24, "cornelia@bla.at", 'f', "1968, 02, 02", 2451.0);
		SozialesProfil p3 = new SozialesProfil("Geralt", "Weg 3", 48, "geralt@bla.at", 'm', "1942,03,03", 5000);
		
		p1.setSalaryStatus(1);
		p3.setSalaryStatus(3);
		
		
		p1.print();
		p2.print();
		p3.print();
		

	}

}
