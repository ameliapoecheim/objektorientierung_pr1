package at.campus02.generics;

public class MayBeInt<T>
{
	private T data;
	private int status;

	public MayBeInt(T data, int status)
	{
		this.data = data;
		this.status = status;
	}

	public String print()
	{
		switch (status)
		{
		case 1:
			return "Zugriff gestattet: " + data;
		case 2:
			return "Zugriff nicht gestattet";
		case 3:
			return "Nicht erfasst";
		default:
			return "Ungültiger Status!";
		}
	}	
}

//public class MayBeInt
//{
//	private int data;
//	private int status;
//
//	public MayBeInt(int data, int status)
//	{
//		this.data = data;
//		this.status = status;
//	}
//
//	public String print()
//	{
//		switch (status)
//		{
//		case 1:
//			return "Zugriff gestattet: " + data;
//		case 2:
//			return "Zugriff nicht gestattet";
//		case 3:
//			return "Nicht erfasst";
//		default:
//			return "Ungültiger Status!!";
//		}
//	}	
//}
