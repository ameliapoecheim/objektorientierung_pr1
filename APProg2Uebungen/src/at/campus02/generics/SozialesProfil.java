package at.campus02.generics;
//import java.util.GregorianCalendar;

public class SozialesProfil
{
	private String name;
	private String address;
	private int age;
	private SozialesMayBe<String> email;
	private char gender;
	private SozialesMayBe<String> dateOfBirth;
//	private SozialesMayBe<GregorianCalendar> dateOfBirth;	
	private SozialesMayBe<Double> salary;
	
	public SozialesProfil(String name, String address, int age, String email, char gender, /* GregorianCalendar */String dateOfBirth, double salary)
	{
		this.name=name;
		this.address=address;
		this.age=age;
		this.email=new SozialesMayBe<String>(email);
		this.gender=gender;
		this.dateOfBirth=new SozialesMayBe<String>(dateOfBirth);
//		this.dateOfBirth=new SozialesMayBe<GregorianCalendar>(dateOfBirth);
		this.salary=new SozialesMayBe<Double>(salary);
	}
	
	public void setSalaryStatus(int status)
	{
		salary.setStatus(status);
	}
	
	public void print()
	{
		String text="Profil [name: "+name+" adress: "+address+" age: "+age+" gender: "+gender;
		System.out.println(text);
		this.email.print();
		this.dateOfBirth.print();
		this.salary.print();		
		System.out.println();
	}
	
}
