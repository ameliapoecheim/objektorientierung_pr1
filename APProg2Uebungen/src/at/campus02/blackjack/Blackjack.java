package at.campus02.blackjack;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class Blackjack
{
	private HashMap<Player, Integer> gamblers = new HashMap<Player, Integer>();
	
	public boolean add(Player player)
	{
		if(!gamblers.containsKey(player))
			{
				gamblers.put(player, 0);
				return true;
			}
		else
		{
			return false;
		}
	}
	
	public boolean addCard(Player player, Integer cardValue)
	{
		if(gamblers.containsKey(player))
		{
//			int endVal=getValue(player)+cardValue;
			int endVal = gamblers.get(player)+cardValue;
			
			gamblers.put(player, endVal);
			
			return true;
		}
		else
		{
		return false;
		}
	}
	
	public Integer getValue(Player player)
	{
//		int val = gamblers.get(player).intValue();
		return gamblers.get(player).intValue();
		
//		return gamblers.get(player);
	}

	public String toString()
	{
		String text="";
		Set<Entry<Player, Integer>> gamblerSet=gamblers.entrySet();
		for (Entry<Player, Integer> entry : gamblerSet)
		{
			text+=entry.getKey().getName()+", "+entry.getValue()+"\n";
		}
		return text;
			
//		return "Blackjack [gamblers=" + gamblers + "]";
	}
	
	

}
