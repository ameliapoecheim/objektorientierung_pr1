package at.campus02.konto;

public class konto
{
	
	private String inhaber;
	private double konto;
	
	public konto(String inhaber, int kontostand)
	{
		this.inhaber=inhaber;
		konto=kontostand;		
	}
	
	public void setInhaber(String inhaber)
	{
		this.inhaber=inhaber;
		konto=0;
	}
	
	public void aufbuchen(double betrag)
	{
		konto=konto+betrag;
	}
	
	public void abbuchen(double betrag)
	{
		if(konto<betrag)
		{
			System.out.print("Achtung mit der Abbuchung w�rde das Konto kleiner als 0� werden.\nEs is m�glich bis zu "+konto+"� abzuheben.\n");
		}
		else
		{
		konto=konto-betrag;
		}
	}
	
	public void kontoAbfragen()
	{
		System.out.println(inhaber + ": "+ konto+"�");
	}
	
	public String toString()
	{
		return inhaber+": "+konto;
	}
}