package at.campus02.emp;

public class Employee
{
	private int empNumber;
	private String name;
	private double salary;
	private String department;
	
	
	public Employee (int anzahl, String name, double gehalt, String depart)
	{
		empNumber=anzahl;
		this.name=name;
		salary=gehalt;
		department=depart;		
		
	}
	
	public void setSalary(double wert)
	{
		salary=wert;
	}
	
	public void setDepartment(String dep)
	{
		department=dep;
	}
	
	public int getNumber()
	{
		return empNumber;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public double getSalary()
	{
		return salary;
	}
	
	public String  getDepartment()
	{
		return department;
	}
	
	public String toString()
	{
		return empNumber + " " + this.name + " "+ salary+" "+department;
	}
	
	
	
}
