package at.campus02.emp;
import java.util.ArrayList;

public class EmployeeManager
{
	ArrayList <Employee> employees=new ArrayList<Employee>();
	
	public void addEmployee(Employee e)
	{
		employees.add(e);
	}

	public Employee findByMaxSalary()
	{
		Employee max=employees.get(0);  // 1. Employee aus der ArrayListe employees wird als Variable "max"genommen, 
										// um einen Vergleich zu haben
		
		for (Employee employee : employees)
		{
			if(employee.getSalary()>max.getSalary())	// Vergleich zw aktuellen Employee aus der Liste und unserer
														// Variable "max"
			{
				max=employee;
			}
			
		}
		return max;
		
	}
	
	public Employee findByEmpNumber(int number)
	{
		Employee gesucht=new Employee(0, "", 0,"");   // Neue Variable um das Suchergebnis zu speichern
		
		for (Employee emp : employees)
		{
			if(emp.getNumber()==number)
			{
				gesucht=emp;
			}	
		}
		return gesucht;
		
	}
	
	public ArrayList<Employee> findByDepartment(String department)
	{
		ArrayList<Employee> gesucht = new ArrayList<Employee>();	// Neue Liste zum speichern alles Gesuchten des gleichen Departments
		
		for (Employee emp : employees)
		{
			if(emp.getDepartment()==department)				// Vergleich ob der Employee dem gesuchten Department angeh�rt
			{
				gesucht.add(emp);
			}	
		}
		return gesucht;
	}
	
	
	
}
