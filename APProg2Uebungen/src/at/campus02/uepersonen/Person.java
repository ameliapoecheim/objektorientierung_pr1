package at.campus02.uepersonen;

public class Person
{
	
	private String vorname;
	private String nachname;
	private int alter;
	
	
	public Person(String vor, String nach, int alt)
	{
		vorname=vor;
		nachname=nach;
		alter=alt;
		
	}
	
	public void print()
	{
		System.out.println(vorname+" "+nachname+", "+alter+" Jahre");
		
		//Optional viele sch�ne if/else Bedingungen
	}


	public String toString()
	{
		return vorname+" "+nachname+", "+alter+" Jahre";
	}
}
