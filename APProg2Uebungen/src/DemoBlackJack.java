import at.campus02.blackjack.*;

public class DemoBlackJack
{

	public static void main(String[] args)
	{
		Player p1 = new Player("Jack", 21);
		Player p2 = new Player("Remy LeBeau", 32);
		Player p3 = new Player("TwistedFate", 25);
		
		Blackjack t1 = new Blackjack();
		
		t1.add(p1);
		t1.add(p2);
		t1.add(p3);
		
		System.out.println(t1);
		
		t1.addCard(p1, 2);
		t1.addCard(p2, 6);
		t1.addCard(p3, 10);
		
		System.out.println("Runde 2"); System.out.println();
		
		t1.addCard(p1, 9);
		t1.addCard(p2, 2);
		t1.addCard(p3, 11);
		
		System.out.println(t1);
		
		
	}

}
