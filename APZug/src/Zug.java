
public class Zug
{
	private Wagon erster;
	
	private Wagon letzter;
	
	private int zuglaenge;
	
	public Zug()
	{		
	}
	
	public void neuerWagon(String inhalt)
	{
		Wagon neuerWagon = new Wagon(inhalt);
		
		if(erster==null)		// null => Die Referenz zeigt noch ins Leere.
		{
			erster =neuerWagon;
			letzter=neuerWagon;
		}
		else		//Es gibt zumindest schon 1 Wagon
		{
			letzter.einhaengen(neuerWagon);
			letzter=neuerWagon;
		}
		
		zuglaenge++;
	}
	
	public String wagonInhaltAnStelle (int stelle) 
	{
		Wagon temp = erster;
		
		if(stelle>zuglaenge-1 || stelle<0)
		{
			return "Gefragte Stelle nicht vorhanden.";
		}
		
		for(int springen=0; springen<stelle; springen++)
		{
			temp=temp.nachbar();
		}
		
		return temp.toString();
	}
	
	
	
	public int length()
	{
		return zuglaenge;
	}
	
	public String toString()
	{
		
		if(erster==null)
		{
			return "0";
		}
		else
		{
			String ergebnis = "" + zuglaenge + ":";
			Wagon temp=erster;
			
			while(temp!=null)
			{
				ergebnis=ergebnis+" "+temp.toString();
				temp=temp.nachbar();
			}
			return ergebnis;
		}
		
	}
}
