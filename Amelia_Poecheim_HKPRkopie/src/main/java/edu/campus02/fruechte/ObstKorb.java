package edu.campus02.fruechte;

import java.util.ArrayList;

public class ObstKorb
{
	ArrayList<Frucht>korb=new ArrayList<Frucht>();
	public void fruchtAblegen(Frucht f)
	{
		korb.add(f);
	}

	public int zaehleFruechte(String sorte)
	{
		ArrayList<Frucht> sort = new ArrayList<Frucht>();
		
		for (Frucht fru : korb)
		{
			if(fru.getSorte()==sorte)
			{
				sort.add(fru);
			}
		}
		
		return sort.size();
	}

	public Frucht schwersteFrucht()
	{
		Frucht max = korb.get(0);
		
		for (Frucht fru : korb)
		{
			if(fru.getGewicht()>max.getGewicht())
			{
				max=fru;
			}
		}
		
		return max;
	}

	public ArrayList<Frucht> fruechteSchwererAls(double gewicht)
	{
		ArrayList<Frucht> vergl =new ArrayList<Frucht>();
		
		for (Frucht fru : korb)
		{
			if(fru.getGewicht()>gewicht)
			{
				vergl.add(fru);
			}
		}
		return vergl;
	}

	public int zaehleSorte()
	{
		ArrayList<String> anzahl = new ArrayList<String>();
		
		for (Frucht fru : korb)
		{
			if(!anzahl.contains(fru.getSorte()))
			{
				anzahl.add(fru.getSorte());
			}
		}
		
		return anzahl.size();
	}
	public String toString()
	{
		return String.format("%s", korb);
	}
}
