package edu.campus02.fruechte;

public class Frucht
{
	private String name;
	private double gewicht;
	
	public Frucht(String name, double gewicht)
	{
		this.name=name;
		this.gewicht=gewicht;
	}

	public double getGewicht()
	{
		return gewicht;
	}

	public String getSorte()
	{
		return name;
	}
	
	public String toString()
	{
		return String.format("(%s %.2f)", name, gewicht);
	}

}
