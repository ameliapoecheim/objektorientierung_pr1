package edu.campus02.dosen;

public class Keks
{
	private String name;
	private double gewicht;
	private double volumen;
	
	public Keks(String name, double gewicht, double volumen)
	{
		this.name=name;
		this.gewicht=gewicht;
		this.volumen=volumen;
	}

	public String getName()
	{
		return name;
	}

	public double getGewicht()
	{
		return gewicht;
	}

	public double getVolumen()
	{
		return volumen;
	}
		
	
}
