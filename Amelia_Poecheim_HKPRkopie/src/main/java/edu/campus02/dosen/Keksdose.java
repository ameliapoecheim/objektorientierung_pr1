package edu.campus02.dosen;
import java.util.ArrayList;
public class Keksdose
{
	private String farbe;
	private double breite;
	private double laenge;
	private double hoehe;
	
	private Keksdose verschachtelt;
	private Keksdose gr�sste;
	private Keksdose kleinste;
	
	ArrayList<Keks> kekseges = new ArrayList<Keks>();
	
	public Keksdose(String farbe, double x, double y, double hoehe)
	{
		this.farbe=farbe;
		this.hoehe=hoehe;
		if(x<y)
		{
			breite=x;
			laenge=y;
		}
		else
		{
			breite=y;
			laenge=x;
		}
	}

	public double getBreite()
	{
		return breite;
	}

	public double getLaenge()
	{
		return laenge;
	}

	public double getHoehe()
	{
		return hoehe;
	}

	public double volumen()
	{
		return laenge*breite*hoehe;
	}

	public double freiesVolumen()
	{
		double gesVol =0;
		for (Keks kek : kekseges)
		{
			gesVol+=kek.getVolumen();
		}
		
		return volumen()-gesVol;
	}

	public boolean keksAblegen(Keks keks)
	{
		if(keks.getVolumen()<=freiesVolumen())
		{
			kekseges.add(keks);
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean leer()
	{
		if(kekseges.isEmpty())
		{
			return true;
		}
		return false;
	}

	public boolean stapeln(Keksdose d)
	{
		this.gr�sste=this;
		if(this.leer()==true && this.getBreite()>d.getBreite() && this.getLaenge()>d.getLaenge() && this.getHoehe()>d.getHoehe())
		{
			verschachtelt=d;
			kleinste=d;
			return true;
		}
		else
		{
		return false;
		}
	}

	public double gesamtVolumen()
	{
		double result=0;
		
		result+=volumen()+verschachtelt.gesamtVolumen();
		
		return result;
	}
	public String toString()
	{
		return String.format("(%s %.1f x %.1f x %.1f)", farbe, breite, laenge, hoehe);
	}
}
