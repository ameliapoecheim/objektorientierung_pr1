package at.campus02.nowa.ss2018.pr3.networkio3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class PingPongClient
{

	public static void main(String[] args) throws UnknownHostException, IOException
	{
	
//		Socket client = new Socket("172.32.0.213", 1111);
		Socket client = new Socket("localhost", 1111);

		try (BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
				PrintWriter writer = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));
				BufferedReader answerReader = new BufferedReader(new InputStreamReader(client.getInputStream()));)
		{

			String line;
			while ((line = consoleReader.readLine()) != null)
			{
				if (line.equalsIgnoreCase("exit"))
				{
					break;
				}
				writer.println(line);
				writer.flush();

				String answer = answerReader.readLine();
				System.out.println("Reply from server: " + answer);

			}
		}
	}

}
