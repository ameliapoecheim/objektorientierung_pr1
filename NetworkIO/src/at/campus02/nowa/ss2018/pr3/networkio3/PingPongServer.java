package at.campus02.nowa.ss2018.pr3.networkio3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class PingPongServer
{
	// TODO: Server funktioniert noch nicht

	public static void main(String[] args) throws IOException
	{
		// ServerSocket wartet auf Request
		ServerSocket serverSocket = new ServerSocket(1111);
		// Schliefe -> Reactor
		while (true)
		{
			// wartet auf Verbindung
			Socket socket = serverSocket.accept();
			System.out.println("Accepted: " + socket);

			// Trywithresources -> resources werden immer geschlossen
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
					PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));)
			{

				String line;
				while ((line = reader.readLine()) != null)
				{
					System.out.println("Received: " + line);
					if (line.equalsIgnoreCase("ping"))
					{
						writer.println("pong");
					} else if (line.equalsIgnoreCase("pong"))
					{
						writer.println("ping");
					} else
					{
						writer.println("what?");
					}

					// er macht das paket zu, obwohl es noch nicht voll ist.
					writer.flush();
				}
			} catch (SocketException e)
			{
				System.out.println("Client disconnected");
			}

			catch (IOException e)
			{
				e.printStackTrace();
			}
		}

	}

}
