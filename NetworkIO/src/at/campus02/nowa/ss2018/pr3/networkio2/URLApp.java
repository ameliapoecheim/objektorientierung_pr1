package at.campus02.nowa.ss2018.pr3.networkio2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class URLApp
{

	public static void main(String[] args) throws IOException
	{
		URL url = new URL("https://wetter.orf.at");
		
		InputStream stream = url.openStream();
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		
		String line;
		
		while((line = reader.readLine()) != null)
		{
			System.out.println(line);
		}

	}

}
