package at.campus02.nowa.ss2018.pr3.networkio2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketApp
{

	public static void main(String[] args) throws UnknownHostException, IOException
	{
		
		//socket -> objekt, dass verbindung herstellt zu einem Server: 't�r�ffner'
		Socket socket = new Socket("wetter.orf.at", 80);
		
		//um Request verschicken zu k�nnen, brauch ich einen Outputstream. �ffnet Verbindung vom Client zum Server
		OutputStream outputStream = socket.getOutputStream();
		
		//Inhalt des Requests
		String requestLine1 = "GET /steiermark/prognose HTTP/1.1\r\n";
		String requestLine2 = "Host: orf.at\r\n\r\n";
		
		//�ber den writer wird der Request an den Server geschickt:
		//BufferedWriter (kann ganze Zeilen lesen) erstellen
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream)); //OUtputStreamWriter kann nur einzelne Zeichen lesen (characters - char)
		//BufferedWriter writer schreibt die Zeilen des Requests
		writer.write(requestLine1);
		writer.write(requestLine2);
		// schickt Request - Die Zeilen, die der writer vorher geschrieben hat
		writer.flush();
		
		//Antwort von Server wird ausgelesen:
		//erstellen von Inputstream mit socket
		InputStream inputStream = socket.getInputStream();
		//reader liest die Zeilen aus vom inputstream
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		
		//gibt Zeile f�r Zeile die Antwort von Server aus
		//line verwandelt die Antwort von Server(durch reader gelesen) in String.
		//solange line einen Inhalt hat, wird dieser in Console ausgegeben
		String line; 
		while ((line = reader.readLine()) != null)
		{
			System.out.println(line);
		}
		
//		Alternative zur Ausgabe auf Console:
//		boolean  isRunning = true;
//		while(isRunning) {
//			String line = reader.readLine();
//				if(line == null)
//				{
//					isRunnung = false;
//					break;
//				}
//				System.out.println(line);
//			}
		
		socket.close();
	}
}
