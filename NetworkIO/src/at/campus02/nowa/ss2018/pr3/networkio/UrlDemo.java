package at.campus02.nowa.ss2018.pr3.networkio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class UrlDemo
{

	public static void main(String[] args) throws MalformedURLException
	{
		URL u = new URL("http://orf.at/");
		try(BufferedReader br = new BufferedReader(new InputStreamReader(u.openStream()))){
			String line;
			while ((line = br.readLine())!= null)
			{
				System.out.println(line);
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
