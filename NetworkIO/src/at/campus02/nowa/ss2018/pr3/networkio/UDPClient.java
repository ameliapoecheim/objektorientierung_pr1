package at.campus02.nowa.ss2018.pr3.networkio;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDPClient
{
	public static void main(String[] args) throws IOException
	{

		//Client erstellen
		DatagramSocket client = new DatagramSocket();
		
		//Daten, die verschickt werden sollen
		String input = "abcde";
		
		//byte arrays als puffer f�r die zu versendenden Daten und Daten, die empfangen werden
		byte[] sendData = new byte[1024];
		byte[] receiveData = new byte[1024];
		
		//Inetadress.getlocalHost -> wenn ich lokalen host verwende
		//leeres Paket erstellen und paket bef�llen
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getLocalHost(), 1111);
		sendPacket.setData(input.getBytes());
		
		//paket senden
		client.send(sendPacket);
	
		//antwort von server verwalten
		//leeres Paket wird vorbereitet zum Empfangen
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
		
		//Paket wird empfangen
		client.receive(receivePacket);
		
		//Daten von Paket werden in String umwandeln
		String received = new String(receivePacket.getData());
		
		//in Console ausgeben
		System.out.println("Reply from server: " + received);
	}
}
