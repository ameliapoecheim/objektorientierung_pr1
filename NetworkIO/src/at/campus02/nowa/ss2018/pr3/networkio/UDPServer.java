package at.campus02.nowa.ss2018.pr3.networkio;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDPServer
{

	public static void main(String[] args) throws IOException
	{
		// server erstellen auf Port 1111
		DatagramSocket server = new DatagramSocket(1111);

		while (true)
		{
			byte[] receiveData = new byte[1024];
			byte[] sendData = new byte[1024];

			// Packet muss leer hergestellt werden und braucht immer einen buffer
			DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

			//Client schickt Daten und hier empf�ngt der Server ein paket und liest sie ein --> data werden geholt
			server.receive(receivePacket);
			
			//Informationen aus dem Paket k�nnen ausgelesen werden
			byte[] received = receivePacket.getData();
			String s = new String(received);
			InetAddress senderAddress = receivePacket.getAddress(); //InetAdress brauche ich damit ich die Adresse von Paket bekommen
			int senderPort = receivePacket.getPort();

			// hier werden die Informationen bearbeitet
			String toSend = s.toUpperCase();

			//neues Paket, dass geschickt werden soll, wird so erstellt, damit man wei� wohin es geschickt werden soll(senderAddress, senderPort)
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, senderAddress, senderPort);
			sendPacket.setData(toSend.getBytes()); // String wird in bytes gewandelt
			
			// Paket wird zur�ckgeschickt 
			server.send(sendPacket);
		}
	}

}
