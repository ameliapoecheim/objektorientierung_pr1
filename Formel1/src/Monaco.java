
public class Monaco
{

	public static void main(String[] args)
	{
		Auto auto1 = new Auto("Rot");
		Auto auto2 = new Auto("Eisblau");
//		Auto auto3 = auto1;			// Entspricht auto1, auto1 und auto3 zeigen auf die gleichen Variablen
									// -> auto1 wird vollgetankt, dann wird auch auto3 vollgetankt
		
	//	Auto[];
		
		auto1.status();
		
		auto1.volltanken();
		auto1.status();
		
		auto2.status();
		
		auto2.volltanken();
		auto2.status();
		auto2.beschleunigen(50);
		auto2.fahren(1800);
		auto2.status();
		
		auto1.beschleunigen(10);
		auto1.status();
		
		auto1.fahren(120);
		auto1.status();
		
		auto1.langsamer(20);
		auto1.status();
		
		auto2.langsamer(60);
		auto2.status();
		
	}
	
//	public static void test()
//	{
//		
//		Auto auto2 = new Auto("Eisblau");		// 
//	}
//	
	
	
	

}
