
public class Auto
{
	private String farbe;
	private double tankstand;
	private double geschwindigkeit;
	
	public Auto(String autofarbe)		// Konstruktor (void fehlt)
	{
		farbe=autofarbe;
	}
	
	public void volltanken()
	{
		tankstand=100;
	}
	
	public void beschleunigen(double gas)
	{
		geschwindigkeit= geschwindigkeit+gas;
		if(geschwindigkeit>350)
		{
			geschwindigkeit=350;
		}
		if (tankstand<=0)
		{
			tankstand=0;
			geschwindigkeit=0;
		}
		checkStatus();		
	}
	
	public void langsamer(double bremsen)
	{
		geschwindigkeit=geschwindigkeit-bremsen;
		if(geschwindigkeit<=0)
		{
			geschwindigkeit=0;
		}
		if (tankstand<=0)
		{
			tankstand=0;
			geschwindigkeit=0;
		}
		checkStatus();
		
	}
	
	public void fahren(double zeit)		//Sekunden
	{
		tankstand=tankstand-(geschwindigkeit*zeit)/18000;
		checkStatus();
	}
	
	public void status()
	{
		System.out.println("Farbe: " + farbe + "  Geschw.: " + geschwindigkeit + " Tank: " + tankstand);
	}
	
	private void checkStatus()
	{
		if (tankstand<=0)
		{
			tankstand=0;
			geschwindigkeit=0;
		}
		
		
	}
	
	public String toString()
	{
		return farbe + " " + geschwindigkeit;
	}
	
}
