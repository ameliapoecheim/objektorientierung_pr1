package at.campus02.ase.books;

import java.util.ArrayList;

public class Bibliothek
{
	private ArrayList <Book> bibliothek = new ArrayList<Book>();
//	private ArrayList <Book> mostWanted = new ArrayList<Book>();
	

	public void addBook(Book b)
	{
		bibliothek.add(b);
	}

	public int countPages()
	{
		int summe=0;
		
		
		for (Book book : bibliothek)
		{
			summe=summe+book.getSeiten();
		}
		
		return summe;
	}

	public double avaragePages()
	{
		double summe=0;
		
		for (Book book : bibliothek)
		{
			summe=summe+book.getSeiten();
		}
		
		summe=summe/bibliothek.size();
		
		return summe;
	}

	public ArrayList<Book> booksByAuthor(String autor)
	{
		ArrayList<Book> neu = new ArrayList<Book>();
	
		for (Book book : bibliothek)
		{

			if(autor==book.getAutor())
			{
				neu.add(book);
			}
			else if(book.getAutor()==null)
			{
				return null;
			}
			
		}
		
		return neu;
	}

	public ArrayList<Book> findBook(String search)
	{
		ArrayList<Book> funde = new ArrayList<Book>();
		
		for (Book book : bibliothek)
		{			
			if(book.match(search)==true)
			{
				funde.add(book);
			}
	
		}
		
		return funde;
	}

}
