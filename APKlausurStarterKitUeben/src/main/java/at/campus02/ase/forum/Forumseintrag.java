package at.campus02.ase.forum;

import java.util.ArrayList;

public class Forumseintrag
{
	private String titel;
	private String text;
	
	private ArrayList<Forumseintrag> Eintraege = new ArrayList<Forumseintrag>();
	
	public Forumseintrag(String titel, String text)
	{
		this.titel=titel;
		this.text=text;
		
	}

	public Forumseintrag antworten(String titel, String text)
	{
//		Neuer Forumseintrag angelegt
//		Eintrag in Liste speichern   .add?
//		R�ckgabe des Eintrags
		
		
		Forumseintrag antwort= new Forumseintrag(titel, text);
		
		Eintraege.add(antwort);
		
		return antwort;
	}

	public ArrayList<Forumseintrag> getAntworten()
	{
//		Soll die Liste an Eintr�gen zur�ckliefern
		
		ArrayList <Forumseintrag> ListeAntworten = new ArrayList<Forumseintrag>();
		
		for (Forumseintrag f : Eintraege)
		{
				ListeAntworten.add(f);
		}
		
		
		return ListeAntworten;
	}

	public int anzahlDerEintraege()
	{
		int counter=0;
		for (Forumseintrag forumseintrag : Eintraege)
		{
			if(forumseintrag.getAntworten().size()>0)
			{
				counter=counter+forumseintrag.getAntworten().size()+1;

			}
			else
			{
				counter++;
			}
		}
		
		
		return counter;
	}
	
	public String toString()
	{
//				return String.format("(%s %s(M? %s))", vorname, nachname, vater);
//		eintrag [antwort]
//		eintrag []
		
		String ausgabe="";		
		
		for (Forumseintrag fe : Eintraege)
		{
			ausgabe=ausgabe+fe.toString();
		}
		
		if(!ausgabe.isEmpty())
		{
			ausgabe="["+ausgabe+"]";
		}
		
		return String.format("(%s,%s)%s",titel, text, ausgabe);
	}
}
