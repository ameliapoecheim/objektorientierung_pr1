import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class HelloWorld
{
	public static void main(String[] args)
	{
		
		System.out.println("Hello Campus02!");
		
		try { 
			// 1a: Jar-Files referenzieren - Java Build Path
			// 1b. access driver laden
			// versucht diese Klasse Ucanaccess zu finden,  CASE SENSITIVE!!
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver"); // Driver laden
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}

		try
		{ // 2. verbindung herstellen
			// get Connection (Driver muss vorher geladen werden)
			Connection con = DriverManager.getConnection("jdbc:ucanaccess://E:/SE/DBP/Urlaubsverwaltung.accdb"); // CASE SENSITIVE!!

			System.out.println("Super - hat funktioniert :-) ");

		} catch (SQLException e)
		{
			e.getStackTrace();
		}
	}
}
