
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.naming.spi.DirStateFactory.Result;

import net.ucanaccess.converters.Metadata;

public class MyDBHelper
{

	private Connection con = null;

	public void init()
	{

		try
		{
			// 1a. Jar-Files referenzieren - Java Build Path
			// 1b. Load Access Driver
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		} catch (ClassNotFoundException e)
		{

			e.printStackTrace();
		}

		try
		{

			// 2. get Connection to database
			con = DriverManager
					.getConnection("jdbc:ucanaccess://C:/Users/amelia.poecheim/Desktop/DBP/Urlaubsverwaltung.accdb");

			System.out.println("You are connected :-D \n");

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public void close()
	{
		if (con != null)
			try
			{
				con.close();
				System.out.println("\nYou are disconnected *wave*");
			} catch (SQLException e)
			{
				e.printStackTrace();
				System.out.println("connection error");
			}
	}

	public Student getStudentWithStudentDetails(int studentid)
	{
		Student s = new Student();

		// Select s.*, u.* FROM s join urlaub u on s.studentid=u.studentid
		
		String query = "SELECT Vorname, Credits, Ort, Reihung, Preis FROM Student, Urlaubswunsch "
				+ "WHERE Student.StudentID = Urlaubswunsch.StudentID AND Student.StudentID=?";
		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, studentid);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
			Urlaub u = new Urlaub();
			s.setVorname(rs.getString(1));
			s.setCredits(rs.getInt(2));
			
			u.setDestination(rs.getString(3));
			u.setPrio(rs.getInt(4));
			u.setPrice(rs.getDouble(5));
			
			s.addUrlaub(u);
			}
			

		} catch (SQLException e)
		{

			e.printStackTrace();
		}

		return s;
	}

	public HashMap<String, String> getColumnNamesForTableStudentFromMetadata()
	{
		HashMap<String, String> columnNames = new HashMap<>();
		try
		{
			ResultSet rs = con.prepareStatement("SELECT * FROM Student").executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			for (int i = 1; i <= meta.getColumnCount(); i++)
			{
				columnNames.put(meta.getColumnLabel(i), meta.getColumnTypeName(i));

			}

		} catch (SQLException e)
		{

			e.printStackTrace();
		}
		return columnNames;
	}

	public void printColumnNamesForTableStudentFromMetadata()
	{
		String query = "SELECT * FROM Student";
		// int ctr=0;
		try
		{
			ResultSet rs = con.createStatement().executeQuery(query);
			ResultSetMetaData meta = rs.getMetaData();

			for (int i = 1; i <= meta.getColumnCount(); i++)
			{
				System.out.println(meta.getColumnLabel(i) + " " + meta.getColumnTypeName(i));
				if (meta.isSigned(i))
				{
					// ctr++;
				}

			}

		} catch (SQLException e)
		{

			e.printStackTrace();
		}
	}

	public int deleteStudent(int studentId)
	{
		int affectedRows = 0;
		// 1a - update Statement
		String deleteString = "";
		deleteString = "DELETE FROM STUDENT ";
		deleteString += " WHERE StudentId=?";

		try
		{
			// 1b Prepared Statement
			PreparedStatement stmtDelete = con.prepareStatement(deleteString);

			// 1c - Parameter setzen
			stmtDelete.setInt(1, studentId);

			affectedRows = stmtDelete.executeUpdate();

		} catch (SQLException e)
		{

			e.printStackTrace();
		}
		return affectedRows;

	}

	// helper.updateStudentWhereFirstnameLike("%a%", 45);
	// helper.updateStudentWhereFirstnameLike("a%", 45);
	// helper.updateStudentWhereFirstnameLike("%a", 45);
	public int updateStudentWhereFirstnameLike(String firstnameContainsLetter, double newCredit)
	{

		int affectedRows = 0;
		// 1a - update Statement
		String updateString = "";
		updateString = "UPDATE Student SET ";
		updateString += " Credits = credits + ? ";
		updateString += " WHERE vorname like ?";

		try
		{
			// 1b Prepared Statement
			PreparedStatement stmtUpdate = con.prepareStatement(updateString);

			// 1c - Parameter setzen
			stmtUpdate.setDouble(1, newCredit);
			stmtUpdate.setString(2, firstnameContainsLetter);

			affectedRows = stmtUpdate.executeUpdate();

		} catch (SQLException e)
		{
			//
			e.printStackTrace();
		}
		return affectedRows;

	}

	public int addNewStudent(Student newStudent)
	{

		int affectedRows = 0;
		// 1a - update Statement
		String insertString = "";
		insertString = "INSERT INTO Student(Vorname, Credits) ";
		insertString += " VALUES(?,?) ";

		int newIdentity = 0;
		try
		{
			// 1b Prepared Statement
			PreparedStatement stmtInsert = con.prepareStatement(insertString);

			// 1c - Parameter setzen
			stmtInsert.setString(1, newStudent.getVorname());
			stmtInsert.setDouble(2, newStudent.getCredits());

			affectedRows = stmtInsert.executeUpdate();

			String newIdentityString = "SELECT @@identity ";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(newIdentityString);

			rs.next();

			newIdentity = rs.getInt(1);

		} catch (SQLException e)
		{
			//
			e.printStackTrace();
		}
		return newIdentity;

	}

	public int updateCreditFromId(double neuerWert, int studentId)
	{
		String query = "UPDATE Student SET  Credits=? WHERE studentid=?";
		int rs = 0;
		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setDouble(1, neuerWert);
			stmt.setInt(2, studentId);
			rs = stmt.executeUpdate();
			System.out.println("Hat funktioniert :-D\nAffected Rows:");
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return rs;
	}

	public Student getStudent(int studentId)
	// EINEN bestimmten Studenten mit allen Eingenschaften ausgeben (OBJEKT
	// AUSGEBEN!!!!)
	{
		Student s = new Student(); // Klasse Student mit Attributen erstellen!!

		String query = "SELECT vorname, nachname, credits, inskribiert, lieblingsfarbe FROM Student WHERE studentId = ?";

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, studentId);
			ResultSet rs = stmt.executeQuery();

			if (rs.next() == true)
			{
				String vorname = rs.getString(1);// rs.setVorname(rs.getString(1));
				s.setVorname(vorname);
				s.setNachname(rs.getString(2));
				s.setCredits(rs.getInt(3));
				s.setInskribiert(rs.getBoolean(4));
				s.setLieblingsfarbe(rs.getString(5));

				return s;
			} else
			{
				return null;
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return s;
	}

	public void printUrlaubsortStatistik()
	{
		String query = "SELECT Ort, COUNT (Ort) FROM Urlaubswunsch GROUP BY Ort" + " ORDER BY COUNT (*) desc"; // leerzeichen
																												// vor
																												// ORDER!!
		// wäre der Ort "null" würde das nicht funktionieren!! deshlab COUNT (*)
		// besser!!!
		// da Ort nach "SELECT" nicht aggregiert wird, muss danach ein GROUP BY kommen

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				String ort = rs.getString(1);
				int anzahlNennung = rs.getInt(2);
				System.out.printf("Ort: %s Anzahl: %d \n", ort, anzahlNennung);
			}
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void printZusammenfassungProStudent() // AGGREGATFUNKTIONEN (in diesem Fall SUM und AV)

	// so würde das als SQL Statement aussehen:
	// SELECT Student.Vorname, avg(Urlaubswunsch.Preis),
	// count(Urlaubswunscht.StudentID)
	// FROM Student, Urlaubswunscht WHERE Student.StudentID =
	// Urlaubswunsch.StudentID
	// GROUP BY Vorname
	// ACHTUNG!!! wenn hier jemadn keinen urlaub angegeben hat, scheint diese person
	// garnicht auf!! --> deshalb LÖSUNG MIT JOINS!!
	{
		String query = "SELECT Student.Vorname, avg(Urlaubswunsch.Preis) Durchschnitt, count(Urlaubswunsch.StudentID) Anzahl"
				+ " FROM Student, Urlaubswunsch" + " WHERE Student.StudentID = Urlaubswunsch.StudentID"
				+ " GROUP BY Vorname"; // Summe der Datensätze wird bestimmt durch

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				String vorname = rs.getString(1); // Auf die Gesuchten vor FROM bezogen!!
				int durchschnitt = rs.getInt(2);
				int anzahlUrlaube = rs.getInt(3);
				System.out.printf(
						"Der Wunschurlaub von Student %s kostet im Schnitt %d € . Urlaubswünsche gesamt: %d \n",
						vorname, durchschnitt, anzahlUrlaube);
			}
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void printUrlaubsuebersichtForAllStudents()
	{
		String query = "SELECT StudentId FROM Student";

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				int studentId = rs.getInt(1);
				printStudendAndUrlaube(studentId, 0); // 0 weil Preis in dem Fall egal!
				// Ausgabe tec. und Urlaubsort-Favorit ist alles schon in der Methode "print
				// Student mit drinnen!!!
			}
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void printStudendAndUrlaube(int studentId, double preisGroesserAls)
	{
		// Verschachtelung, damit klar ist, warum es keine Ausgabe gibt,
		// wenn zB der student fehlt und/oder der student keinen urlaub angegeben hat

		String query = "SELECT Vorname FROM Student WHERE studentId=? ";

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, studentId);
			ResultSet rs = stmt.executeQuery();

			if (rs.next())
			{
				String vorname = rs.getString(1);
				System.out.printf("Student %s: ", vorname);

				String queryUrlaube = "SELECT preis, Ort from Urlaubswunsch " + "WHERE preis>? and studentId=? \n";

				PreparedStatement stmtFuerUrlaube = con.prepareStatement(queryUrlaube);
				stmtFuerUrlaube.setDouble(1, preisGroesserAls);
				stmtFuerUrlaube.setInt(2, studentId);
				ResultSet rsUrlaube = stmtFuerUrlaube.executeQuery();

				int counter = 0;

				while (rsUrlaube.next())
				{
					counter++;
					double preis = rsUrlaube.getDouble(1);
					String ort = rsUrlaube.getString(2);

					System.out.printf("Preis %.2f €, Ort %s, ", preis, ort);
				}

				if (counter == 0) // zählt die vorhandenen Urlaube, wenn keiner vorhanden, kommt die Auagabe
				{
					System.out.println("Dieser Student hat keine Urlaubswuesche ");
				}
			} else
			{
				System.out.printf("\nStudent %d nicht gefunden ", studentId);
			}
		} catch (SQLException e)
		{

			e.printStackTrace();
		}

	}

	public void printAllUrlaubeForStudentWithIdAndPriceGreaterThan(int studentId, double preis)
	{
		String query = "SELECT preis, ort from Urlaubswunsch WHERE studentId = ? and preis > ?";
		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, studentId); // WERTE DIE MITGEGEBEN WURDEN SETZEN!!!!
			stmt.setDouble(2, preis); // WERTE SETZEN!!!!
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				double price = rs.getDouble(1); // WERTE, die ich wissen will
				String ort = rs.getString(2);

				System.out.printf("%.2f %s \n", price, ort);
			}
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void printDetailsForStudentWithPreparedStatement(int studentId)
	{
		String query = "SELECT Vorname, inskribiert FROM Student WHERE studentid = ? ";

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, studentId); // Fragezeichen an Stelle 1 wird ersetzt durch studentId
			ResultSet rs = stmt.executeQuery(); // query darf hier nicht hinein, denn query ist hier der
												// zusammengebastelte Sring

			if (rs.next() == true)
			{
				String vorname = rs.getString(1);
				boolean inskribiert = rs.getBoolean(2);
				System.out.printf("DIESER Student heißt %s mit Vornamen \n", vorname); // Variable mitgeben!!!
			} else
			{
				System.out.printf("Der eingegebene Parameter hat zu keinem Ergebnis geführt \n");
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printDetailsForStudentWithId(int studentId) // nicht mehr diese Version verwenden
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "SELECT Vorname FROM Student WHERE StudentID =" + studentId + " ";
			ResultSet rs = stmt.executeQuery(query); // query = der zusammengebastelte string

			if (rs.next() == true)
			{
				String vorname = rs.getString(1);
				System.out.printf("Der Student heißt %s mit Vornamen \n", vorname); // Variable mitgeben!!!
			} else
			{
				System.out.printf("Der eingegebene Parameter hat zu keinem Ergebnis geführt \n");
			}
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void printAllUrlaubeWithPreisGreaterThanOrderByOrt(double preisGroesserAls)
	{
		// DIESE LÖSUNGSVARIANTE NICHT VERWENDEN, VERALTET UND SICHERHEITSLÜCKE!!
		try
		{
			Statement stmt = con.createStatement();
			String query = "SELECT Preis, Ort FROM Urlaubswunsch " + "WHERE Preis >" + preisGroesserAls
					+ " ORDER BY Ort";
			// DIESE LÖSUNGSVARIANTE NICHT VERWENDEN, VERALTET UND SICHERHEITSLÜCKE!!
			// VOR ORDER MUSS !!!!!! EIN LEERZEICHEN SEIN!!!

			ResultSet rs = stmt.executeQuery(query);
			while (rs.next())
			{
				double preis = rs.getDouble(1);
				String ort = rs.getString(2);

				System.out.printf("Ort ist %s zum Preis von %.2f \n", ort, preis);
			}
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void printAllStudents()
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "SELECT Vorname, Nachname, Credits, inskribiert FROM Student";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next())
			{
				String vorname = rs.getString(1);
				String nachname = rs.getString(2);
				int credits = rs.getInt(3);
				boolean inskribiert = rs.getBoolean(4);
				// String inskri;

				// if(inskri == true)
				// inskri = "inskribiert"

				System.out.printf("Student %s %s hat %d Credits und ist %b \n", vorname, nachname, credits,
						inskribiert);
				// nicht vergessen die Variablen dazu zu schreiben!!

			}
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void printAllUrlaube()
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "SELECT Preis, Ort, Preis * 1.1 FROM Urlaubswunsch";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next())
			{
				String ort = rs.getString(2);
				double preis = rs.getDouble(1);
				double preisPlus10Prz = rs.getDouble(1);

				System.out.printf("Ort %s Preis %.2f € \n", ort, preis);
			}
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void insertUrlaub(String destination, double preis)
	{
		// insert into MeineUrlaube(Ziel, price) .....
	}

	public void insertUrlaub(Urlaub urlaub)
	{
		// insert into MeineUrlaube(Ziel, price) .....
	}

	public void deleteUrlaub(int urlaubsNr)
	{

	}
	/*
	 * public ArrayList<String> GetAlleUrlaubPreisGroesserAls(double preis) {
	 * 
	 * return null; }
	 */

	public ArrayList<Urlaub> GetAlleUrlaubPreisGroesserAls(double preis)
	{

		return null;
	}

	public void listKunden()
	{

		try
		{

			Statement stmt = con.createStatement();
			String sql = "SELECT * FROM Kunde ";

			ResultSet rs = stmt.executeQuery(sql);

			System.out.println("Meine Kunden");
			while (rs.next())
			{
				System.out.print("Kdnr: " + rs.getInt(1));
				System.out.println(" Vorname " + rs.getString(2));

			}
			rs.close();
			stmt.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}

	}
}
