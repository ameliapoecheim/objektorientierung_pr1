
public class Urlaub
{
	private String destination;
	private int prio;
	private double price;
	
	public String getDestination()
	{
		return destination;
	}
	public void setDestination(String destination)
	{
		this.destination = destination;
	}
	public int getPrio()
	{
		return prio;
	}
	public void setPrio(int prio)
	{
		this.prio = prio;
	}
	public double getPrice()
	{
		return price;
	}
	public void setPrice(double price)
	{
		this.price = price;
	}
	@Override
	public String toString()
	{
		return "Urlaub [destination=" + destination + ", prio=" + prio + ", price=" + price + "]";
	}
	
	
}
