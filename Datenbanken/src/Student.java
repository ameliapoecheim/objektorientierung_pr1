import java.util.ArrayList;

public class Student
{
	private String vorname;
	private String nachname;
	private int credits;
	private boolean inskribiert;
	private String lieblingsfarbe;
	ArrayList<Urlaub> urlaube = new ArrayList<>();
	
	public void addUrlaub(Urlaub u)
	{
		urlaube.add(u);
	}
	
	public String getVorname()
	{
		return vorname;
	}

	public void setVorname(String vorname)
	{
		this.vorname = vorname;
	}

	public String getNachname()
	{
		return nachname;
	}

	public void setNachname(String nachname)
	{
		this.nachname = nachname;
	}

	public int getCredits()
	{
		return credits;
	}

	public void setCredits(int credits)
	{
		this.credits = credits;
	}

	public boolean isInskribiert()
	{
		return inskribiert;
	}

	public void setInskribiert(boolean inskribiert)
	{
		this.inskribiert = inskribiert;
	}

	public String getLieblingsfarbe()
	{
		return lieblingsfarbe;
	}

	public void setLieblingsfarbe(String lieblingsfarbe)
	{
		this.lieblingsfarbe = lieblingsfarbe;
	}

	@Override
	public String toString()
	{
		return "Student [vorname=" + vorname + ", nachname=" + nachname + ", credits=" + credits + ", inskribiert="
				+ inskribiert + ", lieblingsfarbe=" + lieblingsfarbe + ", urlaube=" + urlaube + "]";
	}

	

}
