package Uebungen;

import java.util.Arrays;

public class ArrayListe
{
	private String[] MyArrayList;
	private int ctr = 0;

	public ArrayListe()
	{
		MyArrayList = new String[5];
	}

	public String get(int i)
	{
		return MyArrayList[i];
	}

	public void add(String value)
	{

		// String[] MyArrayList2 = MyArrayList;
		// String [] newList= new String[MyArrayList.length];

		if (ctr >= MyArrayList.length)
		{

			// String[] newList = Arrays.copyOf(MyArrayList, MyArrayList.length*2); //Andere
			// Möglichkeiten Arrays zu kopieren
			// MyArrayList = newList, // Schlanker 2-Zeiler

			// System.arraycopy(MyArrayList, 0, newlist, 0, MyArrayList.length);

			String[] newList = new String[MyArrayList.length * 2];

			for (int i = 0; i < MyArrayList.length; i++)
			{
				newList[i] = MyArrayList[i];
			}
			MyArrayList = new String[newList.length];
			MyArrayList = newList;
		}

		MyArrayList[ctr] = value;
		ctr++;

	}
	
	public void extendArrayIfNeeded()
	{
		if (ctr >= MyArrayList.length)
		{
			 String[] newList = Arrays.copyOf(MyArrayList, MyArrayList.length*2); 
			 MyArrayList = newList;
		}
	}
	
	public void insertInto(int index, String value)
	{ 
		extendArrayIfNeeded();
		
		for(int i = MyArrayList.length-1; i>index; i--)
		{
			
			MyArrayList[i]=MyArrayList[i-1];
		}
		MyArrayList[index]=value;
		ctr++;
	}
	
	public int getCtr()
	{
		return ctr;
	}
	public int getLength()
	{
		return MyArrayList.length;
	}

	public void remove(int i)
	{
		if (i <= ctr)
		{
			for (int x = i; x < MyArrayList.length; x++)
			{
				if (x == MyArrayList.length - 1)
				{
					MyArrayList[x] = null;
				} else
				{
					MyArrayList[x] = MyArrayList[x + 1];
				}
			}
			ctr--;
		}
		else
		{
			System.out.println("Diese Stelle ist leer.");
		}

		if (ctr == 0)
		{
			System.out.println("Hier ist nichts zu löschen. Die Liste ist Leer...");
		}
	}

	@Override
	public String toString()
	{
		return "ArrayListe [MyArrayList=" + Arrays.toString(MyArrayList) + "]";
	}

}
