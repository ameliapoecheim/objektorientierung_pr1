package Uebungen;

import java.util.ArrayList;

public class SimpleQueue
{
	private ArrayList<String> queueling = new ArrayList<>();
	
	public void offer(String value)
	{
		queueling.add(value);
	}
	
	public String poll()
	{
		return queueling.remove(0);
	}
	
	public boolean isEmpty()
	{
		if(queueling.isEmpty())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public String peek()
	{
		return queueling.get(0);
	}
	
}
