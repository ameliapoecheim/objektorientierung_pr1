package Uebungen;

import java.util.ArrayList;
import java.util.Iterator;

public class UniqueStringList implements Iterable<String>
{
	
	private ArrayList<String> list = new ArrayList<>();
	
	
	public boolean add(String value)
	{
		if(contains(value) == 0)
		{
			list.add(value);
			return true;
		}
		else
		{
			System.out.println("Inhalt '" + value + "' existiert beeits.");
			return false;
		}
	}
	
	public void remove(String value)
	{
		if(list.contains(value))
		{
			list.remove(value);
		}
		else
		{
			System.out.println("'" + value + "' wurde nicht gefunden und kann somit nicht entfernt werden.");
		}
	}
	
	public int contains(String value)
	{
		int result = 0;
		
		for (String content : list)
		{
			if(content.equals(value))
			{
				result++;
			}
		}
		return result;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((list == null) ? 0 : list.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UniqueStringList other = (UniqueStringList) obj;
		if (list == null)
		{
			if (other.list != null)
				return false;
		} else if (!list.equals(other.list))
			return false;
		return true;
	}

	@Override
	public Iterator<String> iterator()
	{
		
		return list.iterator();
	}
	

}
