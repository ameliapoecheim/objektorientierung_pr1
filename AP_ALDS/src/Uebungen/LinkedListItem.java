package Uebungen;

public class LinkedListItem
{
	// add, remove, size, toArray
	
	private String value;
	private LinkedListItem next;
	
	public LinkedListItem(String value)
	{
		this.value=value;
	}
	
	public String getValue()
	{
		return value;
	}
	
	public void setNext(LinkedListItem next)
	{
		this.next=next;
	}
	public LinkedListItem getNext()
	{
		return next;
	}

}
