package Uebungen;

//import sun.misc.Queue;  relativ neu
import java.util.PriorityQueue;

public class Queues
{

	public static void main(String[] args)
	{
		PriorityQueue<String> values = new PriorityQueue<String>();
		
		values.offer("value 1");
		values.offer("value 2");
		values.offer("value 3");
		values.offer("value 4");
		
		System.out.println("Size: " + values.size());
		
		String value = values.poll();
		System.out.println("Poll: " + value);
		
		System.out.println("Peek: " + values.peek());
		
		
		System.out.println("Size: " + values.size());
		
		
	}

}
