package Uebungen;

import java.util.HashMap;

public class ArrayToMap
{

	public static void main(String[] args)
	{
		String[][]values = new String[][]
				{{"rot",  "#FF0000"},
				{"gr�n", "#00FF00"},
				{"blau", "#0000FF"}};
		HashMap<String, String> converted = convertToMap(values);
		
		for (String val : converted.keySet())
		{
			String var = converted.get(val);
			System.out.println(val + ": " +  var);
		}				

	}
	
	private static HashMap<String, String> convertToMap(String[][] values)
	{
		HashMap<String, String> result = new HashMap<>();
		
			for(int k = 0; k < values.length; k++)
			{
				for(int i = 1; i < values[0].length; i++)
				{
					
					result.put(values[k][i-1], values[k][i]);
				}
			}
		
		return result;
	}

}
