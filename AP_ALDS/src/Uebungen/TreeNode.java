package Uebungen;

public class TreeNode
{
	private String value;
	private TreeNode leftChild;
	private TreeNode rightChild;
	
	
	public TreeNode(String value)
	{
		
		this.value = value;
		
	}
	
	public void add(String newValue)
	{
		if(value.compareTo(newValue) > 0)
		{
			// Links
			if(leftChild == null)
			{
				leftChild = new TreeNode(newValue);
			}
			else
			{
				leftChild.add(newValue);
			}
			
		}
		
		else  // if(value.compareTo(newValue) > 0)
		{
			// Rechts
			if(rightChild == null)
			{
				rightChild = new TreeNode(newValue);
			}
			else 
			{
				rightChild.add(newValue);
			}
		}
	}
	
	
	public void preorder()
	{
		System.out.print("Node: " + value);
		
		if(leftChild != null)
		{
			System.out.print(" next going left\r\n");
			leftChild.preorder();
			
		}
		
		if(rightChild != null)
		{
			System.out.print(" next going right\r\n");
			rightChild.preorder();
			
		}
	}
	
	public void inorder()
	{
		if(leftChild != null)
		{
			leftChild.inorder();
		}
		
		System.out.println(value);
		
		if(rightChild != null)
		{
			rightChild.inorder();
		}
		
		
	}
	
	public boolean preorderChild(String valueToMatch)
	{
		if(value.equals(valueToMatch))
		{
			System.out.println("Found: " + value);
			return true;
		}
		else if(leftChild != null)
		{
			boolean success = leftChild.preorderChild(valueToMatch);		
			if(success == true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if(rightChild != null)
		{
			boolean success = rightChild.preorderChild(valueToMatch);
			if(success == true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			System.out.println(valueToMatch + " not found.");
			return false;
		}
	}
	
	
	public void inorderChild(String valueToMatch)
	{
		if(leftChild != null)
		{
			leftChild.inorderChild(valueToMatch);
		}
		
		if(value.equals(valueToMatch))
		{
			System.out.println("Found: " + value);
			return;
		}
		
		if(rightChild != null)
		{
			rightChild.inorderChild(valueToMatch);
		}
		
		
		
	}
	

	public String getValue()
	{
		return value;
	}

	public TreeNode getLeftChild()
	{
		return leftChild;
	}

	public TreeNode getRightChild()
	{
		return rightChild;
	}
	
	
	
}
