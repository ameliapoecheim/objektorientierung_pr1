package Uebungen;

import java.util.ArrayList;

public class SimpleDeque
{
	ArrayList<String> dequeling = new ArrayList<>();

	public void offerFirst(String value)
	{
		dequeling.add(0, value);
	}

	public void offerLast(String value)
	{
		dequeling.add(value);
	}

	public String pollFirst()
	{
		return dequeling.remove(0);
	}

	public String pollLast()
	{
		return dequeling.remove(dequeling.size() - 1);
	}

	public String peekFirst()
	{
		return dequeling.get(0);
	}
	
	public String peekLast()
	{
		return dequeling.get(dequeling.size() - 1);
	}
	
	public boolean isEmpty()
	{
		if (dequeling.isEmpty())
		{
			return true;
		} 
		else
		{
			return false;

		}
	}

}
