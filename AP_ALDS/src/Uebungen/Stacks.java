package Uebungen;

import java.util.Stack;

public class Stacks
{

	public static void main(String[] args)
	{
		Stack<String> stack = new Stack<String>();
		stack.push("Wert 1");
		stack.push("Wert 2");
		
		System.out.println("Size: " + stack.size());
		
		String peekvalue = stack.peek();
		System.out.println(peekvalue);
		
		System.out.println("Size: " + stack.size());

		String value = stack.pop();
		System.out.println(value);
		
		System.out.println("Size: " + stack.size());

	}

}
