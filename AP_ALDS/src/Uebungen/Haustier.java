package Uebungen;

public class Haustier
{
	private String name;
	private int alter;
	
	public Haustier(String name)
	{
		this.name = name;
		this.alter =0;
	}
	
	public Haustier(String name, int alter)
	{
		this.name = name;
		this.alter = alter;
	}
	
	public int getAlter()
	{
		return alter;
	}

	public String getName()
	{
		return name;
	}




	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + alter;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Haustier other = (Haustier) obj;
		if (alter != other.alter)
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return name;
	}
	
	
}
