package Uebungen;

import java.util.Arrays;

public class SimpleStack
{
	private String[] stackling = new String[10];
	private int ctr = 0;
	
	public void push(String value)
	{
		if (ctr >= stackling.length)
		{
			String[] newList = new String[stackling.length * 2];

			for (int i = 0; i < stackling.length; i++)
			{
				newList[i] = stackling[i];
			}
			stackling = new String[newList.length];
			stackling = newList;
		}
//		if (ctr >= stackling.length)
//		{
//			String newArray = Arrays.copyOf(stackling, stackling.length * 2);
//			stackling = newArray;
//		}

		stackling[ctr] = value;
		ctr++;
	}
	
	public String pop()
	{
		ctr--;
		String pop = stackling[ctr];
		stackling[ctr]=null;
		return pop;
	}
	
	public String peek()
	{
		String peek = stackling[ctr-1];
		return peek;
	}
	public int size()
	{
		return ctr;
	}
}
