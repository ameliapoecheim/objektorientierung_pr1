package Uebungen;

public class Tree
{
	private TreeNode root;
		
	public void add(String value)
	{
		if(root == null)
		{
			root = new TreeNode(value);
			return;			
		}
		
		root.add(value);		
	}
	
	public void printTreePreorder()
	{
		if(root != null)
			root.preorder();
	}
	
	public boolean preorder(String value)
	{
		if(root.getValue().equals(value))
		{
			System.out.println("Found: " + root.getValue());
			return true;
		}
		else
		{
			return root.preorderChild(value);
		}
	}
	
	public void printTreeInorder()
	{
		if(root != null)
			root.inorder();
	}
	
	
	public void inorder(String value)
	{
		if(root != null)
		{
			root.inorderChild(value);
		}
		
		
	}
	
	
}
