package Map;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class WordCounter
{
	
//	 Verbraucht Resourcen, weil das gesamte eingelesene Dokument in einen String eingelesen wird. 
//	 z.B 1GB wird eingelesen -> String ben�tigt 1GB. Methode 2 ist daher besser.
	
	public Map<String, Integer> count(String filePath)
	{
		Map<String, Integer> counting = new HashMap<>();
		
		
		FileReader fr;
		BufferedReader br;
		String content = "";
		int counter = 0;
		 
		try 
		{
			fr = new FileReader(filePath);
			br = new BufferedReader(fr);
			String next;
			
			while((next = br.readLine()) != null)
			{
				content += next;
			}
			
			content = content.toLowerCase();
			String[] splited = content.split("\\s+");
			
			for (String str : splited)
			{
				if(!counting.containsKey(str))
				{
					counting.put(str, 1);
				}
				else
				{
					int val = counting.get(str).intValue();
					val++;
					counting.put(str, val);
				}
			}
			
			fr.close();
			br.close();
		}
	 	catch(IOException e)
		{
		e.printStackTrace();
		}
		
		return counting;
	}
	
//	Resourcen schonender, 
	
	public Map<String, Integer> countAltenativeVersion(String filePath)
	{
		Map<String, Integer> counting = new HashMap<>();
		
		
		
		FileReader fr;
		BufferedReader br;
		String content = "";
		int counter = 0;
		 
		try 
		{
			fr = new FileReader(filePath);
			br = new BufferedReader(fr);
			String line = br.readLine();
			
			while(line != null)
			{
				String[] words = line.split(" ");
				
				for(String word : words)
				{
					if(!counting.containsKey(word))
					{
						counting.put(word, 1);
					}
					else
					{
						int val = counting.get(word).intValue();
						val++;
						counting.replace(word, val);		// replace statt put. put �berschreibt eigentl. keine bestehenden Eintr�ge
					}
				}
				
				line = br.readLine();
			}
			
			fr.close();
			br.close();
		}
	 	catch(IOException e)
		{
		e.printStackTrace();
		}
		
		return counting;
	}
	
	public customMap countUsingCustomMap(String filePath)
	{
		customMap counting = new customMap();
		
		
		FileReader fr;
		BufferedReader br;
		String content = "";
		int counter = 0;
		 
		try 
		{
			fr = new FileReader(filePath);
			br = new BufferedReader(fr);
			String next;
			
			while((next = br.readLine()) != null)
			{
				content += next;
			}
			
			content = content.toLowerCase();
			String[] splited = content.split("\\s+");
			
			for (String str : splited)
			{
				if(!counting.contains(str))
				{
					counting.put(str, 1);
				}
				else
				{
					int val = counting.get(str).intValue();
					val++;
					counting.put(str, val);
				}
			}
			
			fr.close();
			br.close();
		}
	 	catch(IOException e)
		{
		e.printStackTrace();
		}
		
		return counting;
	}
	
}
