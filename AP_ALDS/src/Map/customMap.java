package Map;

import java.util.ArrayList;

public class customMap
{
	protected ArrayList<String> keyList;
	protected ArrayList<Integer> valueList;
	
	public customMap()
	{
		keyList = new ArrayList<>();
		valueList = new ArrayList<>();
	}
	
	public void put(String key, Integer value)
	{
		if(!keyList.contains(key))
		{
			keyList.add(key);
			valueList.add(value);			
		}
		else
		{
			int index = 0;
			
			for( int ctr = 0; ctr < keyList.size(); ctr++)
			{
				if(keyList.get(ctr).equals(key))
				{
					index = ctr;
				}
			}
			
			valueList.add(index, value);			
		}		
	}
	
	public Integer get(String key)
	{
		int index = 0;
		
		int keyIndex = keyList.indexOf(key);
		
//		for( int ctr = 0; ctr < keyList.size(); ctr++)
//		{
//			if(keyList.get(ctr).equals(key))
//			{
//				index = ctr;
//			}
//		}
		
		return valueList.get(index);
//		return valueList.get(keyIndex;
	}
	
	public void remove(String key)
	{
		int index = get(key);
		
		keyList.remove(index);
		valueList.remove(index);		
	}
	
	public boolean contains(String key)
	{
		boolean check = false;
		
		for (String string : keyList)
		{
			if(string.equals(key))
			{
				return true;
			}
		}
		
		return check;
	}
	
	public ArrayList<String> keySet()
	{
		return keyList;
	}

}
