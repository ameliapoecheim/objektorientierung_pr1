package Map;

import java.util.ArrayList;
import java.util.Map;

public class Main
{

	public static void main(String[] args)
	{
		WordCounter wordCount = new WordCounter();
		
		Map<String, Integer> rndmText = wordCount.count("text.txt");
		
		for(String key : rndmText.keySet())
		{
			int value = rndmText.get(key);
			System.out.println("Key: " + key + "\r  Value: " + value);  // \t = tabulator
		}
		
		System.out.println("\r\r");
		
		customMap rndmTextCustomMap = wordCount.countUsingCustomMap("text.txt");	// Eigene Implementierung einer HashMap
//		ArrayList<String> keyCheck = rndmTextCustomMap.keyList;
		
//		ArrayList<Integer> valueCheck = rndmTextCustomMap.valueList;
//		
//		for(int ctr = 0; ctr < keyCheck.size(); ctr ++)
//		{
//			System.out.println(keyCheck.get(ctr) + " " + valueCheck.get(ctr));
//		}
		
		for(String keyString : rndmTextCustomMap.keySet())
		{
			int value = rndmTextCustomMap.get(keyString);
						
			System.out.println("Key: " + keyString + "\r  Value: " + value);
		}
		
	}
	
	

}
