package KlausurUebungen;

import java.util.HashMap;

public class HashsetsVorbereitung
{

	public static void main(String[] args)
	{
		String text = "the quick brown fox jumps over the lazy dog";
		System.out.println(text);
		System.out.println("Gesamtanzahl der Buchstaben: " + text.length());
		
		HashMap<Character, Integer> counting = countingChars(text);
		
		for (Character c : counting.keySet())
		{
			int value = counting.get(c);
			
			System.out.println(c + ": " + value + "x");
		}
	}
	
	private static HashMap<Character, Integer> countingChars(String text)
	{
		HashMap<Character, Integer> result = new HashMap<>();
		
		for(int i = 0; i < text.length(); i++)
		{
			if(result.containsKey(text.charAt(i)))
			{
				int ctr = result.get(text.charAt(i));
				ctr++;
				result.put(text.charAt(i), ctr);
			}
			else
			{
				result.put(text.charAt(i), 1);
			}
		}
		
		return result;
	}
	
	private static void printMap(HashMap<Character, Integer> toPrint)
	{
		for (Character c : toPrint.keySet())
		{
			int value = toPrint.get(c);
			
			System.out.println(c + ": " + value + "x");
		}
	}
}
