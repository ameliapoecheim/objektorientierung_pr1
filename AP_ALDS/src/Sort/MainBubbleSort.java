package Sort;

public class MainBubbleSort
{

	public static void main(String[] args)
	{
		BubbleSorting bs = new BubbleSorting();
//		3 5 1 7 4
		int[] numbers = new int[] {3,5,1,7,4};
		int[] fileNumbers = bs.fromFileToIntArray("numbers.txt"); 
	
		bs.printArray(fileNumbers);
		
		fileNumbers = bs.bubbleSort(fileNumbers);
		
		System.out.println(bs.fromArrayToString(fileNumbers));	
	}

}
