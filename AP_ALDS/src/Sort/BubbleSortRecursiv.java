package Sort;

import java.util.Arrays;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

public class BubbleSortRecursiv
{
	public static void main(String[] args)
	{
		BubbleSorting bs = new BubbleSorting();
//		3 5 1 7 4
		int[] numbers = new int[] {8,2,7,5,3,6,4,5,10,9,1};
		int[] fileNumbers = bs.fromFileToIntArray("numbers.txt"); 
		
		for(int ctr = 0; ctr < numbers.length; ctr++)
		{
			System.out.print(numbers[ctr] + " ");
		}
		System.out.println();
		
		int[]sorted = bubbleSort(numbers);
		
		for(int ctr = 0; ctr < numbers.length; ctr++)
		{
			System.out.print(sorted[ctr] + " ");
		}
		
		
	
//		bs.printArray(fileNumbers);
//		
//		fileNumbers = bs.bubbleSort(fileNumbers);
//		
//		System.out.println(bs.fromArrayToString(fileNumbers));	
		
		
	}
	
	public static int[] bubbleSort(int[] numbers)
	{		
		outerBubble(numbers, 1);
		return numbers;
	}
	
	public static void outerBubble(int[] numbers, int k)
	{
		System.out.println("Round " + k);
		
		boolean swap = innerBubble(numbers, k);
		k++;
		
		if(swap)
		{
			outerBubble(numbers, k);
		}
		
	}
	
	public static boolean innerBubble(int[] numbers, int index)
	{
		if(index >= numbers.length - 1)
		{
			return false;
		}
		
		System.out.println(numbers[index] + " > " + numbers[index+1]);
		
		boolean swap = false;
		
		if(numbers[index] > numbers[index+1])
		{
			int temp=numbers[index];
			numbers[index]=numbers[index+1];
			numbers[index+1]= temp;
			swap = true;
		}
		index++;
		
		innerBubble(numbers, index);
				
		return swap;
	}
	
	
}
