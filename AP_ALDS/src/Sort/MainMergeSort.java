package Sort;

import java.util.Arrays;
import java.util.Random;

public class MainMergeSort
{

	public static void main(String[] args)
	{
		int[] values = new int[10000]; // {8,2,7,3,6,4,5,10,9,1};
		
//		for(int value : values)
//		{
//			System.out.print(value + " ");
//		}
		
//		System.out.println();
//		
//		int[] sortedValues = mergeSort(values);
//		
//		System.out.println();
		
//		for(int value : sortedValues)
//		{
//			System.out.print(value + " ");
//		}
		Random rnd = new Random();
		for(int i = 0; i < values.length; i++)
		{
			values[i] = rnd.nextInt();
		}
		
		
		long startTime = System.nanoTime();
		
		int[] sorted = mergeSort(values);
		
		long endTime = System.nanoTime();
		
		long period = endTime - startTime;
		
		System.out.println(period / 1000000 + "ms");
		

	}
	
	private static int[] mergeSort(int[] values)	// Rekursiv linken und rechten Teilbaum spliten
	{
		if(values.length == 1)
		{
			return values;
		}
		
		int rightSide = values.length/2;
		int leftSide = values.length - rightSide;
		
		int[] leftValues = Arrays.copyOfRange(values, 0, leftSide);
		int[] rightValues =  Arrays.copyOfRange(values, leftSide, values.length);
			
		int[] sortedLeftValues = mergeSort(leftValues);
		int[] sortedRightValues = mergeSort(rightValues);
		
		int[] sortedValues = new int[values.length];
		
		int l = 0;
		int r = 0;
		
		for(int i = 0; i < sortedValues.length; i++)
		{
			if(sortedLeftValues.length <= l)
			{
				sortedValues[i] = sortedRightValues[r];
				r++;
				continue;
			}
			if(sortedRightValues.length <= r)
			{
				sortedValues[i] = sortedLeftValues[l];
				l++;
				continue;
			}
			if(sortedLeftValues[l] < sortedRightValues[r])
			{
				sortedValues[i] = sortedLeftValues[l];
				l++;
				
			}
			else
			{
				sortedValues[i] = sortedRightValues[r];
				r++;
			}
		}
		return sortedValues;
	}

}
