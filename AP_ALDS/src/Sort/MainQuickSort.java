package Sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MainQuickSort
{

	public static void main(String[] args)
	{
		int[] nums = new int[] {8,2,7,5,3,6,4,5,10,9,1};
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		
//		for (Integer val : nums)
//		{
//			numbers.add(val);
//		}
		
		for(int i = 0; i < 10000; i++)
		{
			numbers.add(i);
		}
		
		Collections.shuffle(numbers);
		
		long startTime = System.nanoTime();
		ArrayList<Integer> sorted = quickSort(numbers);
		long endTime = System.nanoTime();
		
		long period = endTime - startTime;
		
		System.out.println(period / 1000000 + "ms");
		
//		System.out.println(numbers + "\r\n");
//		
//		
//		System.out.println(quickSort(numbers));

	}
	
	private static ArrayList<Integer> quickSort(ArrayList<Integer> values)
	{
		if(values.size() <= 1)
		{
			return values;
		}
		
		int pivot = values.get(values.size()-1);
		
		ArrayList<Integer> left = new ArrayList<>();
		ArrayList<Integer> right = new ArrayList<>();
		
		
		for(int i = 0; i < values.size() - 1 ; i++)
		{
			if(pivot > values.get(i))
			{
				left.add(values.get(i));
			}
			else
			{
				right.add(values.get(i));
			}
		}
		
		
//		for (Integer value : values)	// Doppelte Zahlen gehen bei der Sortierung verloren...
//		{
//			if(pivot > value)
//			{
//				left.add(value);
//			}
//			if(pivot < value)
//			{
//				right.add(value);
//			}
//		}
		
//		System.out.print(left);
//		System.out.print("  <-| " + pivot + " |->  ");
//		System.out.print(right);
//		System.out.println("\r\n");
		
		ArrayList<Integer> leftSorted = quickSort(left);
		ArrayList<Integer> rightSorted = quickSort(right);
		
		ArrayList<Integer> sorted = new ArrayList<>();
		
		sorted.addAll(leftSorted);
		sorted.add(pivot);
		sorted.addAll(rightSorted);
		
		
		return sorted;
	}
	
	
	

}
