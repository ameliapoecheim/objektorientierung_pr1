package Sort;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class BubbleSorting
{

	public int[] bubbleSort(int[] numbers)
	{		
		
		int temp;		
		int length = numbers.length;
		int roundCtr = 0;
		boolean swap= false;
		
		while(length!=1)
		{	
			swap = false;
			
			for(int index=0; index+1<length;index++)
			{
				if(numbers[index]>numbers[index+1])
				{
					temp=numbers[index];
					numbers[index]=numbers[index+1];
					numbers[index+1]= temp;
					swap = true;
				}
			}
			roundCtr++;
			length--;
			
			if(swap == false)
			{
				System.out.println(roundCtr + " Runden und bei " + numbers.length +" Zahlen.");
				return numbers;
			}
		}
		System.out.println(roundCtr + " Runden und bei " + numbers.length +" Zahlen.");
		return numbers;
	}
	
	public int[] bubbleSortAlternativ(int[] numbers)
	{		
		int temp;
		boolean swap = false;
		
		for(int k = 0; k < numbers.length; k++)
		{
			
			swap = false;
			for(int index=0; index+1<numbers.length;index++)
			{
				if(numbers[index]>numbers[index+1])
				{
					temp=numbers[index];
					numbers[index]=numbers[index+1];
					numbers[index+1]= temp;
					
					swap = true;
				}
			}
			if(swap == false)
			{
				return numbers;
			}
				
				
		}
			return numbers;
	}
	
	
	public int[] fromFileToIntArray(String filePath)
	{
		int[] result = null;
		FileReader fr;
		BufferedReader br;
		String content = "";
		int counter = 0;
		 
		try 
		{
			fr = new FileReader(filePath);
			br = new BufferedReader(fr);
			String next;
			
			while((next = br.readLine()) != null)
			{
				content += next;
			}
			
			String[] splited = content.split(",");
			result = new int[splited.length];
			
			for(int ctr = 0; ctr < splited.length; ctr++)
			{
				result[ctr] = Integer.parseInt(splited[ctr]);
			}
			
			fr.close();
			br.close();
			
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	public String fromArrayToString(int[] numbers)
	{
		String result = "";
		
		for(int nums : numbers)
		{
			result += nums + " ";
		}
		return result += "\r\n";
	}
	
	public void printArray(int[] numbers)
	{
		for(int nums : numbers)
		{
			System.out.print(nums + " ");
		}
		System.out.println();
	}
}
