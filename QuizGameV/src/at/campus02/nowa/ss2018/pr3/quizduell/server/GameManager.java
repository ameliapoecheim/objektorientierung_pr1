package at.campus02.nowa.ss2018.pr3.quizduell.server;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import at.campus02.nowa.ss2018.pr3.quizduell.messages.Question;

public class GameManager
{
	// Zufallszahlengenerator f�r die Fragen
	// kann Seed mitgeben (=Startzufallswert, soll man aber NIE machen, ausser f�r
	// Testzwecke!! Random(1234) --> wenn ich zwei hab, kann ich zwei vergleichen)
	
	private int playerNum;
	private Random random = new Random();
	private List<QuestionRecord> questions = new ArrayList<>();
	private List<ClientRunnable> clients = new ArrayList<>();
	private Map<ClientRunnable, Boolean> answers = new HashMap<>();
	private QuestionRecord currentQuestion;
	
	private int currentScore;
	
	private Map<String, Integer> scores = new HashMap<>();

	public GameManager()
	{
		// Zentrale Koordinationstelle
		
		try(FileInputStream fin = new FileInputStream("server.properties"))
		{
			Properties config = new Properties();
			config.load(fin);
			playerNum = Integer.parseInt(config.getProperty("players.number"));
			
		} 
		catch(NumberFormatException e1)
		{
			playerNum = 4;
		}
		catch (FileNotFoundException e1)
		{
			playerNum = 4;
		} 
		catch (IOException e1)
		{
			System.out.println("Konfigurationen konnten nicht gelesen werden: " + e1.getMessage());
			System.exit(-1);		//  0 == Alles war gut; alles im Negativen Bereich zeigt Fehler an;  Alles im positiven Bereich gibt zusatzinfo. Z.b das Speichern war erfolgreich
		}
		
		
		
		try ( // Liest Fragen aus Dautei ein
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("quizfragen.csv"), StandardCharsets.UTF_8));
				//nicht FileReader wegen UTF 8
			)	
		{
			String line;
			while ((line = br.readLine()) != null)
			{
				// .. wird Zeile f�r Zeile ausgelesen
				// f�r jede augelesene Zeile wird eine bereinigte Form ohne "" erzeugt
				// danach wird der lange String in ein Array von Strings an den semikolons
				// gessplittet
				String[] fields = line.replace("\"", "").split(";"); // replace l�scht jetzt alle doppelten hochkomma

				// wenns mehr als 7 Zeilen sind, ignorieren wir das und machen weiter
				if (fields.length != 7)
				{
					continue;
				}
				try
				{
					// kann ich den letzten string hinten in einen integer umwandeln? wenn ja ist
					// alles gut, weiter, wenn nicht,
					// wird eine exeption gefangen und continue
					Integer.parseInt(fields[6]);
				} catch (NumberFormatException e)
				{
					System.out.println("Falsche Zahl: " + fields[6]);
					continue;
				}

				// Klasse QuestionRecords ist nur da um den Datensatz zu merken! ->
				// DataAcessObject (DAO)
				// fragetext = 0, richtige antwort = feld 1; feld 6 = schwierigkeitsgrad;
				// kategorie ist in feld 5, interessiert uns nicht
				QuestionRecord qr = new QuestionRecord(fields[0], Integer.parseInt(fields[6]),
						// EIN neuer Anwswerrecord wird erzeugt, mindestens eine muss dabei sein (die
						// richtige)
						new AnswerRecord(fields[1]));
				// getter auf Wrong list --> die falschen Antworten werden auf die Liste
				// eingef�gt
				qr.getWrong().add(new AnswerRecord(fields[2]));
				qr.getWrong().add(new AnswerRecord(fields[3]));
				qr.getWrong().add(new AnswerRecord(fields[4]));
				questions.add(qr); // diese liste ist dann der Pool, aus dem der gamemanager fragen f�r uns
									// ausw�hlen kann
			}
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	// sync damit immer nur NACHEINANDER die funktion verwendet wird!!
	//Zufallsfragen rausziehen
	public synchronized QuestionRecord getRandomQuestion()
	{

		// zuf�lliges element raussuchen
		// zufallszahl von 0 bis l�nge der liste
		// hinten geb ich die Obergrenze an, die NICHT mehr zum Set geh�rt (in dem Fall
		// size - beginnt eh bei 0)

		int r = random.nextInt(questions.size());

		QuestionRecord q = questions.get(r);
		questions.remove(r);
		return q;

	}
	
	public synchronized boolean registerClient(ClientRunnable r)	// boolean um Problem vom letzten Spieler der sich registriert zu l�sen
	{
		clients.add(r);
		
		if(clients.size() == playerNum)			// 2 Spieler k�nnen max spielen
		{
			currentQuestion = getRandomQuestion();
			
			for(ClientRunnable cr : clients)
			{
				synchronized (cr)
				{
					cr.notify();
				}
			}
			return true;
		}
		return false;
		
	}

	public boolean setAnswer(ClientRunnable cr, boolean correct)
	{
		answers.put(cr, correct);
		
		if(!scores.containsKey(cr.getPlayerName()))
		{
			scores.put(cr.getPlayerName(), 0);		// Holt sich Score des Aktuellen Spielers oder nimmt Defaultwert 0
		}
		
		currentScore = scores.get(cr.getPlayerName());
		
		if(correct)
		{
			currentScore += currentQuestion.getLevel();			// Addiert Score hoch
		}
		
		scores.put(cr.getPlayerName(), currentScore);
			
			
		if(answers.size() == playerNum)
		{
			//TODO Statistik mit alter Frage implementieren. Weil danach geht die aktuelle Frage verloren
			
			
			
			currentQuestion = getRandomQuestion();
			for(ClientRunnable clru : clients)
			{
				synchronized (clru)
				{
					clru.notify();
				}
			}
			answers.clear();
			return true;
		}
		return false;
	}
	
	public Map<String, Integer> getScores()
	{
		return scores;
	}

	public QuestionRecord getCurrentQuestion()
	{
		return currentQuestion;
	}
	   
}
