package at.campus02.nowa.ss2018.pr3.quizduell.server;

import java.util.UUID;

public class AnswerRecord
{
	// ist DataAcessObject (DAO)
	// zuf�llig generierte werte, die das betriebssystem erstellt
	// ID f�r Datensatz wie uniqe ID; bequemer weg zufalls ID zu bekommen
	private UUID uuid = UUID.randomUUID();
	private String text;

	public AnswerRecord(String text)
	{
		super();
		this.text = text;
	}

	public UUID getUuid()
	{
		return uuid;
	}

	public String getText()
	{
		return text;
	}

}
