package at.campus02.nowa.ss2018.pr3.quizduell.messages;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

//Nachrichtenklasse - gehört weder zu server, noch zu Client, beide müssen die selbe klasse kennen

//Serializable übersetzen von einem Java-Obekt in ein Bites!
//kann es mit ObjectOutputStream versenden!
public class Question implements Serializable
{
	// default ID
	// serial-ID sollte EINDEUTIG sein
	private static final long serialVersionUID = 1L;
	
	private int level;
	private String text; //eigentlicher Frage-text
	//Map hat keine Reihenfolge -  muss also nicht mischen!!
	//Map beinhaltet ALLE Antwortmöglichkeiten, schlüssel UUI, String-Wert ist antworttext
	//da MAP ungeordnet, brauch ich nicht shuffeln 
	private Map<UUID, String> answers = new HashMap<>();
	
	//Flag für letzte Frage oder nicht
	private boolean last = false;//defaultmässig mit false initialisiert, da das häufigstes
	
	
	public Question(int level, String text)
	{
		super();
		this.level = level;
		this.text = text;
	}


	public int getLevel()
	{
		return level;
	}


	public String getText()
	{
		return text;
	}


	public Map<UUID, String> getAnswers()
	{
		return answers;
	}
	
	//nicht autogenerierbar!!
	public void setLast()
	{
		last = true;
	}


	public boolean isLast()
	{
		return last;
	}

}
