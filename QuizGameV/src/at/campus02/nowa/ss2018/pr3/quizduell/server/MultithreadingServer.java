package at.campus02.nowa.ss2018.pr3.quizduell.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MultithreadingServer
{
	//diese klasse �ffnet den serversocket!
	
	public static void main(String[] args) //main!!!
	{
		try
		{
			ServerSocket server = new ServerSocket(1111);
			GameManager gm = new GameManager();
			
			while(true)
			{
				//Port ist auf Horchposten
				//Sobald Verbindung kommt, wird sie akzeptert
				Socket socket = server.accept(); 
				System.out.println("Accepted: " + socket);
				
				//AB dieser Zeile gehts ums MULTI-threading
				//JEDE eingehende Verbindung erzeugt ein ClientRunable und einen neuen Thread
				 //socket und gamemanager ins runable, damit das runable wei�, mit wem es reden kann
				//jedes dieser ClientRunableObjekte wird beim Start seine run() ausf�hren
				ClientRunnable cr = new ClientRunnable(socket, gm);
				
				//sobald Verbindung angenommen, wird ein Thread gestartet
				//client-runable unbedingt mitgeben!!
				Thread t = new Thread(cr); 
				t.start();

			}
			
			
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}


}
