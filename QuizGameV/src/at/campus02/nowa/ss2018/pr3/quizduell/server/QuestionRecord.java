package at.campus02.nowa.ss2018.pr3.quizduell.server;

import java.util.ArrayList;
import java.util.List;

public class QuestionRecord
{
	// ist DataAcessObject (DAO)
	// server muss sich frage und level merkjen bis antwort kommt = Questionrecord
	private String text; // fragetext
	private int level;
	private AnswerRecord correct; // richtige Anwort
	private List<AnswerRecord> wrong = new ArrayList<>(); // Liste mit den falschen Antworten
	
	//stellt sicher, dass keine frahge ohne correkte antwort angelegt werden kann!
	public QuestionRecord(String text, int level, AnswerRecord correct)
	{
		super();
		this.text = text;
		this.level = level;
		this.correct = correct;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public AnswerRecord getCorrect()
	{
		return correct;
	}

	public void setCorrect(AnswerRecord correct)
	{
		this.correct = correct;
	}

	public List<AnswerRecord> getWrong()
	{
		return wrong;
	}

	public void setWrong(List<AnswerRecord> wrong)
	{
		this.wrong = wrong;
	}
	
}
