package at.campus02.nowa.ss2018.pr3.quizduell.client;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import at.campus02.nowa.ss2018.pr3.quizduell.messages.Answer;
import at.campus02.nowa.ss2018.pr3.quizduell.messages.Player;
import at.campus02.nowa.ss2018.pr3.quizduell.messages.Question;

import java.util.UUID;

public class Client
{

	public static void main(String[] args)
	{
		try (Socket socket = new Socket("localhost", 1111);		// oder statt localhost IP Adresse eingaben		172.32.0.213 fladi  172.32.0.118 vero
				
				// am Client in UMGEKEHRTER REIHENFOLGE von CLIENTRUNNABLE aufmachen!!!!
				ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
				ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));)
		{
			System.out.println("Bitte Spielernamen eingeben:");
			String name = br.readLine();
			Player p = new Player(name);
			oos.writeObject(p);
			oos.flush(); // da das Paket SICHER unter der mindestgr��e liegt, ab der automatisch
							// weggeschickt wird

			while (true) // schleife, damit mehrere Fragen gestellt werden
			{
				Question q = (Question) ois.readObject();
				System.out.println("Frage : " + q.getText() + " (Level: " + q.getLevel() + ")");

				// auslesen der Antworten aus der Map
				// meine L�ung wars nicht ganz, aber nicht schlecht: String answers =
				// q.getAnswers().toString();

				int prefix = 1;

				// Map, die die UUID einer Zahl zuweist, die der User eingeben soll
				// merkt sich zuorfnung zwische pr�fix und uuid
				Map<Integer, UUID> prefixMap = new HashMap<>();

				for (Entry<UUID, String> entry : q.getAnswers().entrySet())
				{
					System.out.println("Die Antwort " + prefix + " : " + entry.getValue());
					prefixMap.put(prefix, entry.getKey());
					prefix++; // damit bei jeder ausgegebenen Antwortm�glichkeit hochgez�hlt wird
				}
				// benutzer auffordern, dass er eintscheidung eingibt
				String answer;

				// solange bis der user was eingibt
				int id; // damit wir's ausserhlab der schleife verwenden k�nnen!!!

				while (true) //pr�ft die g�ltigkeit - kann auch f�r g�ltigkeit des namens - if k�rzer als 2 zeichen oder so - verwendet werden
				{
					// schleife soll nur eine g�ltige eingabe des users erzwingen!!

					answer = br.readLine();
					if (answer == null)
					{
						System.out.println("Auf Wiedersehen, selber schuld! Das Programm wurde vom User beendet");
						return;
					}

					try
					{
						// umwandel von tring in it versuchen
						id = Integer.parseInt(answer);// try-block nur f�r diese Anfrage n�tig
						if (prefixMap.containsKey(id)) // enth�lt diese Map die Zahl?
						{
							// wenn die Zahl enthalten ist, brechen wir aus der While-Schleife aus
							break;
						}
					} catch (NumberFormatException e)
					{
						System.out.println("Ung�ltige Eingabe!");
						System.out.println("--------");
						// danach beginnt schleife von vorn
					}
				}
				// prefixMap.get(id); liefertz die UUID zur�ck
				Answer a = new Answer(prefixMap.get(id));

				// jetzt sollten wir kommunizieren und was schicken
				oos.writeObject(a);

				Answer as = (Answer) ois.readObject();

				if (as.getGuess().equals(prefixMap.get(id)))
				{
					System.out.println("Korrekt!");
					System.out.println("--------");
					System.out.println();
				} else
				{
					System.out.println("Falsche Antwort! Richtig: " + q.getAnswers().get(as.getGuess()));
					System.out.println("--------");
					System.out.println();
				}

				if (q.isLast())
				{
					// wenn das die letzte war, dann brechen wir den while-true loop ab und steigen
					// aus
					break;
				}
			}

			System.out.println("Danke f�r's Spielen! Komm bald wieder!\r\n");
			
			System.out.println("Erreichte Punkte der Spieler:\r\n");
			
			Map<String, Integer> scores = (Map<String, Integer>) ois.readObject();
			
			int value = 0;
			String winner = "";
			List<String> winners = new ArrayList<>();
			
			for (Entry<String, Integer> score : scores.entrySet())
			{
				if(score.getValue() > value)
				{
					value = score.getValue();
					winner = score.getKey();
				}	
				System.out.println(score.getKey() + ": " + score.getValue());
			}
			
			for(Entry<String, Integer> score : scores.entrySet())
			{
				if(value == score.getValue())
				{
					winners.add(score.getKey());
				}
			}
			if(winners.size() == 1)
			{
				System.out.println();
				System.out.println("Gewinner: " + winner + " mit " + value + " Punkten. ");
			}
			else if(winners.size() > 1)
			{
				System.out.println("Gewinner mit " + value +" sind:");
				for (String eq : winners)
				{
					System.out.println(eq);
				}
			}
			else
			{
				System.out.println("Fehler beim Feststellen der Gewinner...");
			}
			
			

		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}