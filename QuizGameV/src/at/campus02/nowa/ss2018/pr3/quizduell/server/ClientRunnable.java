package at.campus02.nowa.ss2018.pr3.quizduell.server;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import at.campus02.nowa.ss2018.pr3.quizduell.messages.Answer;
import at.campus02.nowa.ss2018.pr3.quizduell.messages.Player;
import at.campus02.nowa.ss2018.pr3.quizduell.messages.Question;

public class ClientRunnable implements Runnable
{
	private Socket client;
	private GameManager gm;
	private int maxRounds;
	
	private String playerName;
	private int currentScore = 0;
	
	
	public ClientRunnable(Socket client, GameManager gm)
	{
		super();
		this.client = client;
		this.gm = gm;
	}

	// Thread startet
	// je Verbindung gibts ein eigenen ClientRunnable
	@Override
	public void run()
	{// streams aufmachen, damit wir kommunizieren k�nnen
		try (ObjectInputStream ois = new ObjectInputStream(client.getInputStream());
				ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());)
		{
			// warten bis Player Object geschickt wird
			// readObject gibt Basis-Object zur�ck, deshalb cast auf Player
			Player p = (Player) ois.readObject();
			
			playerName = p.getName();

			// Spielernamen ausgeben lassen (am Server! - zum mitschauen, wer sich schon
			// angemeldet hat)
			System.out.println(p.getName());
			
			
			try(FileInputStream fin = new FileInputStream("server.properties"))
			{
				Properties config = new Properties();
				config.load(fin);
				maxRounds = Integer.parseInt(config.getProperty("game.rounds"));
				
			} 
			catch(NumberFormatException e1)
			{
				maxRounds = 5;
			}
			catch (FileNotFoundException e1)
			{
				maxRounds = 5;
			} 
			catch (IOException e1)
			{
				System.out.println("Konfigurationen konnten nicht gelesen werden: " + e1.getMessage());
				System.exit(-1);		//  0 == Alles war gut; alles im Negativen Bereich zeigt Fehler an;  Alles im positiven Bereich gibt zusatzinfo. Z.b das Speichern war erfolgreich
			}
			

//			int maxRounds = 2;
			
			
			
			
			int count = 1; // Z�hlt Runden
			
			if(!gm.registerClient(this))		// Eigenes Objekt wird registriert
			{
				synchronized (this)
				{
					wait();			//Unendliches Warten bis sich Spieler angemeldet haben.
				}
			}
			
			
			
			while (count <= maxRounds)
			{
				// zufallsfrage holen
				QuestionRecord qr = gm.getCurrentQuestion();

				// Name bekannt, jetzt soll Frage geschickt werden
				// Objekt bauen um Frage zu verschicken
				Question q = new Question(qr.getLevel(), qr.getText());

				// answers-map mit richtiger antwort holen und bef�llen
				q.getAnswers().put(qr.getCorrect().getUuid(), qr.getCorrect().getText());

				// liste der falschen antworten abrufen
				for (AnswerRecord ar : qr.getWrong())
				{
					q.getAnswers().put(ar.getUuid(), ar.getText());
				}

				if (count == maxRounds)
				{
					q.setLast(); // Flag wird aktiviert und schickt jetzt true!
				}

				// antowrt auswahl gewschickt
				oos.writeObject(q);
				oos.flush();

				// server soll was schicken,daswir auf answer casten k�nnen
				Answer a = (Answer) ois.readObject();
				// Frage wei�, welche AW richtig ist!! vergleich sollte uns dann sagen, ob
				// ricgtig oder fals
				boolean correct = a.getGuess().equals(qr.getCorrect().getUuid());
				
				
				
				if(!gm.setAnswer(this, correct))
				{
					synchronized(this)			// um sicherzustellen, dass alle eine Antwort abgegeben haben bevor die richtige Antwort Preis gegeben wird.
					{
						wait();					
					}
				}
				
				// kann mir Result-Klasse sparen und nochmal Answer verwenden!
				// vergleich kann auch der Client machen
				Answer as = new Answer(qr.getCorrect().getUuid());
				oos.writeObject(as);
				oos.flush();

				count++;

			}
			
			
			Map<String, Integer> scores = gm.getScores();
			
			oos.writeObject(scores);
			oos.flush();

		} catch (IOException e)
		{
			e.printStackTrace();
		} catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}

	}

	public String getPlayerName()
	{
		return playerName;
	}

}
