package at.campus02.nowa.ss2018.pr3.quizduell.messages;

import java.io.Serializable;
import java.util.UUID;

public class Answer implements Serializable //ALLE KLASSEN ZUR KOMMUNIKATION M�SSEN SERIALIZEBLE SEIN
{

	private static final long serialVersionUID = 3L;
	
	private UUID guess; 

	public Answer(UUID guess)
	{
		super();
		this.guess = guess;
	}

	public UUID getGuess() //vielleicht schlauer auf getUUID umvbenennen
	{
		return guess;
	}
	
	

}
