package at.campus02.ase.messages;

import java.util.ArrayList;


public class Message
{
	private String titel;
	private String text;
	
	ArrayList<Message> Kommunikation = new ArrayList<Message>();
		
	public Message(String titel, String text)
	{
		this.titel=titel;
		this.text=text;
	}

	public Message antworten(String titel, String text)
	{
		Message neues = new Message(titel, text);
		Kommunikation.add(neues);

		return neues;
	}

	public ArrayList<Message> getAntworten()
	{
		ArrayList <Message> alleAntworten = new ArrayList<Message>();
		
		for (Message message : Kommunikation)
		{
			alleAntworten.add(message);
		}
		
		
		return alleAntworten;
	}

	public int countMessages()
	{
		int result=0;
		
		for (Message message : Kommunikation)
		{
			if(message.getAntworten().size()>0)
			{
				result+=message.getAntworten().size()+1;
			}
		}

		return result;
	}
	
	public String toString()
	{
//		if(getAntworten().size()>0)
//		{
//			return String.format("(%s,%s)%s", titel, text, Kommunikation);
//		}
//		else
//		{
//			return String.format("(%s,%s)", titel, text);
//		}
		
		String ausgabe="";		
		
		for (Message me : Kommunikation)
		{
			ausgabe=ausgabe+me.toString();
		}
		
		if(!ausgabe.isEmpty())
		{
			ausgabe="["+ausgabe+"]";
		}
		
		return String.format("(%s,%s)%s",titel, text, ausgabe);
		
		
		
	}
}
