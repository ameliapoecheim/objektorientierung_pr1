package at.campus02.ase.filesystem;

import java.util.ArrayList;

public class Filesystem
{
	ArrayList<File> Verwaltung = new ArrayList<File>();
	
	public void addFile(File b)
	{
		Verwaltung.add(b);
	}

	public int countSize()
	{
		int result=0;
		for (File file : Verwaltung)
		{
			result=result+file.getSize();
		}
		return result;
	}

	public double averageSize()
	{
		double result=0;
		
		for (File file : Verwaltung)
		{
			result+=file.getSize();
		}
		result=result/Verwaltung.size();
		return result;

	}

	public ArrayList<File> fileByOwner(String owner)
	{
		ArrayList<File> ownerfilter = new ArrayList<File>();
		
		for (File file : Verwaltung)
		{
			if(file.getOwner()==owner)
			{
				ownerfilter.add(file);
			}
		}
		
		return ownerfilter;
	}

	public ArrayList<File> findFile(String search)
	{
		ArrayList<File> allgFilter = new ArrayList<File>();
		
		if(search==null)
		{
			return allgFilter;
		}
		
		for (File file : Verwaltung)
		{
			if(file.match(search))
			{
				allgFilter.add(file);
			}
		}
		

		return allgFilter;
	}
}
