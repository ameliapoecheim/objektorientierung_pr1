package at.campus02.ase.filesystem;

public class File
{

	private String owner;
	private String name;
	private int size;

	public File(String owner, String name, int size)
	{
		this.owner=owner;
		this.name=name;
		this.size=size;
	}

	public String getOwner()
	{
		return owner;
	}

	public String getName()
	{
		return name;
	}

	public int getSize()
	{
		return size;
	}

	public boolean match(String search)
	{
		
		if(owner.contains(search)||name.contains(search))
		{
			return true;
		}
			
			
		return false;
	}
	
	public String toString()
	{
		return String.format("[%s,%s,%d]", owner, name, size);
	}
	

}
