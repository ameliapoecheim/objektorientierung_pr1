

import static org.junit.Assert.*;

import org.campus02.domino.Tile;
import org.junit.Test;

public class TileTest {
	@Test
	public void test01() throws Exception {

		Tile a = new Tile(3, 4);

		assertEquals(4, a.getNumber1());
		assertEquals(3, a.getNumber2());

	}

	@Test
	public void test02() throws Exception {

		Tile a = new Tile(4, 2);
		assertEquals(4, a.getNumber1());
		assertEquals(2, a.getNumber2());
	}

	@Test
	public void test03() throws Exception {

		Tile a = new Tile(4, 2);
		assertEquals("(4,2)", a.toString());
	}

	@Test
	public void test06() throws Exception {

		Tile a = new Tile(4, 1);
		Tile b = new Tile(3, 2);

		assertFalse(a.compare(b));
	}

	@Test
	public void test04() throws Exception {

		Tile a = new Tile(4, 1);
		Tile b = new Tile(4, 2);

		assertTrue(a.compare(b));
	}

	@Test
	public void test05() throws Exception {

		Tile a = new Tile(3, 1);
		Tile b = new Tile(4, 2);

		assertTrue(a.compare(b));
	}
}
