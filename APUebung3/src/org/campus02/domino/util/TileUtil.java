package org.campus02.domino.util;

import java.util.ArrayList;

import org.campus02.domino.Tile;

public class TileUtil
{

	public static int sum(ArrayList<Tile> tiles)
	{
		int sum=0;
		
		for (Tile til : tiles)
		{
			sum=sum+til.getNumber1()+til.getNumber2();
		}
		return sum;
	}

	public static Tile greatesTile(ArrayList<Tile> tiles)
	{

		Tile greatest=tiles.get(0);
		
		for (Tile til : tiles)
		{
			if(greatest.compare(til)==true)
			{
				greatest=til;
			}
		}
		
		return greatest;

	}
}
