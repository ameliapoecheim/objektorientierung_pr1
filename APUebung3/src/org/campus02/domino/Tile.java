package org.campus02.domino;

public class Tile
{

	private int h;
	private int s;

	public Tile(int h, int s)
	{
		this.h = h;
		this.s = s;
	}

	public int getNumber1()
	{
		if (h > s)
		{
			return h;
		} else
		{
			return s;
		}
	}

	public int getNumber2()
	{
		if (h < s)
		{
			return h;
		} else
		{
			return s;
		}
	}

	public boolean compare(Tile otherTile)
	{
		if(this.getNumber1()==otherTile.getNumber1())
		{
			if(this.getNumber2()>otherTile.getNumber2())
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else if(this.getNumber1()>otherTile.getNumber1())
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	@Override
	public String toString()
	{
		return String.format("(%s,%s)", h, s);
	}
}
