package edu.campus02.automat;
import java.util.ArrayList;

public class State
{
	private String name;
	private boolean fina;
	
	ArrayList<Transition>transVerwaltung=new ArrayList<Transition>();
	
	public State(String name)
	{
		this.name=name;
		
	}

	public State(String name, boolean fina)
	{
		this.name=name;
		this.fina=fina;
		
	}

	public boolean isFinal()
	{
		return fina;
	}

	public void addTransition(Transition tran)
	{
		transVerwaltung.add(tran);
	}

	public Transition findMatchingTransition(char c)
	{
		for (Transition tra : transVerwaltung)
		{
			if(tra.match(c))
			{
				return tra;
			}
		}
		return null;
	}
	
	public String toString()
	{
		if(transVerwaltung.isEmpty())
		{	
			return String.format("(%s,%b)", name, fina);
			
		}
		return String.format("(%s,%b,%s)", name, fina, transVerwaltung);
		
	
	}

}
