package edu.campus02.automat;

public class ValidateUtil
{

	public static boolean verify(State start, String word)
	{
		char[] temp = word.toCharArray();
		State current = start;
		
		for (char c : temp)
		{
			Transition matchingTransition = current.findMatchingTransition(c);
			if(matchingTransition!=null)
			{
				current=matchingTransition.getTarget();
			}
			else 
				{
					return false;
				}
		}
		
		
		
		return current.isFinal();
	}

	public static boolean verifyRekursive(State state, String word)
	{
		
		
		
		
		return false;
	}

}
