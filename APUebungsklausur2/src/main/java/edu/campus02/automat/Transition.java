package edu.campus02.automat;

public class Transition
{
	private String trigger;
	private State target;

	public Transition(String trigger, State target)
	{
		this.trigger=trigger;
		this.target=target;
	}

	public State getTarget()
	{
		return target;
	}

	public boolean match(char c)
	{
		String search2=""+c;			//CharToString
		
		if(trigger.contains(search2))
		{
			return true;
		}
		
		
//		~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		
//		char[] search=trigger.toCharArray();
		//  {t,r,i,g,g,e,r}  == trigger
		
		
		
	
//		for (char temp : search)
//		{
//			if(temp==c)
//			{
//				return true;
//			}		
//		}
		return false;		
	}
	
	public String toString()
	{
		return String.format("[%s]->%s",trigger, target);
	}

}
