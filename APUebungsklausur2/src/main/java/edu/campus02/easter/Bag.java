package edu.campus02.easter;

import java.util.ArrayList;

public class Bag
{
	ArrayList<Egg> Korb = new ArrayList<Egg>();

	public void putEgg(Egg egg)
	{
		Korb.add(egg);
	}

	public int countEggs()
	{
		int result = 0;

		for (int index = 0; index < Korb.size(); index++)
		{
			result++;
		}

		return result;
	}

	public double sumWeight()
	{
		double allweight = 0;

		for (Egg egg : Korb)
		{
			allweight = allweight + egg.getWeight();
		}

		return allweight;
	}

	public double sumWeight(String color)
	{
		double selectedweight = 0;

		for (Egg egg : Korb)
		{
			if (egg.getColor() == color)
			{
				selectedweight = selectedweight + egg.getWeight();
			}
		}
		return selectedweight;
	}

	public int countDifferentColors()
	{
		ArrayList<String> diffColor = new ArrayList<String>();

		for (Egg egg : Korb)
		{
			if (!diffColor.contains(egg.getColor()))
			{
				diffColor.add(egg.getColor());

			}
		}
		return diffColor.size();

		// Egg temp=Korb.get(0);
		// int result=1; // Result 1, weil das 1. Ei Schon eine Farbe hat
		//
		// for(int index=1;index<Korb.size();index++)
		// {
		// if(Korb.get(index).getColor()!=temp.getColor())
		// {
		// temp=Korb.get(index);
		// result++;
		// }
		// }
		// return result;
	}

}
