package edu.campus02.easter;

public class Egg
{
	private double eggweight;
	private String eggcolor;
	
	
	public Egg(double weight)
	{
		eggweight=weight;
		eggcolor="weiss";		
	}

	public Egg(double weight, String color)
	{
		eggweight=weight;
		eggcolor=color;
	}

	public double getWeight()
	{
		return eggweight;
	}

	public String getColor()
	{
		return eggcolor;
	}

	public void setColor(String color)
	{
		eggcolor=color;
	}
	
	public String toString()
	{
		return String.format("(%.2f %s)", eggweight,eggcolor);
		
	}
	

}
