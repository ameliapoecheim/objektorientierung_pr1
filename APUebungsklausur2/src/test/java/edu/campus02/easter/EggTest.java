package edu.campus02.easter;

import static org.junit.Assert.*;

import org.junit.Test;

public class EggTest {

	@Test
	public void testKonstruktorDefaultColor() throws Exception {

		Egg e1 = new Egg(75);

		assertEquals(75, e1.getWeight(), 0.001);
		assertEquals("weiss", e1.getColor());

	}
	@Test
	public void testKonstruktorColor() throws Exception {

		Egg e1 = new Egg(75,"red");

		assertEquals(75, e1.getWeight(), 0.001);
		assertEquals("red", e1.getColor());

	}
	
	@Test
	public void testKonstruktorSetColor() throws Exception {

		Egg e1 = new Egg(75);
		e1.setColor("rot");
		assertEquals(75, e1.getWeight(), 0.001);
		assertEquals("rot", e1.getColor());

	}

	@Test
	public void testToString() throws Exception {

		Egg e1 = new Egg(75.333);

		assertEquals("(75,33 weiss)", e1.toString());

	}
}
